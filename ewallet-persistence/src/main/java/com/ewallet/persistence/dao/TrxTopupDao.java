package com.ewallet.persistence.dao;

import com.ewallet.persistence.model.TrxTopup;

/**
 * @author akbar.wijayanto
 * Date Oct 22, 2015 11:35:39 AM
 */
public interface TrxTopupDao extends GenericTransactionDao<TrxTopup, Long> {

}
