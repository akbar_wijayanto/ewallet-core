package com.ewallet.persistence.dao;

import com.ewallet.persistence.model.CoreBank;

/**
 * @author akbar.wijayanto
 * Date Oct 16, 2015 7:47:19 PM
 */
public interface CoreBankDao extends GenericDao<CoreBank, String> {

}
