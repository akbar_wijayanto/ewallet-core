package com.ewallet.persistence.dao;

import com.ewallet.persistence.model.CoreSystemParameter;

public interface CoreSystemParameterDao extends GenericDao<CoreSystemParameter, String>{

}
