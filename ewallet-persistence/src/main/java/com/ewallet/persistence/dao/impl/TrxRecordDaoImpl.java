package com.ewallet.persistence.dao.impl;

import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.ewallet.persistence.dao.TrxRecordDao;
import com.ewallet.persistence.model.CoreUser;
import com.ewallet.persistence.model.TrxRecord;
import com.ewallet.util.DateUtil;

/**
 * @author akbar.wijayanto 
 * Date Oct 23, 2015 11:16:47 PM
 */
@Repository
public class TrxRecordDaoImpl extends
		GenericTransactionDaoImpl<TrxRecord, Long> implements TrxRecordDao {

	public TrxRecordDaoImpl() {
		super(TrxRecord.class);
	}

	@Override
	public List<TrxRecord> getBySellerId(String sellerId) {
		String sql = "FROM TrxRecord r WHERE r.coreSeller.sellerId=:sellerId";
		List<TrxRecord> listRecords = getEntityManager()
				.createQuery(sql)
				.setParameter("sellerId", sellerId)
				.setMaxResults(8)
				.getResultList();
		return listRecords;
	}

	@Override
	public List<TrxRecord> getByUsername(String username) {
		String sql = "FROM TrxRecord r WHERE r.coreUser.username=:username order by r.transactionDate desc";
		List<TrxRecord> listRecords = getEntityManager()
				.createQuery(sql)
				.setParameter("username", username)
				.setMaxResults(10)
				.getResultList();
		return listRecords;
	}

	@Override
	public List<TrxRecord> getSellerSettlementReport(String sellerId,
			String name, Date startDate, Date endDate) {
		endDate = DateUtil.addDate(endDate, 1);
		String sql = "FROM TrxRecord r WHERE r.coreSeller.firstName LIKE "+"'%"+name+"%'"+" " +
				"OR r.coreSeller.lastName LIKE "+"'%"+name+"%'"+" " +
				"AND r.transactionDate BETWEEN :startDate AND :endDate order by r.transactionDate desc";
		try {
			return getEntityManager()
					.createQuery(sql)
					.setParameter("startDate", startDate)
					.setParameter("endDate", endDate)
					.getResultList();
		} catch (Exception e) {return null;}
	}

}
