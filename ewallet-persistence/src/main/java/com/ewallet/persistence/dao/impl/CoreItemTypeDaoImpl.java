package com.ewallet.persistence.dao.impl;

import org.springframework.stereotype.Repository;

import com.ewallet.persistence.dao.CoreItemTypeDao;
import com.ewallet.persistence.model.CoreItemType;

/**
 * @author akbar.wijayanto
 * Date Oct 16, 2015 7:48:12 PM
 */
@Repository("coreItemTypeDao")
public class CoreItemTypeDaoImpl extends GenericDaoImpl<CoreItemType, Long> implements
		CoreItemTypeDao {

	public CoreItemTypeDaoImpl() {
		super(CoreItemType.class);
	}
	
}
