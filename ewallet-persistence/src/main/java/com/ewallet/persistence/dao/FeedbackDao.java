package com.ewallet.persistence.dao;

import java.util.List;

import com.ewallet.persistence.model.Feedback;

/**
 * @author akbar.wijayanto
 * Date Oct 16, 2015 8:00:37 PM
 */
public interface FeedbackDao extends GenericDao<Feedback, Long> {

	List<Feedback> getAllSellerFeedbacks();
	List<Feedback> getAllManagementFeedbacks();
	List<Feedback> getSellerFeedbacks(String sellerId);
	
}
