package com.ewallet.persistence.dao;

import com.ewallet.persistence.model.VirtualAccount;

/**
 * @author akbar.wijayanto
 * Date Oct 23, 2015 11:16:09 PM
 */
public interface VirtualAccountDao extends GenericTransactionDao<VirtualAccount, String> {
}
