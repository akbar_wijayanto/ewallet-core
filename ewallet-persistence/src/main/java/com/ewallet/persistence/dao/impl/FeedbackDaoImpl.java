package com.ewallet.persistence.dao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.ewallet.persistence.dao.FeedbackDao;
import com.ewallet.persistence.model.Feedback;

/**
 * @author akbar.wijayanto
 * Date Oct 16, 2015 8:01:43 PM
 */
@Repository
public class FeedbackDaoImpl extends GenericDaoImpl<Feedback, Long> implements FeedbackDao {

	public FeedbackDaoImpl() {
		super(Feedback.class);
	}

	@Override
	public List<Feedback> getAllSellerFeedbacks() {
		String sql = "FROM Feedback f WHERE f.coreSeller is not null";
		List<Feedback> result = getEntityManager()
								.createQuery(sql)
								.getResultList();
		return result;
	}

	@Override
	public List<Feedback> getAllManagementFeedbacks() {
		String sql = "FROM Feedback f WHERE f.management is not null";
		List<Feedback> result = getEntityManager()
								.createQuery(sql)
								.getResultList();
		return result;
	}

	@Override
	public List<Feedback> getSellerFeedbacks(String sellerId) {
		String sql = "FROM Feedback f WHERE f.coreSeller.sellerId=:sellerId";
		List<Feedback> result = getEntityManager()
								.createQuery(sql)
								.setParameter("sellerId", sellerId)
								.getResultList();
		return result;
	}
	
}
