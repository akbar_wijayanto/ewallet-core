package com.ewallet.persistence.dao.impl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.ewallet.enumeration.OrderStatusEnum;
import com.ewallet.persistence.dao.TrxOrderDao;
import com.ewallet.persistence.model.TrxOrder;
import com.ewallet.util.DateUtil;

/**
 * @author akbar.wijayanto
 * Date Oct 16, 2015 8:01:43 PM
 */
@Repository
public class TrxOrderDaoImpl extends GenericTransactionDaoImpl<TrxOrder, String> implements TrxOrderDao {

	public TrxOrderDaoImpl() {
		super(TrxOrder.class);
	}

	@Override 
	public List<TrxOrder> getOrdersByUser(String username) {
		//String sql = "from TrxOrder o where o.coreUser.username=:username";  //custom by silvi.zain req client
		String sql = "from TrxOrder o where o.coreUser.username=:username and status <> 'DELIVERED' and o.coreSeller.sellerId is not null and o.orderDate BETWEEN :startDate AND :endDate order by orderDate desc";
		SimpleDateFormat spdf = new SimpleDateFormat("yyyy-MM-dd");
		String sDateToday = spdf.format(new Date());
		try {
			return getEntityManager()
					.createQuery(sql)
					.setParameter("username", username)
					.setParameter("startDate", DateUtil.fromDBFullFormat(sDateToday+" 00:00:00"))
					.setParameter("endDate", DateUtil.fromDBFullFormat(sDateToday+" 23:59:59"))
					.getResultList();
		} catch (Exception e) {return null;}
	}

	@Override
	public List<TrxOrder> getOrdersBySeller(String sellerId) {
		String sql = "from TrxOrder o where o.coreSeller.sellerId=:sellerId and o.orderDate BETWEEN :startDate AND :endDate";  //custom by silvi.zain req client
//		String sql = "from TrxOrder o where o.coreSeller.sellerId=:sellerId and status NOT IN('DELIVERED','REFUND') order by order_date asc";
		System.out.println(DateUtil.addDate(new Date(), -1));
		System.out.println(DateUtil.addDate(new Date(), 1));
		SimpleDateFormat spdf = new SimpleDateFormat("yyyy-MM-dd");
		String sDateToday = spdf.format(new Date());
		System.out.println("date string today:"+sDateToday);
		
		try {
			return getEntityManager()
					.createQuery(sql)
					.setParameter("sellerId", sellerId)
					/*.setParameter("startDate", DateUtil.addDate(new Date(), -1))
					.setParameter("endDate", DateUtil.addDate(new Date(), 1))*/
					.setParameter("startDate", DateUtil.fromDBFullFormat(sDateToday+" 00:00:00"))
					.setParameter("endDate", DateUtil.fromDBFullFormat(sDateToday+" 23:59:59"))
//					.setMaxResults(8)   //request client
					.getResultList();
		} catch (Exception e) {return null;}
	}

	@Override
	public TrxOrder getOrderByOrderId(String orderId) {
		String sql = "from TrxOrder o where o.orderId=:orderId";
		try {
			return (TrxOrder) getEntityManager()
					.createQuery(sql)
					.setParameter("orderId", orderId)
					.getSingleResult();
		} catch (Exception e) {return null;}
	}
	
	@Override
	 public int checkValidOrder(String sellerId, String orderId) {
	  String sql ="from TrxOrder o where o.coreSeller.sellerId=:sellerId and o.orderId=:orderId";
	  int validResp = 0;
	  try {
	   List<String> respSql = new ArrayList<String>();
	    respSql = getEntityManager()
	     .createQuery(sql)
	     .setParameter("sellerId", sellerId)
	     .setParameter("orderId", orderId)
	     .getResultList();
	    if(respSql.size()>0){
	     validResp=1;
	    }else{
	     validResp=0;
	     }
	  } catch (Exception e) {return 99;}
	  return validResp;
	 }

	@Override
	public List<TrxOrder> getLast7DaysOrdersBySeller(String sellerId) {
		String sql = "from TrxOrder o where o.coreSeller.sellerId=:sellerId and o.orderDate BETWEEN :startDate AND :endDate AND o.status in (:status, :status2)";
		try {
			return getEntityManager()
					.createQuery(sql)
					.setParameter("sellerId", sellerId)
					.setParameter("startDate", DateUtil.addDate(new Date(), -7))
					.setParameter("endDate", DateUtil.addDate(new Date(), 1))
					.setParameter("status", OrderStatusEnum.DELIVERED.getName())
					.setParameter("status2", OrderStatusEnum.DONE.getName())
					.getResultList();
		} catch (Exception e) {return null;}
	}
	
	@Override
	public List<TrxOrder> getLast7DaysOrdersBySellerUnpaid(String sellerId) {
		String sql = "from TrxOrder o where o.coreSeller.sellerId=:sellerId and o.orderDate BETWEEN :startDate AND :endDate AND o.status in (:status, :status2) AND o.paidDate =:paidDate";
		try {
			return getEntityManager()
					.createQuery(sql)
					.setParameter("sellerId", sellerId)
					.setParameter("startDate", DateUtil.addDate(new Date(), -7))
					.setParameter("endDate", DateUtil.addDate(new Date(), 1))
					.setParameter("status", OrderStatusEnum.DELIVERED.getName())
					.setParameter("status2", OrderStatusEnum.DONE.getName())
					.setParameter("paidDate", null)
					.getResultList();
		} catch (Exception e) {return null;}
	}

	@Override
	public List<TrxOrder> getLast7DaysOrders() {
		String sql = "from TrxOrder o where o.orderDate BETWEEN :startDate AND :endDate AND o.status in (:status,:status2) ";//and paid.date is not null
		try {
			return getEntityManager()
					.createQuery(sql)
					.setParameter("startDate", DateUtil.addDate(new Date(), -7))
					.setParameter("endDate", DateUtil.addDate(new Date(), 1))
					.setParameter("status", OrderStatusEnum.DELIVERED.getName())
					.setParameter("status2", OrderStatusEnum.DONE.getName())
					.getResultList();
		} catch (Exception e) {return null;}
	}

	@Override
	public List<TrxOrder> getLast7DaysOrdersByUser(String username) {
		String sql = "from TrxOrder o where o.coreUser.username=:username and o.orderDate BETWEEN :startDate AND :endDate AND o.status in (:status,:status2)";
		try {
			return getEntityManager()
					.createQuery(sql)
					.setParameter("username", username)
					.setParameter("startDate", DateUtil.addDate(new Date(), -7))
					.setParameter("endDate", DateUtil.addDate(new Date(), 1))
					.setParameter("status", OrderStatusEnum.DELIVERED.getName())
					.setParameter("status2", OrderStatusEnum.DONE.getName())
					.getResultList();
		} catch (Exception e) {return null;}
	}

	@Override
	public List<TrxOrder> getPeriodOrdersBySeller(String sellerId,
			String startDate, String endDate) {
		String sql = "from TrxOrder o where o.coreSeller.sellerId=:sellerId and o.orderDate >= :startDate AND o.orderDate <= :endDate AND o.status in (:status,:status2)";
		try {
			return getEntityManager()
					.createQuery(sql)
					.setParameter("sellerId", sellerId)
					.setParameter("startDate", DateUtil.fromDBFullFormat(startDate+" 00:00:00"))
					.setParameter("endDate", DateUtil.fromDBFullFormat(endDate+" 23:59:59"))
					.setParameter("status", OrderStatusEnum.DELIVERED.getName())
					.setParameter("status2", OrderStatusEnum.DONE.getName())
					.getResultList();
		} catch (Exception e) {return null;}
	}

	@Override
	public List<TrxOrder> getPeriodOrders(String startDate, String endDate) {
		String sql = "from TrxOrder o where o.orderDate >= :startDate AND o.orderDate <= :endDate AND o.status<>:status";
		try {
			return getEntityManager()
					.createQuery(sql)
					.setParameter("startDate", DateUtil.fromDBFullFormat(startDate+" 00:00:00"))
					.setParameter("endDate", DateUtil.fromDBFullFormat(endDate+" 23:59:59"))
					.setParameter("status", OrderStatusEnum.REFUND.getName())
					.getResultList();
		} catch (Exception e) {return null;}
	}

	@Override
	public List<TrxOrder> getPeriodOrdersBySellerUnpaid(String sellerId,
			String startDate, String endDate) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<TrxOrder> getPeriodOrdersByUser(String username, String startDate, String endDate) {
		String sql = "from TrxOrder o where o.coreUser.username=:username and o.orderDate >= :startDate AND o.orderDate <= :endDate AND o.status in (:status,:status2)";
		try {
			return getEntityManager()
					.createQuery(sql)
					.setParameter("username", username)
					.setParameter("startDate", DateUtil.fromDBFullFormat(startDate+" 00:00:00"))
					.setParameter("endDate", DateUtil.fromDBFullFormat(endDate+" 23:59:59"))
					.setParameter("status", OrderStatusEnum.DELIVERED.getName())
					.setParameter("status2", OrderStatusEnum.DONE.getName())
					.getResultList();
		} catch (Exception e) {return null;}
	}

	@Override
	public List<TrxOrder> getPeriodicRefund(String startDate, String endDate) {
		String sql = "from TrxOrder o where o.orderDate BETWEEN :startDate AND :endDate AND o.status=:status";  //custom by silvi.zain req client
		try {
			return getEntityManager()
					.createQuery(sql)
					.setParameter("startDate", DateUtil.fromDBFullFormat(startDate+" 00:00:00"))
					.setParameter("endDate", DateUtil.fromDBFullFormat(endDate+" 23:59:59"))
					.setParameter("status", OrderStatusEnum.REFUND.getName())
					.getResultList();
		} catch (Exception e) {return null;}
	}

	@Override
	public List<TrxOrder> getPeriodicOrder(String startDate, String endDate) {
		String sql = "from TrxOrder o where o.orderDate BETWEEN :startDate AND :endDate AND o.status<>:status";  //custom by silvi.zain req client
		try {
			return getEntityManager()
					.createQuery(sql)
					.setParameter("startDate", DateUtil.fromDBFullFormat(startDate+" 00:00:00"))
					.setParameter("endDate", DateUtil.fromDBFullFormat(endDate+" 23:59:59"))
					.setParameter("status", OrderStatusEnum.REFUND.getName())
					.getResultList();
		} catch (Exception e) {return null;}
	}

	@Override
	public List<TrxOrder> getPeriodicRefundBySellerid(String sellerId, String startDate, String endDate) {
		String sql = "from TrxOrder o where o.coreSeller.sellerId=:sellerId AND o.orderDate >= :startDate AND o.orderDate <= :endDate AND o.status=:status";
		try {
			return getEntityManager()
					.createQuery(sql)
					.setParameter("sellerId", sellerId)
					.setParameter("startDate", DateUtil.fromDBFullFormat(startDate+" 00:00:00"))
					.setParameter("endDate", DateUtil.fromDBFullFormat(endDate+" 23:59:59"))
					.setParameter("status", OrderStatusEnum.REFUND.getName())
					.getResultList();
		} catch (Exception e) {return null;}
	}
	
	@Override
	public List<TrxOrder> getPeriodicOrderBySellerid(String sellerId, String startDate, String endDate) {
		String sql = "from TrxOrder o where o.coreSeller.sellerId=:sellerId AND o.orderDate BETWEEN :startDate AND :endDate AND o.status<>:status";
		try {
			return getEntityManager()
					.createQuery(sql)
					.setParameter("sellerId", sellerId)
					.setParameter("startDate", DateUtil.fromDBFullFormat(startDate+" 00:00:00"))
					.setParameter("endDate", DateUtil.fromDBFullFormat(endDate+" 23:59:59"))
					.setParameter("status", OrderStatusEnum.REFUND.getName())
					.getResultList();
		} catch (Exception e) {return null;}
	}
	
	@Override
	public List<TrxOrder> getPeriodOrdersBySellerId(String sellerId, String startDate, String endDate) {
		String sql = "from TrxOrder o where o.coreSeller.sellerId=:sellerId and o.orderDate >= :startDate AND o.orderDate <= :endDate AND o.status<>:status";
		try {
			return getEntityManager()
					.createQuery(sql)
					.setParameter("sellerId", sellerId)
					.setParameter("startDate", DateUtil.fromDBFullFormat(startDate+" 00:00:00"))
					.setParameter("endDate", DateUtil.fromDBFullFormat(endDate+" 23:59:59"))
					.setParameter("status", OrderStatusEnum.REFUND.getName())
					.getResultList();
		} catch (Exception e) {return null;}
	}

	@Override
	public List<TrxOrder> getLast7DaysOrdersForSettlement() {
		String sql = "from TrxOrder o where o.orderDate BETWEEN :startDate AND :endDate AND o.status in (:status,:status2) and o.paidDate is null";//and paid.date is not null
		try {
			return getEntityManager()
					.createQuery(sql)
					.setParameter("startDate", DateUtil.addDate(new Date(), -7))
					.setParameter("endDate", DateUtil.addDate(new Date(), 1))
					.setParameter("status", OrderStatusEnum.DELIVERED.getName())
					.setParameter("status2", OrderStatusEnum.DONE.getName())
					.getResultList();
		} catch (Exception e) {return null;}
	}

	@Override
	public List<TrxOrder> getPeriodOrdersByUserBuyerId(Long buyerId, String username, String startDate, String endDate) {
		//String sql = "from TrxOrder o where o.coreUser.id=:id AND o.coreUser.username=:username and o.orderDate >= :startDate AND o.orderDate <= :endDate AND o.status=:status";
		String sql = "from TrxOrder o where o.coreUser.username=:username and o.orderDate >= :startDate AND o.orderDate <= :endDate AND o.status in (:status,:status2)";
		try {
			return getEntityManager()
					.createQuery(sql)
					//.setParameter("id", buyerId)
					.setParameter("username", username)
					.setParameter("startDate", DateUtil.fromDBFullFormat(startDate+" 00:00:00"))
					.setParameter("endDate", DateUtil.fromDBFullFormat(endDate+" 23:59:59"))
					.setParameter("status", OrderStatusEnum.DELIVERED.getName())
					.setParameter("status2", OrderStatusEnum.DONE.getName())
					.getResultList();
		} catch (Exception e) {return null;}
	}

	@Override
	public List<TrxOrder> getPeriodicOrderBySellerid(String sellerId,
			Date startDate, Date endDate) {
		String sql = "from TrxOrder o where o.coreSeller.sellerId=:sellerId and o.paidDate between :start AND :end AND o.status in (:status,:status2)";
		try {
			Date date = DateUtil.toSimpleStripeFormat(startDate.toString());
			Date date2 = DateUtil.addDate(endDate, 1);
			return getEntityManager()
					.createQuery(sql)
					.setParameter("sellerId", sellerId)
					.setParameter("start", DateUtil.toSimpleStripeFormat(startDate.toString()))
					.setParameter("end", DateUtil.addDate(endDate, 1))
					.setParameter("status", OrderStatusEnum.DELIVERED.getName())
					.setParameter("status2", OrderStatusEnum.DONE.getName())
					.getResultList();
		} catch (Exception e) {return null;}
	}

	@Override
	public List<TrxOrder> getListOrdersForSettlement(String startDat, String endDat) {
		String sql = "from TrxOrder o where o.orderDate BETWEEN :startDate AND :endDate AND o.status in (:status,:status2) and o.paidDate is null";//and paid.date is not null
		//String sql = "from TrxOrder o where o.orderDate BETWEEN '2016-03-04 00:00:00' AND '2016-03-10 23:59:59' AND o.status in ('DELIVERED','DONE') and o.paidDate is null";//and paid.date is not null
		try {
			return getEntityManager()
					.createQuery(sql)
					.setParameter("startDate", DateUtil.fromDBFullFormat(startDat+" 00:00:00"))
					.setParameter("endDate", DateUtil.fromDBFullFormat(endDat+" 23:59:59"))
					.setParameter("status", OrderStatusEnum.DELIVERED.getName())
					.setParameter("status2", OrderStatusEnum.DONE.getName())
					.getResultList();
		} catch (Exception e) {return null;}
	}

}
