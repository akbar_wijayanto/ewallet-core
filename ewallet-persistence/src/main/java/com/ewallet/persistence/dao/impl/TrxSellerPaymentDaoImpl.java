package com.ewallet.persistence.dao.impl;

import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.ewallet.enumeration.OrderStatusEnum;
import com.ewallet.enumeration.SettleStatusEnum;
import com.ewallet.persistence.dao.TrxSellerPaymentDao;
import com.ewallet.persistence.model.TrxSellerPayment;
import com.ewallet.util.DateUtil;

/**
 * @author akbar.wijayanto Date Oct 23, 2015 11:16:47 PM
 */
@Repository
public class TrxSellerPaymentDaoImpl extends
		GenericTransactionDaoImpl<TrxSellerPayment, String> implements
		TrxSellerPaymentDao {

	public TrxSellerPaymentDaoImpl() {
		super(TrxSellerPayment.class);
	}

	@Override
	public List<TrxSellerPayment> getAllSellerPayment() {
		String sql = "from TrxSellerPayment o where o.status=:status";
		try {
			return getEntityManager().createQuery(sql)
					.setParameter("status", SettleStatusEnum.UNPAID.getName())
					.getResultList();
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public TrxSellerPayment getTrxSellerPaymentByPaymentId(String paymentId) {
		String sql = "from TrxSellerPayment o where o.status=:status and o.paymentId=:paymentId";
		try {
			List<TrxSellerPayment> bean = getEntityManager().createQuery(sql)
					.setParameter("status", SettleStatusEnum.UNPAID.getName())
					.setParameter("paymentId", paymentId).getResultList();
			return bean.get(0);
		} catch (Exception e) {
			return null;
		}
	}
	
	@Override
	public List<TrxSellerPayment> getSettlementBySellerId(String sellerid) {
		String sql = "from TrxSellerPayment st where st.coreSeller.sellerId=:sellerid order by paymentDate desc";
		try {
			return getEntityManager()
					.createQuery(sql)
					.setParameter("sellerid", sellerid)
					.setMaxResults(8)
					.getResultList();
		} catch (Exception e) {return null;}
	}

	@Override
	public TrxSellerPayment getBySellerIdAndPaidDate(String sellerId,
			Date paidDate) {
		String sql = "from TrxSellerPayment st where st.coreSeller.sellerId=:sellerId AND st.paymentDate between :start and :end AND status=:status";
		try {
			return (TrxSellerPayment) getEntityManager()
					.createQuery(sql)
					.setParameter("sellerId", sellerId)
					.setParameter("start", DateUtil.addDate(paidDate, -1))
					.setParameter("end", DateUtil.addDate(paidDate, 1))
					.setParameter("status", SettleStatusEnum.PAID.getName())
					.getSingleResult();
		} catch (Exception e) {return null;}
	}

	@Override
	public List<TrxSellerPayment> getBySellerIdAndDate(String sellerId,
			Date startDate, Date endDate) {
		String sql = "from TrxSellerPayment st where st.coreSeller.sellerId=:sellerId AND st.paymentDate between :start and :end AND status=:status";
		try {
			return getEntityManager()
					.createQuery(sql)
					.setParameter("sellerId", sellerId)
					.setParameter("start", DateUtil.addDate(startDate, -1))
					.setParameter("end", DateUtil.addDate(endDate, 1))
					.setParameter("status", SettleStatusEnum.PAID.getName())
					.getResultList();
		} catch (Exception e) {return null;}
	}

	@Override
	public List<TrxSellerPayment> getAllSellerPaymentPaid() {
		String sql = "from TrxSellerPayment o where o.status=:status";
		try {
			return getEntityManager().createQuery(sql)
					.setParameter("status", SettleStatusEnum.PAID.getName())
					.getResultList();
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public List<TrxSellerPayment> getAllSellerPaymentReport(String startDate, String endDate) {
		String sql = "from TrxSellerPayment o where o.paymentDate between :startDate and :endDate AND o.status=:status";
		try {
			return getEntityManager().createQuery(sql)
					.setParameter("startDate", DateUtil.fromDBFullFormat(startDate+" 00:00:00"))
					.setParameter("endDate", DateUtil.fromDBFullFormat(endDate+" 23:59:59"))
					.setParameter("status", SettleStatusEnum.PAID.getName())
					.getResultList();
		} catch (Exception e) {
			return null;
		}
	}
	
}
