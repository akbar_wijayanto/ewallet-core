package com.ewallet.persistence.dao;

import com.ewallet.persistence.model.TrxRecord;

/**
 * @author akbar.wijayanto
 * Date Oct 16, 2015 8:00:37 PM
 */
public interface TransactionDao extends GenericTransactionDao<TrxRecord, Long> {

}
