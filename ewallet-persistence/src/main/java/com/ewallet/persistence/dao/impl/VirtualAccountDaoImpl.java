package com.ewallet.persistence.dao.impl;

import org.springframework.stereotype.Repository;

import com.ewallet.persistence.dao.VirtualAccountDao;
import com.ewallet.persistence.model.VirtualAccount;

/**
 * @author akbar.wijayanto 
 * Date Oct 23, 2015 11:16:47 PM
 */
@Repository
public class VirtualAccountDaoImpl extends
		GenericTransactionDaoImpl<VirtualAccount, String> implements VirtualAccountDao {

	public VirtualAccountDaoImpl() {
		super(VirtualAccount.class);
	}

}
