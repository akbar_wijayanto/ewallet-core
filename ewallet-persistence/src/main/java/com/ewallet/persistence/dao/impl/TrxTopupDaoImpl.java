package com.ewallet.persistence.dao.impl;

import org.springframework.stereotype.Repository;

import com.ewallet.persistence.dao.TrxTopupDao;
import com.ewallet.persistence.model.TrxTopup;

/**
 * @author akbar.wijayanto
 * Date Oct 22, 2015 11:36:19 AM
 */
@Repository
public class TrxTopupDaoImpl extends GenericTransactionDaoImpl<TrxTopup, Long> implements TrxTopupDao {

	public TrxTopupDaoImpl() {
		super(TrxTopup.class);
	}
	
}
