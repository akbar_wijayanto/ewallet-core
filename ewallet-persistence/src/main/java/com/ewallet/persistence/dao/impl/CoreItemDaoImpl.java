package com.ewallet.persistence.dao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.ewallet.enumeration.RecordStatusEnum;
import com.ewallet.persistence.dao.CoreItemDao;
import com.ewallet.persistence.model.CoreItem;
import com.ewallet.persistence.model.CoreSeller;

/**
 * @author akbar.wijayanto
 * Date Oct 16, 2015 7:48:12 PM
 */
@Repository("coreItemDao")
public class CoreItemDaoImpl extends GenericDaoImpl<CoreItem, Long> implements
		CoreItemDao {

	public CoreItemDaoImpl() {
		super(CoreItem.class);
	}

	@Override
	public CoreItem save(CoreItem object) {
		return super.save(object);
	}

	@Override
	public void remove(Long id) {
		super.remove(id);
	}

	@Override
	public CoreItem authorize(CoreItem oldObject, CoreItem newObject,
			boolean isApproved) {
		return super.authorize(oldObject, newObject, isApproved);
	}

	@Override
	public CoreItem getByItemId(String itemId) {
		try {
			CoreItem product = (CoreItem) getEntityManager()
					.createQuery("from CoreItem where itemId=:itemId")
					.setParameter("itemId", itemId).getSingleResult();
			return product;
		} catch (Exception ex) {
			return null;
		}
	}

	@Override
	public List<CoreItem> getBySellerId(String sellerId) {
		String sql = "from CoreItem u WHERE u.coreSeller.sellerId=:sellerId AND u.coreSeller.status=:status";
		try {
			return (List<CoreItem>) getEntityManager()
					.createQuery(sql)
					.setParameter("sellerId", sellerId)
					.setParameter("status", RecordStatusEnum.LIVE.getName())
					.getResultList();
		} catch (Exception e) {return null;}
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<CoreItem> getAllAuthItem() {
		// TODO Auto-generated method stub
		String sql = "from CoreItem u WHERE u.status in (:status,:status2)";
		try {
			return (List<CoreItem>) getEntityManager()
					.createQuery(sql)
					.setParameter("status", RecordStatusEnum.INAU.getName())
					.setParameter("status2", "INAUACT")
					.getResultList();
		} catch (Exception e) {return null;}
	}
}
