package com.ewallet.persistence.model;

import java.io.Serializable;

import javax.persistence.AssociationOverride;
import javax.persistence.AssociationOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.Table;

/**
 * @author akbar.wijayanto
 * Date Nov 25, 2015 5:12:37 PM
 */
@Entity
@Table(name = "trx_order_item_detail")
@AssociationOverrides({
		@AssociationOverride(name = "pk.coreItem", joinColumns = @JoinColumn(name = "item_id", nullable = false)),
		@AssociationOverride(name = "pk.trxOrder", joinColumns = @JoinColumn(name = "order_id", nullable = false)) })
public class TrxOrderItemDetail extends BaseTransaction {
	
	private static final long serialVersionUID = 5988555213869838910L;
	private TrxOrderItemId pk;
	private Integer quantity;

	@EmbeddedId
	public TrxOrderItemId getPk() {
		return pk;
	}

	public void setPk(TrxOrderItemId pk) {
		this.pk = pk;
	}

	@Column(name = "quantity")
	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((pk == null) ? 0 : pk.hashCode());
		result = prime * result
				+ ((quantity == null) ? 0 : quantity.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TrxOrderItemDetail other = (TrxOrderItemDetail) obj;
		if (pk == null) {
			if (other.pk != null)
				return false;
		} else if (!pk.equals(other.pk))
			return false;
		if (quantity == null) {
			if (other.quantity != null)
				return false;
		} else if (!quantity.equals(other.quantity))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "TrxOrderItemDetail [pk=" + pk + ", quantity=" + quantity + "]";
	}
}
