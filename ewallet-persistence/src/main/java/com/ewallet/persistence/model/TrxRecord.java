package com.ewallet.persistence.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

/**
 * @author akbar.wijayanto
 * Date Oct 16, 2015 3:51:04 PM
 */
@Entity
@Table(name = "trx_record")
@Where(clause = " status <> 'HIST' OR  status is null ")
@SQLDelete(sql = "update trx_record set status='HIST' where id=?")
public class TrxRecord extends BaseTransaction {
	
	private static final long serialVersionUID = 4165661136334067426L;
	private Long id;
	private String transactionId;
	private Date transactionDate;
	private String accountNumber;
	private String pairedAccount;
	private BigDecimal amount;
	private String transactionType;
	private CoreSeller coreSeller;
	private CoreUser coreUser;

	@Column(name = "id")
    @GeneratedValue
    @Id
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "transaction_date")
	public Date getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}

	@Column(name = "account_number")
	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	@Column(name = "amount")
	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	
	@Column(name = "transaction_type")
	public String getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}

	@ManyToOne
	@JoinColumn(name = "user_id")
	public CoreUser getCoreUser() {
		return coreUser;
	}

	public void setCoreUser(CoreUser coreUser) {
		this.coreUser = coreUser;
	}
	
	@Column(name = "transaction_id", unique = true)
	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	@Column(name = "paired_account")
	public String getPairedAccount() {
		return pairedAccount;
	}

	public void setPairedAccount(String pairedAccount) {
		this.pairedAccount = pairedAccount;
	}
	
	@ManyToOne
	@JoinColumn(name = "seller_id")
	public CoreSeller getCoreSeller() {
		return coreSeller;
	}

	public void setCoreSeller(CoreSeller coreSeller) {
		this.coreSeller = coreSeller;
	}

	@Override
	public String toString() {
		return "TrxRecord [id=" + id + ", transactionId=" + transactionId
				+ ", transactionDate=" + transactionDate + ", accountNumber="
				+ accountNumber + ", pairedAccount=" + pairedAccount
				+ ", amount=" + amount + ", transactionType=" + transactionType
				+ ", coreSeller=" + coreSeller + ", coreUser=" + coreUser + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((accountNumber == null) ? 0 : accountNumber.hashCode());
		result = prime * result + ((amount == null) ? 0 : amount.hashCode());
		result = prime * result
				+ ((coreSeller == null) ? 0 : coreSeller.hashCode());
		result = prime * result
				+ ((coreUser == null) ? 0 : coreUser.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result
				+ ((pairedAccount == null) ? 0 : pairedAccount.hashCode());
		result = prime * result
				+ ((transactionDate == null) ? 0 : transactionDate.hashCode());
		result = prime * result
				+ ((transactionId == null) ? 0 : transactionId.hashCode());
		result = prime * result
				+ ((transactionType == null) ? 0 : transactionType.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TrxRecord other = (TrxRecord) obj;
		if (accountNumber == null) {
			if (other.accountNumber != null)
				return false;
		} else if (!accountNumber.equals(other.accountNumber))
			return false;
		if (amount == null) {
			if (other.amount != null)
				return false;
		} else if (!amount.equals(other.amount))
			return false;
		if (coreSeller == null) {
			if (other.coreSeller != null)
				return false;
		} else if (!coreSeller.equals(other.coreSeller))
			return false;
		if (coreUser == null) {
			if (other.coreUser != null)
				return false;
		} else if (!coreUser.equals(other.coreUser))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (pairedAccount == null) {
			if (other.pairedAccount != null)
				return false;
		} else if (!pairedAccount.equals(other.pairedAccount))
			return false;
		if (transactionDate == null) {
			if (other.transactionDate != null)
				return false;
		} else if (!transactionDate.equals(other.transactionDate))
			return false;
		if (transactionId == null) {
			if (other.transactionId != null)
				return false;
		} else if (!transactionId.equals(other.transactionId))
			return false;
		if (transactionType == null) {
			if (other.transactionType != null)
				return false;
		} else if (!transactionType.equals(other.transactionType))
			return false;
		return true;
	}
	
}