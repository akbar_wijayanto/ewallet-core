package com.ewallet.persistence.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

/**
 * @author akbar.wijayanto
 * Date Oct 16, 2015 2:56:27 PM
 */
@Entity
@Table(name = "trx_order")
@Where(clause = " status <> 'HIST' OR  status is null ")
@SQLDelete(sql = "update trx_order set status='HIST' where id=?")
public class TrxOrder extends BaseTransaction {
	
	private static final long serialVersionUID = 4165661136334067426L;
//	private Long id;
	private String orderId;
	private Date orderDate;
	private Date paidDate;
	private CoreUser coreUser;
	private Set<TrxOrderItemDetail> trxOrderItemDetails;
//	private List<CoreItem> coreItems = new ArrayList<CoreItem>();
	private CoreSeller coreSeller;
	private String paymentReference;
	
	//	@Column(name = "id")
//    @GeneratedValue
//    @Id
//	public Long getId() {
//		return id;
//	}
//
//	public void setId(Long id) {
//		this.id = id;
//	}
	@Id
	@Column(name = "order_id", unique = true)
	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	@Column(name = "order_date")
	public Date getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}

	@Column(name = "paid_date")
	public Date getPaidDate() {
		return paidDate;
	}

	public void setPaidDate(Date paidDate) {
		this.paidDate = paidDate;
	}
	
	@ManyToOne
	@JoinColumn(name = "user_id")
	public CoreUser getCoreUser() {
		return coreUser;
	}

	public void setCoreUser(CoreUser coreUser) {
		this.coreUser = coreUser;
	}

	@JsonIgnore
	@OneToMany(fetch = FetchType.EAGER, mappedBy="pk.trxOrder")
	public Set<TrxOrderItemDetail> getTrxOrderItemDetails() {
		return trxOrderItemDetails == null ? new HashSet<TrxOrderItemDetail>() : trxOrderItemDetails;
	}

	public void setTrxOrderItemDetails(Set<TrxOrderItemDetail> trxOrderItemDetails) {
		this.trxOrderItemDetails = trxOrderItemDetails;
	}
	
//	@JsonIgnore
//	@ManyToMany(fetch = FetchType.EAGER)
//	@JoinTable(name = "trx_order_detail", 
//			joinColumns = { @JoinColumn(name = "order_id") }, 
//			inverseJoinColumns = @JoinColumn(name = "product_id"))
//	public List<CoreItem> getCoreItems() {
//		return coreItems;
//	}
//
//	public void setCoreItems(List<CoreItem> coreItems) {
//		this.coreItems = coreItems;
//	}
	
	@ManyToOne
	@JoinColumn(name = "seller_id")
	public CoreSeller getCoreSeller() {
		return coreSeller;
	}

	public void setCoreSeller(CoreSeller coreSeller) {
		this.coreSeller = coreSeller;
	}
	
	@Column(name = "payment_reference")
	public String getPaymentReference() {
		return paymentReference;
	}

	public void setPaymentReference(String paymentReference) {
		this.paymentReference = paymentReference;
	}

	@Override
	public String toString() {
		return "TrxOrder [orderId=" + orderId + ", orderDate=" + orderDate
				+ ", paidDate=" + paidDate + ", coreUser=" + coreUser
				+ ", coreSeller=" + coreSeller + ", paymentReference="
				+ paymentReference + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((coreSeller == null) ? 0 : coreSeller.hashCode());
		result = prime * result
				+ ((coreUser == null) ? 0 : coreUser.hashCode());
		result = prime * result
				+ ((orderDate == null) ? 0 : orderDate.hashCode());
		result = prime * result + ((orderId == null) ? 0 : orderId.hashCode());
		result = prime * result
				+ ((paidDate == null) ? 0 : paidDate.hashCode());
		result = prime
				* result
				+ ((paymentReference == null) ? 0 : paymentReference.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TrxOrder other = (TrxOrder) obj;
		if (coreSeller == null) {
			if (other.coreSeller != null)
				return false;
		} else if (!coreSeller.equals(other.coreSeller))
			return false;
		if (coreUser == null) {
			if (other.coreUser != null)
				return false;
		} else if (!coreUser.equals(other.coreUser))
			return false;
		if (orderDate == null) {
			if (other.orderDate != null)
				return false;
		} else if (!orderDate.equals(other.orderDate))
			return false;
		if (orderId == null) {
			if (other.orderId != null)
				return false;
		} else if (!orderId.equals(other.orderId))
			return false;
		if (paidDate == null) {
			if (other.paidDate != null)
				return false;
		} else if (!paidDate.equals(other.paidDate))
			return false;
		if (paymentReference == null) {
			if (other.paymentReference != null)
				return false;
		} else if (!paymentReference.equals(other.paymentReference))
			return false;
		return true;
	}
}