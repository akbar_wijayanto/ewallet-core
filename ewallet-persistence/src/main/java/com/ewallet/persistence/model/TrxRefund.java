package com.ewallet.persistence.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

/**
 * @author akbar.wijayanto
 * Date Oct 16, 2015 3:51:04 PM
 */
@Entity
@Table(name = "trx_refund")
@Where(clause = " status <> 'HIST' OR  status is null ")
@SQLDelete(sql = "update trx_refund set status='HIST' where id=?")
public class TrxRefund extends BaseTransaction {
	
	private static final long serialVersionUID = 4165661136334067426L;
	private Long id;
	private String refundId;
	private String description;
	private Date refundDate;
	private BigDecimal amount;
	private CoreSeller coreSeller;
	private CoreUser coreUser;

	@Column(name = "id")
    @GeneratedValue
    @Id
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	@Column(name = "description")
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Column(name = "refund_date")
	public Date getRefundDate() {
		return refundDate;
	}

	public void setRefundDate(Date refundDate) {
		this.refundDate = refundDate;
	}

	@Column(name = "amount")
	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	@ManyToOne
	@JoinColumn(name = "seller_id")
	public CoreSeller getCoreSeller() {
		return coreSeller;
	}

	public void setCoreSeller(CoreSeller coreSeller) {
		this.coreSeller = coreSeller;
	}

	@ManyToOne
	@JoinColumn(name = "user_id")
	public CoreUser getCoreUser() {
		return coreUser;
	}

	public void setCoreUser(CoreUser coreUser) {
		this.coreUser = coreUser;
	}
	
	@Column(name = "refund_id", unique = true)
	public String getRefundId() {
		return refundId;
	}

	public void setRefundId(String refundId) {
		this.refundId = refundId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((amount == null) ? 0 : amount.hashCode());
		result = prime * result
				+ ((coreSeller == null) ? 0 : coreSeller.hashCode());
		result = prime * result
				+ ((coreUser == null) ? 0 : coreUser.hashCode());
		result = prime * result
				+ ((description == null) ? 0 : description.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result
				+ ((refundDate == null) ? 0 : refundDate.hashCode());
		result = prime * result
				+ ((refundId == null) ? 0 : refundId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TrxRefund other = (TrxRefund) obj;
		if (amount == null) {
			if (other.amount != null)
				return false;
		} else if (!amount.equals(other.amount))
			return false;
		if (coreSeller == null) {
			if (other.coreSeller != null)
				return false;
		} else if (!coreSeller.equals(other.coreSeller))
			return false;
		if (coreUser == null) {
			if (other.coreUser != null)
				return false;
		} else if (!coreUser.equals(other.coreUser))
			return false;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (refundDate == null) {
			if (other.refundDate != null)
				return false;
		} else if (!refundDate.equals(other.refundDate))
			return false;
		if (refundId == null) {
			if (other.refundId != null)
				return false;
		} else if (!refundId.equals(other.refundId))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "TrxRefund [id=" + id + ", refundId=" + refundId
				+ ", description=" + description + ", refundDate=" + refundDate
				+ ", amount=" + amount + ", coreSeller=" + coreSeller
				+ ", coreUser=" + coreUser + "]";
	}

}