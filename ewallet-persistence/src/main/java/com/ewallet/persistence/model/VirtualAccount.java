package com.ewallet.persistence.model;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

/**
 * @author akbar.wijayanto
 * Date Oct 16, 2015 3:51:04 PM
 */
@Entity
@Table(name = "virtual_account")
@Where(clause = " status <> 'HIST' OR  status is null ")
@SQLDelete(sql = "update virtual_account set status='HIST' where id=?")
public class VirtualAccount extends BaseTransaction {
	
	private static final long serialVersionUID = 4165661136334067426L;
	private String id;
	private Boolean accountStatus;
	private BigDecimal balance;
	private CoreUser coreUser;

	@Id
	@Column(name = "id")
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	
	@Column(name = "account_status")
	public Boolean getAccountStatus() {
		return accountStatus;
	}
	public void setAccountStatus(Boolean accountStatus) {
		this.accountStatus = accountStatus;
	}
	
	@Column(name = "balance")
	public BigDecimal getBalance() {
		return balance;
	}
	public void setBalance(BigDecimal balance) {
		this.balance = balance;
	}
	
	@ManyToOne
	@JoinColumn(name = "user_id")
	public CoreUser getCoreUser() {
		return coreUser;
	}
	public void setCoreUser(CoreUser coreUser) {
		this.coreUser = coreUser;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((accountStatus == null) ? 0 : accountStatus.hashCode());
		result = prime * result + ((balance == null) ? 0 : balance.hashCode());
		result = prime * result + ((coreUser == null) ? 0 : coreUser.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		VirtualAccount other = (VirtualAccount) obj;
		if (accountStatus == null) {
			if (other.accountStatus != null)
				return false;
		} else if (!accountStatus.equals(other.accountStatus))
			return false;
		if (balance == null) {
			if (other.balance != null)
				return false;
		} else if (!balance.equals(other.balance))
			return false;
		if (coreUser == null) {
			if (other.coreUser != null)
				return false;
		} else if (!coreUser.equals(other.coreUser))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "VirtualAccount [id=" + id + ", accountStatus=" + accountStatus + ", balance=" + balance + ", coreUser="
				+ coreUser + "]";
	}

}