package com.ewallet.persistence.util;



import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.JsonSerializer;
import org.codehaus.jackson.map.SerializerProvider;
import org.springframework.context.i18n.LocaleContextHolder;

import com.ewallet.Constants;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.ResourceBundle;

/**
 * @author : <a href="mailto:ab.annas@gmail.com">Andi Baso Annas</a>
 * @version : 1.0, 8/6/12, 10:09 AM
 */
public class CustomJsonDateSerializer extends JsonSerializer<Date> {
    @Override
    public void serialize(Date aDate, JsonGenerator aJsonGenerator, SerializerProvider aSerializerProvider)
            throws IOException, JsonProcessingException {

        Locale locale = LocaleContextHolder.getLocale();
        SimpleDateFormat format = new SimpleDateFormat(ResourceBundle.getBundle(Constants.BUNDLE_KEY, locale)
                .getString("date.format"));
        String dateString = format.format(aDate);
        aJsonGenerator.writeString(dateString);
    }
}
