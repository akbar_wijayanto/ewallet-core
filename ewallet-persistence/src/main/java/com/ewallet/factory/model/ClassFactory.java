package com.ewallet.factory.model;

public interface ClassFactory {

	public Object getObject(Object oldObject);
	
}
