package com.ewallet.factory.permision;

import org.springframework.security.web.servletapi.SecurityContextHolderAwareRequestWrapper;

public interface ClassFactory {

	public Boolean checkPermission(String className, SecurityContextHolderAwareRequestWrapper securityContextHolderAwareRequestWrapper);
	
}
