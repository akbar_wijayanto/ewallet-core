package com.ewallet.report.dto;

import java.math.BigDecimal;
import java.util.Date;

public class OrderDto {
	private String orderId;
	private String status;
	private Date orderDate;
	private Date paidDate;
	private String userName;
	private BigDecimal totHargaPokok;
	private BigDecimal totHargaJual;
	private BigDecimal grandTotal;
	private BigDecimal grandTotalPokok;
	private String sellerId;
	
	public String getSellerId() {
		return sellerId;
	}
	public void setSellerId(String sellerId) {
		this.sellerId = sellerId;
	}
	public BigDecimal getTotHargaPokok() {
		return totHargaPokok;
	}
	public void setTotHargaPokok(BigDecimal totHargaPokok) {
		this.totHargaPokok = totHargaPokok;
	}
	public BigDecimal getTotHargaJual() {
		return totHargaJual;
	}
	public void setTotHargaJual(BigDecimal totHargaJual) {
		this.totHargaJual = totHargaJual;
	}
	public BigDecimal getGrandTotalPokok() {
		return grandTotalPokok;
	}
	public void setGrandTotalPokok(BigDecimal grandTotalPokok) {
		this.grandTotalPokok = grandTotalPokok;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	public Date getOrderDate() {
		return orderDate;
	}
	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}
	public Date getPaidDate() {
		return paidDate;
	}
	public void setPaidDate(Date paidDate) {
		this.paidDate = paidDate;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public BigDecimal getGrandTotal() {
		return grandTotal;
	}
	public void setGrandTotal(BigDecimal grandTotal) {
		this.grandTotal = grandTotal;
	}

}
