package com.ewallet.report.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

public class BuyerReportDto implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	List<CoreUserTransactionReportDto> list;
	BigDecimal grandTotalAllUser;
	public List<CoreUserTransactionReportDto> getList() {
		return list;
	}
	public void setList(List<CoreUserTransactionReportDto> list) {
		this.list = list;
	}
	public BigDecimal getGrandTotalAllUser() {
		return grandTotalAllUser;
	}
	public void setGrandTotalAllUser(BigDecimal grandTotalAllUser) {
		this.grandTotalAllUser = grandTotalAllUser;
	}

}
