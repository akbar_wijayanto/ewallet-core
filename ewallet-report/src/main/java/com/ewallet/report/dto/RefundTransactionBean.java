package com.ewallet.report.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

public class RefundTransactionBean implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private List<RefundDto> refundDtoBean;
	private BigDecimal grandTotal;

	public BigDecimal getGrandTotal() {
		return grandTotal;
	}

	public void setGrandTotal(BigDecimal grandTotal) {
		this.grandTotal = grandTotal;
	}

	public List<RefundDto> getRefundDtoBean() {
		return refundDtoBean;
	}

	public void setRefundDtoBean(List<RefundDto> refundDtoBean) {
		this.refundDtoBean = refundDtoBean;
	}
	
}
