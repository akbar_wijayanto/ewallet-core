package com.ewallet.report.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

public class OrderTransactionBean implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	List<OrderDto> orderDtoBean;
	BigDecimal grandTotal;
	BigDecimal grandTotalPokok;
	
	public BigDecimal getGrandTotalPokok() {
		return grandTotalPokok;
	}
	public void setGrandTotalPokok(BigDecimal grandTotalPokok) {
		this.grandTotalPokok = grandTotalPokok;
	}
	public List<OrderDto> getOrderDtoBean() {
		return orderDtoBean;
	}
	public void setOrderDtoBean(List<OrderDto> orderDtoBean) {
		this.orderDtoBean = orderDtoBean;
	}
	public BigDecimal getGrandTotal() {
		return grandTotal;
	}
	public void setGrandTotal(BigDecimal grandTotal) {
		this.grandTotal = grandTotal;
	}

}
