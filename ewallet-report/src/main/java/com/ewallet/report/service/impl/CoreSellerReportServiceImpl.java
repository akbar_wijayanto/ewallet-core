/**
 * 
 */
package com.ewallet.report.service.impl;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ewallet.Constants;
import com.ewallet.persistence.model.CoreSeller;
import com.ewallet.persistence.model.CoreSystem;
import com.ewallet.persistence.model.TrxOrder;
import com.ewallet.persistence.model.TrxOrderItemDetail;
import com.ewallet.persistence.model.TrxSellerPayment;
import com.ewallet.report.dto.CoreSellerSettlementReportDto;
import com.ewallet.report.dto.CoreSellerTransactionReportDto;
import com.ewallet.report.dto.RefundDto;
import com.ewallet.report.dto.SellerTransactionBean;
import com.ewallet.report.service.CoreSellerReportService;
import com.ewallet.service.CoreSellerManager;
import com.ewallet.service.CoreSystemManager;
import com.ewallet.service.TrxOrderManager;
import com.ewallet.service.TrxSellerPaymentManager;

/**
 * @author akbar.wijayanto
 * Date Jan 4, 2016 5:07:45 PM
 */
@Service
public class CoreSellerReportServiceImpl implements CoreSellerReportService {

	@Autowired
	private TrxOrderManager trxOrderManager;
	
	@Autowired
	private CoreSystemManager coreSystemManager;
	
	@Autowired
	private CoreSellerManager coreSellerManager;
	
	@Autowired
	private TrxSellerPaymentManager trxSellerPayment;
	
	@Override
	public List<CoreSellerSettlementReportDto> getAllSellerSettlementReport() {
		List<CoreSeller> coreSellers = coreSellerManager.getAllLive();		
		BigDecimal grandTotal = BigDecimal.ZERO;
		//List<TrxOrder> last7DaysOrders = trxOrderManager.getLast7DaysOrders();
		Date date = new Date();
		List<TrxOrder> last7DaysOrders = new ArrayList<TrxOrder>();
		if (date.equals("Thursday")){
			last7DaysOrders = trxOrderManager.getLast7DaysOrders();
		}else {
			last7DaysOrders = trxOrderManager.getLast7DaysOrdersForSettlement();
		}
				
		for (TrxOrder trxOrder : last7DaysOrders) {
			for (TrxOrderItemDetail trxOrderItemDetail : trxOrder.getTrxOrderItemDetails()) {
				BigDecimal orderAmount = trxOrderItemDetail.getPk().getCoreItem().getCostPrice().multiply(new BigDecimal(trxOrderItemDetail.getQuantity()));
				grandTotal = grandTotal.add(orderAmount);
			}
		}
		List<CoreSellerSettlementReportDto> settlementReportDtos = new ArrayList<CoreSellerSettlementReportDto>();
		/*if (grandTotal.compareTo(new BigDecimal("0"))==0){
			List<CoreSellerSettlementReportDto> settlementReportDtos1 = new ArrayList<CoreSellerSettlementReportDto>();
			return settlementReportDtos1;
		}else {*/
		String paidFlag = "";		
		if(last7DaysOrders != null){
		for (CoreSeller coreSeller : coreSellers) {
			CoreSellerSettlementReportDto dto = new CoreSellerSettlementReportDto();
			List<TrxOrder> last7DaysSellerOrders = trxOrderManager.getLast7DaysOrdersBySeller(coreSeller.getSellerId());
			BigDecimal totalTransaction = BigDecimal.ZERO;
			boolean flag = false;
			for (TrxOrder trxOrder : last7DaysSellerOrders) {
				if (trxOrder.getPaidDate() != null) {
					flag = true;
				}
				for (TrxOrderItemDetail trxOrderItemDetail : trxOrder.getTrxOrderItemDetails()) {
					BigDecimal orderAmount = trxOrderItemDetail.getPk().getCoreItem().getCostPrice().multiply(new BigDecimal(trxOrderItemDetail.getQuantity()));
					totalTransaction = totalTransaction.add(orderAmount);
				}				
			}
			if(flag){
				paidFlag="Paid";
			}else{
				 if( totalTransaction == BigDecimal.ZERO){
					 paidFlag="No Settlement";
				 }else{
					 paidFlag="Unpaid";
				 }
			}
			BigDecimal percentage = new BigDecimal("0");
			if (grandTotal.compareTo(new BigDecimal("0"))==0){
				percentage = new BigDecimal("0");
			}else {
			percentage = totalTransaction.divide(grandTotal, 2, RoundingMode.HALF_UP);
			percentage = percentage.multiply(new BigDecimal(100)).divide(new BigDecimal(100), 2, RoundingMode.HALF_UP);
			}
			CoreSystem coreSystem = coreSystemManager.get(Constants.SYSTEM);
			BigDecimal biaya = percentage.multiply(coreSystem.getServiceCash());
			BigDecimal totalAmountPaid = totalTransaction.subtract(biaya);
			dto.setSellerId(coreSeller.getSellerId());
			dto.setSellerName(coreSeller.getFirstName().concat(" ").concat(coreSeller.getLastName()));
			dto.setGrandTotal(grandTotal);
			dto.setTotalTransaction(totalTransaction);
			dto.setPercentage(percentage);
			dto.setServiceCash(coreSystem.getServiceCash());
			//dto.setServiceCash(serviceCash);
			dto.setCharge(biaya);
			dto.setTotalAmountPaid(totalAmountPaid);
			dto.setAccountNumber(coreSeller.getAccountNumber());
			dto.setFlagPaid(paidFlag);			
			settlementReportDtos.add(dto);
		}
		}
		return settlementReportDtos;
		//}
	}

	@Override
	public SellerTransactionBean getAllSellerTransactionReport() {
		List<TrxOrder> t = trxOrderManager.getLast7DaysOrders();
		List<CoreSellerTransactionReportDto> beanList = new ArrayList<CoreSellerTransactionReportDto>();
		SellerTransactionBean dto = new SellerTransactionBean();
		BigDecimal gTot = new BigDecimal("0");
		for (TrxOrder tOrder : t) {
			for (TrxOrderItemDetail bn : tOrder.getTrxOrderItemDetails()) {
				CoreSellerTransactionReportDto bean = new CoreSellerTransactionReportDto();
				bean.setSellerId(tOrder.getCoreSeller().getSellerId());
				bean.setTransactionDate(getDate(tOrder.getOrderDate()));
				bean.setItemId(bn.getPk().getCoreItem().getItemId());
				bean.setItemName(bn.getPk().getCoreItem().getItemName());
				bean.setQuantity(bn.getQuantity());
				bean.setCostPrice(bn.getPk().getCoreItem().getCostPrice());
				BigDecimal total = BigDecimal.valueOf(bean.getQuantity()).multiply(bean.getCostPrice());
				bean.setTotal(total);
				gTot = gTot.add(total);
				bean.setGrandTotal(gTot);
				bean.setReportDate(getDateToString(new Date()));
				beanList.add(bean);
			}
			
		}
		dto.setCoreSellerTransactionReportDtoBean(beanList);
		dto.setGrandTotal(gTot);
		return dto;
	}

	@Override
	public List<CoreSellerSettlementReportDto> getSellerSettlementReportBySellerId(
			String sellerId) {

		BigDecimal grandTotal = BigDecimal.ZERO;
		List<TrxOrder> last7DaysOrders = trxOrderManager.getLast7DaysOrders();
		for (TrxOrder trxOrder : last7DaysOrders) {
			for (TrxOrderItemDetail trxOrderItemDetail : trxOrder.getTrxOrderItemDetails()) {
				BigDecimal orderAmount = trxOrderItemDetail.getPk().getCoreItem().getCostPrice().multiply(new BigDecimal(trxOrderItemDetail.getQuantity()));
				grandTotal = grandTotal.add(orderAmount);
			}
		}
		System.out.println("grandTotal : "+grandTotal);
		System.out.println("======================================================");
		
		CoreSeller coreSeller = coreSellerManager.getByQRCode(sellerId);
		List<CoreSellerSettlementReportDto> settlementReportDtos = new ArrayList<CoreSellerSettlementReportDto>();
		
		CoreSellerSettlementReportDto dto = new CoreSellerSettlementReportDto();
		List<TrxOrder> last7DaysSellerOrders = trxOrderManager.getLast7DaysOrdersBySeller(coreSeller.getSellerId());
		System.out.println("Seller : "+coreSeller.getSellerId());
		BigDecimal totalTransaction = BigDecimal.ZERO;
		for (TrxOrder trxOrder : last7DaysSellerOrders) {
			for (TrxOrderItemDetail trxOrderItemDetail : trxOrder.getTrxOrderItemDetails()) {
				BigDecimal orderAmount = trxOrderItemDetail.getPk().getCoreItem().getCostPrice().multiply(new BigDecimal(trxOrderItemDetail.getQuantity()));
				totalTransaction = totalTransaction.add(orderAmount);
			}
		}
		
		System.out.println("totalTransaction : "+totalTransaction);
		
		BigDecimal percentage = totalTransaction.divide(grandTotal, 2, RoundingMode.HALF_UP);
		percentage = percentage.multiply(new BigDecimal(100)).divide(new BigDecimal(100), 2, RoundingMode.HALF_UP);
		System.out.println("percentage : "+percentage);
		
		CoreSystem coreSystem = coreSystemManager.get(Constants.SYSTEM);
		System.out.println("service cash : "+coreSystem.getServiceCash());
		
		BigDecimal biaya = percentage.multiply(coreSystem.getServiceCash());
		System.out.println("Biaya yang harus dibayar : "+biaya);
		
		BigDecimal totalAmountPaid = totalTransaction.subtract(biaya);
		System.out.println("Biaya yang ditransfer : "+totalAmountPaid);
		System.out.println("======================================================");
		
		dto.setSellerId(coreSeller.getSellerId());
		dto.setSellerName(coreSeller.getFirstName().concat(" ").concat(coreSeller.getLastName()));
		dto.setGrandTotal(grandTotal);
		dto.setTotalTransaction(totalTransaction);
		dto.setPercentage(percentage);
		dto.setServiceCash(coreSystem.getServiceCash());
		dto.setCharge(biaya);
		dto.setTotalAmountPaid(totalAmountPaid);
		dto.setAccountNumber(coreSeller.getAccountNumber());
		
		settlementReportDtos.add(dto);
		
		return settlementReportDtos;
	}
	
	@Override
	public List<CoreSellerSettlementReportDto> getAllSellerSettlementPayment() {
		List<CoreSeller> coreSellers = coreSellerManager.getAllLive();
		List<TrxSellerPayment> trxPayment = trxSellerPayment.getAllSellerPayment();
		List<String> lsellerId = new ArrayList<String>();
		for (TrxSellerPayment tPayment : trxPayment) {
			String idSeller = tPayment.getCoreSeller().getId().toString();
			lsellerId.add(idSeller);
		}
		
		BigDecimal grandTotal = BigDecimal.ZERO;
		List<TrxOrder> last7DaysOrders = trxOrderManager.getLast7DaysOrders();
		for (TrxOrder trxOrder : last7DaysOrders) {
			for (TrxOrderItemDetail trxOrderItemDetail : trxOrder.getTrxOrderItemDetails()) {
				BigDecimal orderAmount = trxOrderItemDetail.getPk().getCoreItem().getCostPrice().multiply(new BigDecimal(trxOrderItemDetail.getQuantity()));
				grandTotal = grandTotal.add(orderAmount);
			}
		}
		System.out.println("grandTotal : "+grandTotal);
		System.out.println("======================================================");
		
		List<CoreSellerSettlementReportDto> settlementReportDtos = new ArrayList<CoreSellerSettlementReportDto>();
		
		for (CoreSeller coreSeller : coreSellers) {
			boolean flag = false;
			CoreSellerSettlementReportDto dto = new CoreSellerSettlementReportDto();
			List<TrxOrder> last7DaysSellerOrders = trxOrderManager.getLast7DaysOrdersBySeller(coreSeller.getSellerId());
			System.out.println("Seller : "+coreSeller.getSellerId());
			BigDecimal totalTransaction = BigDecimal.ZERO;
			for (TrxOrder trxOrder : last7DaysSellerOrders) {
				if ((trxOrder.getPaidDate() != null) || (Collections.frequency(lsellerId, trxOrder.getCoreSeller().getId().toString())>0) ) {
					flag = true;
				}
				for (TrxOrderItemDetail trxOrderItemDetail : trxOrder.getTrxOrderItemDetails()) {
					BigDecimal orderAmount = trxOrderItemDetail.getPk().getCoreItem().getCostPrice().multiply(new BigDecimal(trxOrderItemDetail.getQuantity()));
					totalTransaction = totalTransaction.add(orderAmount);
				}
			}
			if (flag == true || totalTransaction == BigDecimal.ZERO ) {
				continue;
			}
			System.out.println("totalTransaction : "+totalTransaction);
			
			BigDecimal percentage = totalTransaction.divide(grandTotal, 2, RoundingMode.HALF_UP);
			percentage = percentage.multiply(new BigDecimal(100)).divide(new BigDecimal(100), 2, RoundingMode.HALF_UP);
			System.out.println("percentage : "+percentage);
			
			CoreSystem coreSystem = coreSystemManager.get(Constants.SYSTEM);
			System.out.println("service cash : "+coreSystem.getServiceCash());
			
			BigDecimal biaya = percentage.multiply(coreSystem.getServiceCash());
			System.out.println("Biaya yang harus dibayar : "+biaya);
			
			BigDecimal totalAmountPaid = totalTransaction.subtract(biaya);
			System.out.println("Biaya yang ditransfer : "+totalAmountPaid);
			System.out.println("======================================================");
			
			dto.setSellerId(coreSeller.getSellerId());
			dto.setSellerName(coreSeller.getFirstName().concat(" ").concat(coreSeller.getLastName()));
			dto.setGrandTotal(grandTotal);
			dto.setTotalTransaction(totalTransaction);
			dto.setPercentage(percentage);
			dto.setServiceCash(coreSystem.getServiceCash());
			dto.setCharge(biaya);
			dto.setTotalAmountPaid(totalAmountPaid);
			dto.setAccountNumber(coreSeller.getAccountNumber());
			
			settlementReportDtos.add(dto);
		}
		return settlementReportDtos;
	}
	
	private Timestamp getDate(Date orderDate) {
		Calendar c = Calendar.getInstance();
		c.setTime(orderDate);
		long time = c.getTimeInMillis();
		return new Timestamp(time);
	}
	
	private String getDateToString(Date date) {
		SimpleDateFormat sdf = new SimpleDateFormat("EEE MMM dd kk:mm:ss z yyyy");
		String d = sdf.format(date);
		return d;
	}

	@Override
	public SellerTransactionBean getAllSellerTransactionPeriodicReport(String startDate, String endDate) {
		List<TrxOrder> t = trxOrderManager.getPeriodOrders(startDate, endDate);
		List<CoreSellerTransactionReportDto> beanList = new ArrayList<CoreSellerTransactionReportDto>();
		SellerTransactionBean dto = new SellerTransactionBean();
		BigDecimal gTot = new BigDecimal("0");
		for (TrxOrder tOrder : t) {
			for (TrxOrderItemDetail bn : tOrder.getTrxOrderItemDetails()) {
				CoreSellerTransactionReportDto bean = new CoreSellerTransactionReportDto();
				bean.setSellerId(tOrder.getCoreSeller().getSellerId());
				bean.setTransactionDate(getDate(tOrder.getOrderDate()));
				bean.setItemId(bn.getPk().getCoreItem().getItemId());
				bean.setItemName(bn.getPk().getCoreItem().getItemName());
				bean.setQuantity(bn.getQuantity());
				//bean.setCostPrice(bn.getPk().getCoreItem().getSellingPrice());
				bean.setCostPrice(bn.getPk().getCoreItem().getCostPrice());
				BigDecimal total = BigDecimal.valueOf(bean.getQuantity()).multiply(bean.getCostPrice());
				bean.setTotal(total);
				gTot = gTot.add(total);
				bean.setGrandTotal(gTot);
				bean.setReportDate(getDateToString(new Date()));
				beanList.add(bean);
			}
			
		}
		dto.setCoreSellerTransactionReportDtoBean(beanList);
		dto.setGrandTotal(gTot);
		return dto;
	}

	@Override
	public SellerTransactionBean getAllSellerTransactionPeriodicReportBySellerId(String sellerId, String startDate, String endDate) {
		List<TrxOrder> t = trxOrderManager.getPeriodOrdersBySellerId(sellerId, startDate, endDate);
		List<CoreSellerTransactionReportDto> beanList = new ArrayList<CoreSellerTransactionReportDto>();
		SellerTransactionBean dto = new SellerTransactionBean();
		BigDecimal gTot = new BigDecimal("0");
		for (TrxOrder tOrder : t) {
			for (TrxOrderItemDetail bn : tOrder.getTrxOrderItemDetails()) {
				CoreSellerTransactionReportDto bean = new CoreSellerTransactionReportDto();
				bean.setSellerId(tOrder.getCoreSeller().getSellerId());
				bean.setTransactionDate(getDate(tOrder.getOrderDate()));
				bean.setItemId(bn.getPk().getCoreItem().getItemId());
				bean.setItemName(bn.getPk().getCoreItem().getItemName());
				bean.setQuantity(bn.getQuantity());
				//bean.setCostPrice(bn.getPk().getCoreItem().getSellingPrice());
				bean.setCostPrice(bn.getPk().getCoreItem().getCostPrice());
				BigDecimal total = BigDecimal.valueOf(bean.getQuantity()).multiply(bean.getCostPrice());
				bean.setTotal(total);
				gTot = gTot.add(total);
				bean.setGrandTotal(gTot);
				bean.setReportDate(getDateToString(new Date()));
				beanList.add(bean);
			}
			
		}
		dto.setCoreSellerTransactionReportDtoBean(beanList);
		dto.setGrandTotal(gTot);
		return dto;
	}

	@Override
	public List<CoreSellerSettlementReportDto> getSettlementInfoPeriodic(
			String startDat, String endDat) {
		BigDecimal grandTotal = BigDecimal.ZERO;
		List<TrxOrder> listOrders = trxOrderManager.getListOrdersForSettlement(startDat, endDat);
		for (TrxOrder trxOrder : listOrders) {
			for (TrxOrderItemDetail trxOrderItemDetail : trxOrder.getTrxOrderItemDetails()) {
				BigDecimal orderAmount = trxOrderItemDetail.getPk().getCoreItem().getCostPrice().multiply(new BigDecimal(trxOrderItemDetail.getQuantity()));
				grandTotal = grandTotal.add(orderAmount);
			}
		}
		System.out.println("grandTotal : "+grandTotal);
		System.out.println("======================================================");
		
		List<CoreSeller> coreSellers = coreSellerManager.getAllLive();
		List<TrxSellerPayment> trxPayment = trxSellerPayment.getAllSellerPayment();
		List<String> lsellerId = new ArrayList<String>();
		for (TrxSellerPayment tPayment : trxPayment) {
			String idSeller = tPayment.getCoreSeller().getId().toString();
			lsellerId.add(idSeller);
		}
		List<CoreSellerSettlementReportDto> settlementReportDtos = new ArrayList<CoreSellerSettlementReportDto>();
		try {
			for (CoreSeller coreSeller : coreSellers) {
				//System.out.println(coreSeller.getId()+" - "+coreSeller.getStatus());
				boolean flag = false;
				CoreSellerSettlementReportDto dto = new CoreSellerSettlementReportDto();
				List<TrxOrder> listSellerOrders = trxOrderManager.getPeriodOrdersBySeller(coreSeller.getUsername(),startDat,endDat);
				System.out.println("Seller : "+coreSeller.getSellerId());
				BigDecimal totalTransaction = BigDecimal.ZERO;
				for (TrxOrder trxOrder : listSellerOrders) {
					if (trxOrder.getPaidDate() != null || (Collections.frequency(lsellerId, trxOrder.getCoreSeller().getId().toString())>0)) {
						flag = true;
					}
					for (TrxOrderItemDetail trxOrderItemDetail : trxOrder.getTrxOrderItemDetails()) {
						BigDecimal orderAmount = trxOrderItemDetail.getPk().getCoreItem().getCostPrice().multiply(new BigDecimal(trxOrderItemDetail.getQuantity()));
						totalTransaction = totalTransaction.add(orderAmount);
					}
				}
				if (flag == true || totalTransaction == BigDecimal.ZERO ) {
					continue;
				}
				System.out.println("totalTransaction : "+totalTransaction);
				
				
				BigDecimal percentage = new BigDecimal("0");
				if(grandTotal.compareTo(new BigDecimal("0"))>0){
				percentage = totalTransaction.divide(grandTotal, 2, RoundingMode.HALF_UP);
				percentage = percentage.multiply(new BigDecimal(100)).divide(new BigDecimal(100), 2, RoundingMode.HALF_UP);
				System.out.println("percentage : "+percentage);
				}
				
				BigDecimal percentage2 = new BigDecimal("0");
				if(grandTotal.compareTo(new BigDecimal("0"))>0){
				percentage2 = totalTransaction.setScale(5).divide(grandTotal,5);
				percentage2 = percentage2.multiply(new BigDecimal(100)).divide(new BigDecimal(100));
				System.out.println("percentage2 : "+percentage2);
				}
			
				CoreSystem coreSystem = coreSystemManager.get(Constants.SYSTEM);
				System.out.println("service cash : "+coreSystem.getServiceCash());
				
				BigDecimal biaya = percentage2.multiply(coreSystem.getServiceCash());
				System.out.println("Biaya yang harus dibayar : "+biaya);
				
				BigDecimal totalAmountPaid = totalTransaction.subtract(biaya);
				System.out.println("Biaya yang ditransfer : "+totalAmountPaid);
				System.out.println("======================================================");
				
				dto.setSellerId(coreSeller.getSellerId());
				dto.setSellerName(coreSeller.getFirstName().concat(" ").concat(coreSeller.getLastName()));
				dto.setGrandTotal(grandTotal);
				dto.setTotalTransaction(totalTransaction);
				dto.setPercentage(percentage);
				dto.setServiceCash(coreSystem.getServiceCash());
				dto.setCharge(biaya);
				dto.setTotalAmountPaid(totalAmountPaid);
				dto.setAccountNumber(coreSeller.getAccountNumber());
				dto.setStartDat(startDat);
				dto.setEndDat(endDat);
				
				settlementReportDtos.add(dto);
				
			}
			return settlementReportDtos;
		} catch (Exception e) {
			return settlementReportDtos;
		}
		
	}

}
