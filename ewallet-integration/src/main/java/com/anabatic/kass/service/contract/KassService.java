
package com.anabatic.kass.service.contract;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.xml.bind.annotation.XmlSeeAlso;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.4-b01
 * Generated source version: 2.2
 * 
 */
@WebService(name = "kassService", targetNamespace = "http://kass.anabatic.com/service/contract")
@SOAPBinding(parameterStyle = SOAPBinding.ParameterStyle.BARE)
@XmlSeeAlso({
    com.anabatic.kass.service.contract.ObjectFactory.class,
    com.anabatic.kass.service.contract.bean.ObjectFactory.class
})
public interface KassService {


    /**
     * 
     * @param createAccountRequest
     * @return
     *     returns com.anabatic.kass.service.contract.CreateAccountResponse
     */
    @WebMethod(operationName = "CreateAccount")
    @WebResult(name = "CreateAccountResponse", targetNamespace = "http://kass.anabatic.com/service/contract", partName = "CreateAccountResponse")
    public CreateAccountResponse createAccount(
        @WebParam(name = "CreateAccountRequest", targetNamespace = "http://kass.anabatic.com/service/contract", partName = "CreateAccountRequest")
        CreateAccountRequest createAccountRequest);

    /**
     * 
     * @param payrollPaymentTransactionRequest
     * @return
     *     returns com.anabatic.kass.service.contract.PayrollPaymentTransactionResponse
     */
    @WebMethod(operationName = "PayrollPaymentTransaction")
    @WebResult(name = "PayrollPaymentTransactionResponse", targetNamespace = "http://kass.anabatic.com/service/contract", partName = "PayrollPaymentTransactionResponse")
    public PayrollPaymentTransactionResponse payrollPaymentTransaction(
        @WebParam(name = "PayrollPaymentTransactionRequest", targetNamespace = "http://kass.anabatic.com/service/contract", partName = "PayrollPaymentTransactionRequest")
        PayrollPaymentTransactionRequest payrollPaymentTransactionRequest);

    /**
     * 
     * @param accountInformationRequest
     * @return
     *     returns com.anabatic.kass.service.contract.AccountInformationResponse
     */
    @WebMethod(operationName = "AccountInformation")
    @WebResult(name = "AccountInformationResponse", targetNamespace = "http://kass.anabatic.com/service/contract", partName = "AccountInformationResponse")
    public AccountInformationResponse accountInformation(
        @WebParam(name = "AccountInformationRequest", targetNamespace = "http://kass.anabatic.com/service/contract", partName = "AccountInformationRequest")
        AccountInformationRequest accountInformationRequest);

    /**
     * 
     * @param registrationRequest
     * @return
     *     returns com.anabatic.kass.service.contract.RegistrationResponse
     */
    @WebMethod(operationName = "Registration")
    @WebResult(name = "RegistrationResponse", targetNamespace = "http://kass.anabatic.com/service/contract", partName = "RegistrationResponse")
    public RegistrationResponse registration(
        @WebParam(name = "RegistrationRequest", targetNamespace = "http://kass.anabatic.com/service/contract", partName = "RegistrationRequest")
        RegistrationRequest registrationRequest);

    /**
     * 
     * @param updateLimitPayrollRequest
     * @return
     *     returns com.anabatic.kass.service.contract.UpdateLimitPayrollResponse
     */
    @WebMethod(operationName = "UpdateLimitPayroll")
    @WebResult(name = "UpdateLimitPayrollResponse", targetNamespace = "http://kass.anabatic.com/service/contract", partName = "UpdateLimitPayrollResponse")
    public UpdateLimitPayrollResponse updateLimitPayroll(
        @WebParam(name = "UpdateLimitPayrollRequest", targetNamespace = "http://kass.anabatic.com/service/contract", partName = "UpdateLimitPayrollRequest")
        UpdateLimitPayrollRequest updateLimitPayrollRequest);

    /**
     * 
     * @param getLimitPayrollRequest
     * @return
     *     returns com.anabatic.kass.service.contract.GetLimitPayrollResponse
     */
    @WebMethod(operationName = "GetLimitPayroll")
    @WebResult(name = "GetLimitPayrollResponse", targetNamespace = "http://kass.anabatic.com/service/contract", partName = "GetLimitPayrollResponse")
    public GetLimitPayrollResponse getLimitPayroll(
        @WebParam(name = "GetLimitPayrollRequest", targetNamespace = "http://kass.anabatic.com/service/contract", partName = "GetLimitPayrollRequest")
        GetLimitPayrollRequest getLimitPayrollRequest);

    /**
     * 
     * @param paymentTransactionRequest
     * @return
     *     returns com.anabatic.kass.service.contract.PaymentTransactionResponse
     */
    @WebMethod(operationName = "PaymentTransaction")
    @WebResult(name = "PaymentTransactionResponse", targetNamespace = "http://kass.anabatic.com/service/contract", partName = "PaymentTransactionResponse")
    public PaymentTransactionResponse paymentTransaction(
        @WebParam(name = "PaymentTransactionRequest", targetNamespace = "http://kass.anabatic.com/service/contract", partName = "PaymentTransactionRequest")
        PaymentTransactionRequest paymentTransactionRequest);

    /**
     * 
     * @param refundTransactionRequest
     * @return
     *     returns com.anabatic.kass.service.contract.RefundTransactionResponse
     */
    @WebMethod(operationName = "RefundTransaction")
    @WebResult(name = "RefundTransactionResponse", targetNamespace = "http://kass.anabatic.com/service/contract", partName = "RefundTransactionResponse")
    public RefundTransactionResponse refundTransaction(
        @WebParam(name = "RefundTransactionRequest", targetNamespace = "http://kass.anabatic.com/service/contract", partName = "RefundTransactionRequest")
        RefundTransactionRequest refundTransactionRequest);

}
