/**
 * 
 */
package com.anabatic.retail.dto;

import java.math.BigDecimal;

/**
 * @author akbar.wijayanto
 * Date Oct 27, 2015 11:24:17 PM
 */
public class AccountDto {
	
	private Long customerId;
	private String accountNumber;
	private boolean accountStatus;
	private BigDecimal amount;
	private BigDecimal limitDailyTransaction;
	private BigDecimal limitSingletransaction;
	private BigDecimal limitTransactionFrequency;
	private String nickname;
	private String accountType;
	private boolean allowedPayroll;
	private BigDecimal limitPayroll;
	private String company;
	private boolean asMember;
	
	public Long getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}
	public String getAccountNumber() {
		return accountNumber;
	}
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}
	public boolean isAccountStatus() {
		return accountStatus;
	}
	public void setAccountStatus(boolean accountStatus) {
		this.accountStatus = accountStatus;
	}
	public BigDecimal getAmount() {
		return amount;
	}
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	public BigDecimal getLimitDailyTransaction() {
		return limitDailyTransaction;
	}
	public void setLimitDailyTransaction(BigDecimal limitDailyTransaction) {
		this.limitDailyTransaction = limitDailyTransaction;
	}
	public BigDecimal getLimitSingletransaction() {
		return limitSingletransaction;
	}
	public void setLimitSingletransaction(BigDecimal limitSingletransaction) {
		this.limitSingletransaction = limitSingletransaction;
	}
	public BigDecimal getLimitTransactionFrequency() {
		return limitTransactionFrequency;
	}
	public void setLimitTransactionFrequency(BigDecimal limitTransactionFrequency) {
		this.limitTransactionFrequency = limitTransactionFrequency;
	}
	public String getNickname() {
		return nickname;
	}
	public void setNickname(String nickname) {
		this.nickname = nickname;
	}
	public String getAccountType() {
		return accountType;
	}
	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}
	public boolean isAllowedPayroll() {
		return allowedPayroll;
	}
	public void setAllowedPayroll(boolean allowedPayroll) {
		this.allowedPayroll = allowedPayroll;
	}
	public BigDecimal getLimitPayroll() {
		return limitPayroll;
	}
	public void setLimitPayroll(BigDecimal limitPayroll) {
		this.limitPayroll = limitPayroll;
	}
	public String getCompany() {
		return company;
	}
	public void setCompany(String company) {
		this.company = company;
	}
	public boolean isAsMember() {
		return asMember;
	}
	public void setAsMember(boolean asMember) {
		this.asMember = asMember;
	}
	
}
