/**
 * 
 */
package com.anabatic.retail.dto;

import java.math.BigDecimal;

/**
 * @author akbar.wijayanto
 * Date Nov 2, 2015 10:10:25 PM
 */
public class PaymentDto {
	
	private String id;
	private String accountFrom;
	private String description;
	private BigDecimal amount;
	private String reference;
	
	public String getAccountFrom() {
		return accountFrom;
	}
	public void setAccountFrom(String accountFrom) {
		this.accountFrom = accountFrom;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public BigDecimal getAmount() {
		return amount;
	}
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getReference() {
		return reference;
	}
	public void setReference(String reference) {
		this.reference = reference;
	}
	
}
