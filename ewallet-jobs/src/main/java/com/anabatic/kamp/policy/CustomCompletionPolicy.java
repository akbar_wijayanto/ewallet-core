package com.anabatic.kamp.policy;

import org.springframework.batch.repeat.CompletionPolicy;
import org.springframework.batch.repeat.RepeatContext;
import org.springframework.batch.repeat.policy.CompletionPolicySupport;

public class CustomCompletionPolicy extends CompletionPolicySupport implements CompletionPolicy{

	@Override
	public boolean isComplete(RepeatContext context) {
		return false;
	}
	
}
