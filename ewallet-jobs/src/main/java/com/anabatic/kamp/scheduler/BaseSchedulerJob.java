/**
 * 
 */
package com.anabatic.kamp.scheduler;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.context.ApplicationContext;


/**
 * Base class untuk schedule handler. Ada method <code>getApplicationContext</code> untuk mendapatkan
 * bean dari Spring, karena tidak bisa memakai <code>@Autowired</code> dari Spring.
 * 
 * @author agung.kurniawan
 * Date Sep 18, 2013
 */
public abstract class BaseSchedulerJob implements Job {

	private static final String APPLICATION_CONTEXT_KEY = "applicationContext";

	private JobExecutionContext jobContext;

	/**
	 * Gunakan method ini untuk mendapatkan bean dari Spring. <code>@Autowired</code> tidak bisa bekerja 
	 * karena spring context dan quartz bekerja pada singleton yang berbeda yang berjalan secara besamaan.
	 * 
	 * @param context
	 * @return
	 * @throws Exception
	 */
	private ApplicationContext getJobContext(JobExecutionContext context )
		    throws Exception {
        ApplicationContext appCtx = null;
        appCtx = (ApplicationContext)context.getScheduler().getContext().get(APPLICATION_CONTEXT_KEY);
        if (appCtx == null) {
            throw new JobExecutionException("No application context available in scheduler context for key \"" + APPLICATION_CONTEXT_KEY + "\"");
        }
        return appCtx;
    }

	@Override
	public void execute(JobExecutionContext context)
			throws JobExecutionException {
		try{
			this.jobContext = context;
			beforeExecute();
			onExecute();
			afterExecute();
		} catch (Exception e){
			e.printStackTrace();
		}
	}
	
	protected ApplicationContext getApplicationContext() throws Exception{
		ApplicationContext appCtx = getJobContext(this.jobContext);
		return appCtx;
	}
	
	protected void onExecute(){};
	protected void beforeExecute(){};
	protected void afterExecute(){};
	
}
