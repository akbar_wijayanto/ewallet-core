/**
 * 
 */
package com.anabatic.kamp.scheduler;

import org.quartz.CronTrigger;
import org.springframework.beans.BeansException;
import org.springframework.scheduling.quartz.CronTriggerBean;

/**
 * @author dimas.sulistyono
 *
 */
public class AutoDebetJob extends BaseSchedulerJob {
	private CronTrigger trigger;
	private CronScheduler cronScheduler;
	
	@Override
	protected void beforeExecute() {
		try {
			trigger = getApplicationContext().getBean(CronTriggerBean.class);
			cronScheduler = getApplicationContext().getBean(CronScheduler.class);
		} catch (BeansException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	protected void onExecute() {
		System.out.println("Harusnya jalan setelahnya");
		System.out.println(trigger.getCronExpression());
		cronScheduler.setNewCron(trigger);
		System.out.println(trigger.getCronExpression());
	}
}
