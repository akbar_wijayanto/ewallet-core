/**
 * 
 */
package com.anabatic.kamp.batch;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.anabatic.kamp.enumeration.ReportParameterEnum;
import com.anabatic.kamp.persistence.model.CoreReport;


/**
 * 
 * @version 1.0, Oct 6, 2015 10:13:38 AM
 */
public class ReportGeneratorRowMapper implements RowMapper<CoreReport>{

	@Override
	public CoreReport mapRow(ResultSet rs, int rowNum) throws SQLException {
		CoreReport coreReport = new CoreReport();
		coreReport.setId(rs.getLong("id"));
		coreReport.setDescription(rs.getString("description"));
		coreReport.setReportFileName(rs.getString("report_file_name"));
		coreReport.setReportName(rs.getString("report_name"));
		coreReport.setParameter(ReportParameterEnum.get(rs.getInt("report_parameter")));
		return coreReport;
	}
}
