package com.anabatic.kamp.batch;


import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.anabatic.kamp.persistence.model.CoreSystem;


public class CoreSystemRowMapper implements RowMapper<CoreSystem>{

	@Override
	public CoreSystem mapRow(ResultSet rs, int rowNum) throws SQLException {
		CoreSystem system = new CoreSystem();
		system.setId(rs.getString("id"));
		system.setNextDate(rs.getDate("next_date"));
		system.setPreviousDate(rs.getDate("previous_date"));
		system.setTodayDate(rs.getDate("today_date"));
		system.setSystemStatus(rs.getInt("system_status"));
		return system;
	}

}
