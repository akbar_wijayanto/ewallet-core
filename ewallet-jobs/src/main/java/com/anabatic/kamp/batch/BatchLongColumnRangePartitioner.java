package com.anabatic.kamp.batch;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.batch.core.partition.support.Partitioner;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;

/**
 * Simple minded partitioner for a range of values of a column in a database
 * table. Works best if the values are uniformly distributed (e.g.
 * auto-generated primary key values).
 *
 */
public class BatchLongColumnRangePartitioner implements Partitioner{

    protected final Log log = LogFactory.getLog(BatchLongColumnRangePartitioner.class);

    private JdbcTemplate jdbcTemplate;

    private String table;

    private String column;

    /**
     * The name of the SQL table the data are in.
     *
     * @param table the name of the table
     */
    public void setTable(String table) {
        this.table = table;
    }

    /**
     * The name of the column to partition.
     *
     * @param column the column name.
     */
    public void setColumn(String column) {
        this.column = column;
    }

    /**
     * The data source for connecting to the database.
     *
     * @param dataSource a {@link javax.sql.DataSource}
     */
    public void setDataSource(DataSource dataSource) {
        jdbcTemplate = new JdbcTemplate(dataSource);
    }

    /**
     * Partition a database table assuming that the data in the column specified
     * are uniformly distributed. The execution context values will have keys
     * <code>minValue</code> and <code>maxValue</code> specifying the range of
     * values to consider in each partition.
     *
     * @see org.springframework.batch.core.partition.support.Partitioner#partition(int)
     */
    public Map<String, ExecutionContext> partition(int gridSize) {
        //log.debug("Pointer");
        Long min = jdbcTemplate.queryForLong("SELECT MIN(" + column + ") from " + table +" WHERE status='LIVE'");
        Long max = jdbcTemplate.queryForLong("SELECT MAX(" + column + ") from " + table +" WHERE status='LIVE'");
        Long targetSize = (max - min) / gridSize + 1L;

        Map<String, ExecutionContext> result = new HashMap<String, ExecutionContext>();
        Long number = 0L;
        Long start = min;
        Long end = start + targetSize - 1L;

        while (start <= max) {

            ExecutionContext value = new ExecutionContext();

            if (end >= max) {
                end = max;
            }
            value.putLong("minValue", start);
            value.putLong("maxValue", end);
            result.put("partition" + number, value);
            start += targetSize;
            end += targetSize;
            number++;
        }

        return result;

    }

}
