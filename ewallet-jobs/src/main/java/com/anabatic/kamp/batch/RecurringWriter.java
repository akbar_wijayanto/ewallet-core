package com.anabatic.kamp.batch;

import java.util.List;

import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.anabatic.kamp.businesslogic.handler.api.TransactionHandler;
import com.anabatic.kamp.enumeration.ActivityEnum;
import com.anabatic.kamp.persistence.model.CustomerAccountOpening;
import com.anabatic.kamp.persistence.model.TrxRecord;
import com.anabatic.kamp.service.CustomerAccountOpeningManager;
import com.anabatic.kamp.service.JournalTransactionManager;
import com.anabatic.kamp.service.TrxRecordManager;

/**
 * 
 * @version 1.0, Sep 16, 2015 1:45:02 AM
 */
public class RecurringWriter implements ItemWriter<TrxRecord> {

	@Autowired
	private JournalTransactionManager journalTransactionManager;
	
	@Autowired
	private CustomerAccountOpeningManager customerAccountOpeningManager;
	
	@Autowired
	private TrxRecordManager trxRecordManager;
	
	@Autowired @Qualifier("transactionCalculator")
	private TransactionHandler<TrxRecord> transactionCalculator;
	
	@Override
	public void write(List<? extends TrxRecord> items) throws Exception {
		if(items.size()>0) {
			for (TrxRecord item : items) {
				CustomerAccountOpening accountFrom = customerAccountOpeningManager.getByAccountCode(item.getAccountFrom());
				CustomerAccountOpening accountTo = customerAccountOpeningManager.getByAccountCode(item.getAccountTo());
				item = transactionCalculator.process(item);
				item = trxRecordManager.save(item);
				customerAccountOpeningManager.updateBalance(accountFrom, item.getConvertedAmount(), ActivityEnum.COB_ACCOUNT_SUBSTARCT);
				customerAccountOpeningManager.updateBalance(accountTo, item.getConvertedAmount(), ActivityEnum.COB_ACCOUNT_ADD);
				journalTransactionManager.createJurnal(item.getTrxCode().getTransactionCode(), item, accountFrom, accountTo);
			}		
		}
	}
}
