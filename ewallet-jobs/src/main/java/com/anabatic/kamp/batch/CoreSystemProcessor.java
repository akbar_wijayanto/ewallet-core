package com.anabatic.kamp.batch;

import java.util.Calendar;

import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;

import com.anabatic.kamp.persistence.model.CoreCalendar;
import com.anabatic.kamp.persistence.model.CoreSystem;
import com.anabatic.kamp.persistence.util.SystemCalendarUtil;
import com.anabatic.kamp.service.CoreCalendarManager;

public class CoreSystemProcessor implements ItemProcessor<CoreSystem, CoreSystem>{

	private CoreCalendarManager coreCalendarManager;
	
	@Autowired
	public void setCoreCalendarManager(CoreCalendarManager coreCalendarManager)
	{
		this.coreCalendarManager = coreCalendarManager;
	}
	
	@Override
	public CoreSystem process(CoreSystem item) throws Exception {
		Calendar calendar = Calendar.getInstance();
        calendar.setTime(item.getTodayDate());
        CoreCalendar[] calendars = coreCalendarManager.getCurrentPrevNextYears(calendar.get(Calendar.YEAR));
		item.setPreviousDate(item.getTodayDate());
		item.setTodayDate(item.getNextDate());
		item.setNextDate(SystemCalendarUtil.getOtherWorkingDay(calendars, +1, item.getTodayDate(),item.getDefaultHoliday()));
		return item;
	}

}
