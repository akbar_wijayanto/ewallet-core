package com.anabatic.kamp.batch;


import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.anabatic.kamp.batch.dto.AccountDormantDto;


/**
 * 
 * @version 1.0, Aug 19, 2015 9:56:38 PM
 */
public class ChangeAccountStatusDormantRowMapper implements RowMapper<AccountDormantDto>{

	@Override
	public AccountDormantDto mapRow(ResultSet rs, int rowNum) throws SQLException {
		AccountDormantDto dto = new AccountDormantDto();
		dto.setId(rs.getLong("id"));
		dto.setCodeAccount(rs.getString("code_account"));
		dto.setDormantDueDate(rs.getDate("dormant_due_date"));
		return dto;
	}

}
