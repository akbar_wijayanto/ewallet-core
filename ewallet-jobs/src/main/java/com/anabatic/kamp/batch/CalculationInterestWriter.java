package com.anabatic.kamp.batch;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;

import com.anabatic.kamp.businesslogic.calculator.balance.BalanceCalculatorFactory;
import com.anabatic.kamp.businesslogic.calculator.balance.IBalance;
import com.anabatic.kamp.enumeration.BalanceTypeEnum;
import com.anabatic.kamp.enumeration.TransactionTypeEnum;
import com.anabatic.kamp.enumeration.TypeInterestEnum;
import com.anabatic.kamp.persistence.model.CoreSystem;
import com.anabatic.kamp.persistence.model.CustomerAccountDailyBalance;
import com.anabatic.kamp.persistence.model.TrxRecord;
import com.anabatic.kamp.service.CoreSystemManager;
import com.anabatic.kamp.service.CustomerAccountDailyBalanceManager;
import com.anabatic.kamp.service.CustomerAccountHistoryBalanceManager;
import com.anabatic.kamp.service.JournalTransactionManager;
import com.anabatic.kamp.service.TransactionCodeManager;
import com.anabatic.kamp.service.TrxRecordManager;
import com.anabatic.kamp.service.TrxReversalManager;

/**
 * 
 * @version 1.0, Aug 6, 2015 9:21:12 PM
 */
public class CalculationInterestWriter implements ItemWriter<List<CustomerAccountDailyBalance>> {

	@Autowired 
	private CustomerAccountDailyBalanceManager mgr;
	
	@Autowired 
	private CoreSystemManager coreSystemManager;
	
	@Autowired
	private TransactionCodeManager transactionCodeManager;
	
	@Autowired
	private JournalTransactionManager journalTransactionManager;
	
	@Autowired
	private TrxReversalManager trxReversalManager;
	
	@Autowired
	private TrxRecordManager trxRecordManager;
	
	@Autowired
	private CustomerAccountHistoryBalanceManager customerAccountHistoryBalanceManager;
	
	@Override
	public void write(List<? extends List<CustomerAccountDailyBalance>> itemss) throws Exception {
		if(itemss.size()>0) {
			for (List<CustomerAccountDailyBalance> items : itemss) {
				CoreSystem coreSystem = coreSystemManager.getCurrentSystem();
				TrxRecord record = new TrxRecord();
				record.setBookingDate(coreSystem.getTodayDate());
				record.setPostingDate(coreSystem.getPostingDate());
				record.setRate(BigDecimal.ONE);
				record.setAccountFrom(null);
				record.setDescription("INTEREST ACCRUE " + items.get(0).getCalculationType().getValue() + ", ACCOUNT : " + items.get(0).getAccount().getCodeAccount());
				record.setCoreBranch(items.get(0).getBranchName());
				record.setCoreCurrency(items.get(0).getCurrency());
				record.setPairedCurrency(items.get(0).getCurrency());
				record.setTeller("TL002");

				List<BigDecimal> amounts = new ArrayList<BigDecimal>();
				BigDecimal amount = BigDecimal.ZERO;
				for (CustomerAccountDailyBalance item : items) {
					if(item.getCalculationType() == BalanceTypeEnum.DAILY) {
						amount = amount.add(item.getPositiveInterestAccrued());
						record.setAmount(amount);
					}
					else
						amounts.add(item.getPositiveInterestAccrued());
				}
				
				if(amount.compareTo(BigDecimal.ZERO) == 0) {
					IBalance calculator = BalanceCalculatorFactory.getCalculator(items.get(0).getCalculationType());
		        	amount = calculator.calculate(amounts);
					record.setAmount(amount);
				}
				
				if(items.get(0).getInterestType().equals(TypeInterestEnum.CREDIT.getValue()))
					record.setTrxCode(transactionCodeManager.get(TransactionTypeEnum.INTEREST_ACCRUE_CREDIT.getTransactionType()));
				else
					record.setTrxCode(transactionCodeManager.get(TransactionTypeEnum.INTEREST_ACCRUE_DEBIT.getTransactionType()));

				record = trxRecordManager.save(record);
				
//				journalTransactionManager.createGlToGlJournal(record.getTrxCode().getTransactionCode(), record);
				
				Calendar calendar = Calendar.getInstance();
				calendar.setTime(coreSystem.getTodayDate());
				
				for (CustomerAccountDailyBalance item : items) {
					item.setTransactionReference(record.getTransactionReference());
					
					SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMM");
					Integer period = Integer.valueOf(dateFormat.format(coreSystem.getTodayDate()));

					Integer date = calendar.get(Calendar.DATE);
					String dd = (date<10) ? 0+String.valueOf(date) : String.valueOf(date); 
					
					customerAccountHistoryBalanceManager.add(period, dd, item.getAccount().getId(), item.getMinimumBalance(), item.getMaximumBalance(), item.getAverageBalance(), item.getCurrentBalance(), item.getPositiveInterestAccrued());
					
					calendar.add(Calendar.DATE, 1);
					
					mgr.save(item);
				}
			}
		}
	}
}
