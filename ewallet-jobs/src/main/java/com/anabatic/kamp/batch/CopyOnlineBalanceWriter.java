package com.anabatic.kamp.batch;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.batch.item.ItemWriter;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;

import com.anabatic.kamp.persistence.model.CustomerAccountOpening;

/**
 * 
 * @version 1.0, Aug 16, 2015 10:45:12 PM
 */
public class CopyOnlineBalanceWriter implements ItemWriter<CustomerAccountOpening> {

	private JdbcTemplate jdbcTemplate;

    public void setDataSource(DataSource dataSource) {
        if (jdbcTemplate == null) {
            this.jdbcTemplate = new JdbcTemplate(dataSource);
        }
    }
    
	@Override
	public void write(final List<? extends CustomerAccountOpening> items) throws Exception {
		if(items.size()>0) {

			String sql = "update customer_account_opening set working_balance=? where id=?";
			jdbcTemplate.batchUpdate(sql, new BatchPreparedStatementSetter() {
            	
                public void setValues(PreparedStatement ps, int i) throws SQLException {
                	CustomerAccountOpening account = items.get(i);
                    ps.setBigDecimal(1, account.getOnlineBalance());
                    ps.setLong(2, account.getId());
                }

                public int getBatchSize() {
                    return items.size();
                }
            });
		}
	}

}
