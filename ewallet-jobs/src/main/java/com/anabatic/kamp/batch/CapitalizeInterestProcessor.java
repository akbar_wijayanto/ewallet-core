package com.anabatic.kamp.batch;

import org.springframework.batch.item.ItemProcessor;

import com.anabatic.kamp.batch.dto.CapitalizeInterestDto;


/**
 * 
 * @version 1.0, Aug 18, 2015 4:13:27 AM
 */
public class CapitalizeInterestProcessor implements ItemProcessor<CapitalizeInterestDto, CapitalizeInterestDto> {
	
	@Override
	public CapitalizeInterestDto process(CapitalizeInterestDto item) throws Exception {
    	return item;
	}

}
