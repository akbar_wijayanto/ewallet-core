package com.anabatic.kamp.batch;

public interface BatchProcessing {

	public void run();
}
