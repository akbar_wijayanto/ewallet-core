package com.anabatic.kamp.batch;

import org.springframework.batch.item.ItemProcessor;

import com.anabatic.kamp.enumeration.BranchStateEnum;
import com.anabatic.kamp.persistence.model.CoreBranch;

public class ClosingBranchProcessing implements ItemProcessor<CoreBranch, CoreBranch>{

	@Override
	public CoreBranch process(CoreBranch item) throws Exception {
		item.setOnlineState(BranchStateEnum.OFFLINE.getStateValue());
		return item;
	}

}
