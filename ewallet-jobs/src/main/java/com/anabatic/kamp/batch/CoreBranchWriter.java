package com.anabatic.kamp.batch;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.batch.item.ItemWriter;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;

import com.anabatic.kamp.persistence.model.CoreBranch;

public class CoreBranchWriter implements ItemWriter<CoreBranch>{

	private JdbcTemplate jdbcTemplate;

    public void setDataSource(DataSource dataSource) {
        if (jdbcTemplate == null) {
            this.jdbcTemplate = new JdbcTemplate(dataSource);
        }
    }
    
	@Override
	public void write(final List<? extends CoreBranch> items) throws Exception {
		if(items.size()>0)
		{
			int[] updateCounts = null;
			String sql = "";

            sql = "update core_branch set online_state=? where id=?";
            updateCounts = jdbcTemplate.batchUpdate(sql, new BatchPreparedStatementSetter() {
            	
                public void setValues(PreparedStatement ps, int i) throws SQLException {
                    CoreBranch coreBranch = items.get(i);
                    ps.setInt(1, coreBranch.getOnlineState());
                    ps.setString(2, coreBranch.getId());
                }

                public int getBatchSize() {
                    return items.size();
                }
            });
		}
	}

}
