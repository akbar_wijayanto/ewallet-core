/**
 * 
 */
package com.ewallet.businesslogic.handler.impl;

import org.springframework.transaction.annotation.Transactional;

import com.ewallet.businesslogic.handler.api.ITransactionHandler;
import com.ewallet.exception.RetailException;
import com.ewallet.persistence.model.TrxOrder;

/**
 * @author akbar.wijayanto
 * Date Nov 2, 2015 3:05:52 PM
 */
public abstract class GenericTransactionHandler implements ITransactionHandler<TrxOrder>{

	@Override
	@Transactional(rollbackFor=Exception.class)
	public TrxOrder process(TrxOrder t) throws RetailException {
		// TODO Auto-generated method stub
		return null;
	}
	
}
