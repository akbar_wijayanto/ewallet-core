/**
 * 
 */
package com.ewallet.businesslogic.calculator.balance;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;


/**
 * 
 * @version 1.0, Aug 27, 2015 3:20:41 PM
 */
public class AverageBalanceCalculator implements IBalance {

	@Override
	public BigDecimal calculate(List<BigDecimal> balances) {

		BigDecimal total = BigDecimal.ZERO;
		for (BigDecimal balance : balances) { 
			total = total.add(balance);
		}
		
		return total.divide(new BigDecimal(balances.size()), 2, RoundingMode.HALF_UP);
	}

}
