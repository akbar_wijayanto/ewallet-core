/**
 * 
 */
package com.ewallet.enumeration;

import java.util.HashMap;
import java.util.Map;

/**
 * @author arni.sihombing
 *
 */
public enum ApprovalEnum {
	USER_APPROVAL(1,"User Approval"),
	DIRECT_TO_CUSTOMER(2,"Direct to Customer");

	private Integer id;
	private String  lable;
	
	private ApprovalEnum( Integer id , String lable){
		this.lable = lable;
		this.id = id;
	}	
	
	public Integer getId() {
		return id;
	}
	
	public String getLable() {
		return lable;
	}
	
	private static final Map<Integer, ApprovalEnum> lookup = new HashMap<Integer, ApprovalEnum>();
    static {
        for (ApprovalEnum d : ApprovalEnum.values())
            lookup.put(d.getId(), d);
    }
    
    public static ApprovalEnum get(Integer id) {
        return lookup.get(id);
    }
}
