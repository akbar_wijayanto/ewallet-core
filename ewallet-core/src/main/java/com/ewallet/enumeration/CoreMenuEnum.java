package com.ewallet.enumeration;

import java.util.HashMap;
import java.util.Map;

public enum CoreMenuEnum {
	
	CHILD("C","Child"),
	PARENT("P","Parent");
	
	private String menuValue;
	private String menuType;

	private CoreMenuEnum(String menuValue, String menuType){
		this.menuValue = menuValue;
		this.menuType = menuType;
	}	
	public String getMenuValue() {
		return menuValue;
	}
	
	public String getMenuType() {
		return menuType;
	}

	private static final Map<String, CoreMenuEnum> lookup = new HashMap<String, CoreMenuEnum>();
    static {
        for (CoreMenuEnum d : CoreMenuEnum.values())
            lookup.put(d.getMenuValue(), d);
    }
    
    public static CoreMenuEnum get(String id) {
        return lookup.get(id);
    }
}
