package com.ewallet.enumeration;

import java.util.HashMap;
import java.util.Map;

/**
 * @author akbar.wijayanto
 * Date Oct 24, 2015 12:35:32 AM
 */
public enum OrderStatusEnum {
   
	BOOKED("BOOKED"),
    PROCESS("PROCESS"),
    DONE("DONE"),
    REFUND("REFUND"),
    DELIVERED("DELIVERED");

    private String name;

    private OrderStatusEnum(String name){
      this.name=name;
    }

    public String getName() {
        return name;
    }
    
    private static final Map<String, OrderStatusEnum> lookup = new HashMap<String, OrderStatusEnum>();
    static {
        for (OrderStatusEnum d : OrderStatusEnum.values())
            lookup.put(d.getName(), d);
    }
    
    public static OrderStatusEnum get(String id) {
        return lookup.get(id);
    }
}
