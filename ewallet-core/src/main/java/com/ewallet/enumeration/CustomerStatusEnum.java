package com.ewallet.enumeration;

public enum CustomerStatusEnum {
	
	ACTIVE(0, "Active"),
	INACTIVE(1, "Inactive");
	
	private int value;
	private String name;
	
	/**
	 * @param value
	 * @param name
	 */
	private CustomerStatusEnum(int value, String name) {
		this.value = value;
		this.name = name;
	}
	
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * @return the value
	 */
	public int getValue() {
		return value;
	}

}
