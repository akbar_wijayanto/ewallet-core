/**
 * 
 */
package com.ewallet.enumeration;

import java.util.HashMap;
import java.util.Map;

/**
 * @author arni.sihombing
 *
 */
public enum AutoDebetEnum {
	AUTOMATIC(1,"Automatic"),
	MANUAL(2,"Manual");

	private Integer id;
	private String  label;
	
	private AutoDebetEnum( Integer id , String lable){
		this.label = lable;
		this.id = id;
	}	
	
	public Integer getId() {
		return id;
	}
	
	public String getLabel() {
		return label;
	}
	
	private static final Map<Integer, AutoDebetEnum> lookup = new HashMap<Integer, AutoDebetEnum>();
    static {
        for (AutoDebetEnum d : AutoDebetEnum.values())
            lookup.put(d.getId(), d);
    }
    
    public static AutoDebetEnum get(Integer id) {
        return lookup.get(id);
    }
}
