package com.ewallet.enumeration;

public enum CashNonCashTransactionEnum {

	CASH(0, "CASH"), NONCASH(1, "NONCASH");

	private int statusValue;
	private String statusType;

	private CashNonCashTransactionEnum(int statusValue, String statusType) {
		this.statusType = statusType;
		this.statusValue = statusValue;
	}

	public int getStatusValue() {
		return statusValue;
	}

	public String getStatusType() {
		return statusType;
	}

	

}
