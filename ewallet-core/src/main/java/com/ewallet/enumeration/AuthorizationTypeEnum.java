package com.ewallet.enumeration;

import java.util.HashMap;
import java.util.Map;

public enum AuthorizationTypeEnum {
	
	SAVE(0,"SAVE"), DELETE(1,"DELETE");
	
	private int id;
	private String  label;
		
	private AuthorizationTypeEnum(int id, String label) {
		this.id = id;
		this.label = label;
	}

	public int getId() {
		return id;
	}

	public String getLabel() {
		return label;
	}
	
	private static final Map<Integer, AuthorizationTypeEnum> lookup = new HashMap<Integer, AuthorizationTypeEnum>();
    static {
        for (AuthorizationTypeEnum d : AuthorizationTypeEnum.values())
            lookup.put(d.getId(), d);
    }
    
    public static AuthorizationTypeEnum get(Integer id) {
        return lookup.get(id);
    }
}
