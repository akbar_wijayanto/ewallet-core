/**
 * 
 */
package com.ewallet.util;

import java.util.ArrayList;
import java.util.List;

/**
 * @author akbar.wijayanto
 * Date Oct 28, 2015 11:13:23 PM
 */
public class NameUtil {

	public static final List<String> splitFullName(String fullName){
		int firstSpace = fullName.indexOf(" "); // detect the first space character
		
		String firstName = fullName;
		if(firstSpace > 0)
				fullName.substring(0, firstSpace);  // get everything upto the first space character
		
		String lastName = "";
		if(firstSpace > 0)
				fullName.substring(firstSpace).trim(); // get everything after the first space, trimming the spaces off
		
		List<String> values = new ArrayList<String>();
		values.add(firstName);
		values.add(lastName);
		return values;
	}
	
}
