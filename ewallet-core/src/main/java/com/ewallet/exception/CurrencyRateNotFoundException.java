package com.ewallet.exception;

public class CurrencyRateNotFoundException extends Exception {

	private static final long serialVersionUID = -7745532201295529414L;

	public CurrencyRateNotFoundException(String message) {
        super(message);
    }

    public CurrencyRateNotFoundException(String message, Throwable throwable) {
        super(message, throwable);
    }
}
