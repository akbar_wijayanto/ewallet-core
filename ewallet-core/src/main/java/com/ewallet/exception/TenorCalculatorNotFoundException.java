package com.ewallet.exception;

public class TenorCalculatorNotFoundException extends Exception {
	private static final long serialVersionUID = -1698977049084719862L;

	public TenorCalculatorNotFoundException(String message, Throwable cause) {
		super(message, cause);
	}

	public TenorCalculatorNotFoundException(String message) {
		super(message);
	}
}
