del ewallet-business-logic\.classpath
del ewallet-business-logic\.project
rmdir /S /Q ewallet-business-logic\.settings
rmdir /S /Q ewallet-business-logic\target
rmdir /S /Q ewallet-business-logic\bin

del ewallet-core\.classpath
del ewallet-core\.project
rmdir /S /Q ewallet-core\.settings
rmdir /S /Q ewallet-core\target
rmdir /S /Q ewallet-core\bin

del ewallet-persistence\.classpath
del ewallet-persistence\.project
rmdir /S /Q ewallet-persistence\.settings
rmdir /S /Q ewallet-persistence\target
rmdir /S /Q ewallet-persistence\bin

del ewallet-service\.classpath
del ewallet-service\.project
rmdir /S /Q ewallet-service\.settings
rmdir /S /Q ewallet-service\target
rmdir /S /Q ewallet-service\bin

del ewallet-web\.classpath
del ewallet-web\.project
rmdir /S /Q ewallet-web\.settings
rmdir /S /Q ewallet-web\target
rmdir /S /Q ewallet-web\bin

del ewallet-ws\.classpath
del ewallet-ws\.project
rmdir /S /Q ewallet-ws\.settings
rmdir /S /Q ewallet-ws\target
rmdir /S /Q ewallet-ws\bin

del ewallet-jobs\.classpath
del ewallet-jobs\.project
rmdir /S /Q ewallet-jobs\.settings
rmdir /S /Q ewallet-jobs\target
rmdir /S /Q ewallet-jobs\bin

del ewallet-report\.classpath
del ewallet-report\.project
rmdir /S /Q ewallet-report\.settings
rmdir /S /Q ewallet-report\target
rmdir /S /Q ewallet-report\bin

del ewallet-integration\.classpath
del ewallet-integration\.project
rmdir /S /Q ewallet-integration\.settings
rmdir /S /Q ewallet-integration\target
rmdir /S /Q ewallet-integration\bin

del anabatic-platform-ewallet\mobile-adminconsole\.classpath
del anabatic-platform-ewallet\mobile-adminconsole\.project
rmdir /S /Q anabatic-platform-ewallet\mobile-adminconsole\.settings
rmdir /S /Q anabatic-platform-ewallet\mobile-adminconsole\target
rmdir /S /Q anabatic-platform-ewallet\mobile-adminconsole\bin

del anabatic-platform-ewallet\mobile-appsmarket\.classpath
del anabatic-platform-ewallet\mobile-appsmarket\.project
rmdir /S /Q anabatic-platform-ewallet\mobile-appsmarket\.settings
rmdir /S /Q anabatic-platform-ewallet\mobile-appsmarket\target
rmdir /S /Q anabatic-platform-ewallet\mobile-appsmarket\bin

del anabatic-platform-ewallet\mobile-core\.classpath
del anabatic-platform-ewallet\mobile-core\.project
rmdir /S /Q anabatic-platform-ewallet\mobile-core\.settings
rmdir /S /Q anabatic-platform-ewallet\mobile-core\target
rmdir /S /Q anabatic-platform-ewallet\mobile-core\bin

del anabatic-platform-ewallet\mobile-persistence\.classpath
del anabatic-platform-ewallet\mobile-persistence\.project
rmdir /S /Q anabatic-platform-ewallet\mobile-persistence\.settings
rmdir /S /Q anabatic-platform-ewallet\mobile-persistence\target
rmdir /S /Q anabatic-platform-ewallet\mobile-persistence\bin

del anabatic-platform-ewallet\mobile-platform\.classpath
del anabatic-platform-ewallet\mobile-platform\.project
rmdir /S /Q anabatic-platform-ewallet\mobile-platform\.settings
rmdir /S /Q anabatic-platform-ewallet\mobile-platform\target
rmdir /S /Q anabatic-platform-ewallet\mobile-platform\bin

del anabatic-platform-ewallet\mobile-service\.classpath
del anabatic-platform-ewallet\mobile-service\.project
rmdir /S /Q anabatic-platform-ewallet\mobile-service\.settings
rmdir /S /Q anabatic-platform-ewallet\mobile-service\target
rmdir /S /Q anabatic-platform-ewallet\mobile-service\bin

del anabatic-platform-ewallet\mobile-webconsole\.classpath
del anabatic-platform-ewallet\mobile-webconsole\.project
rmdir /S /Q anabatic-platform-ewallet\mobile-webconsole\.settings
rmdir /S /Q anabatic-platform-ewallet\mobile-webconsole\target
rmdir /S /Q anabatic-platform-ewallet\mobile-webconsole\bin