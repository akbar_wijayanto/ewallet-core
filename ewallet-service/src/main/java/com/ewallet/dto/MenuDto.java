package com.ewallet.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.ewallet.persistence.model.CoreMenu;

public class MenuDto implements Serializable{

	private static final long serialVersionUID = 1L;

	private CoreMenu parentMenu;
	private List<CoreMenu> childMenus = new ArrayList<CoreMenu>();
	private List<CoreMenu> childSubMenus = new ArrayList<CoreMenu>();

	public CoreMenu getParentMenu() {
		return parentMenu;
	}

	public void setParentMenu(CoreMenu parentMenu) {
		this.parentMenu = parentMenu;
	}

	public List<CoreMenu> getChildMenus() {
		return childMenus;
	}

	public void setChildMenus(List<CoreMenu> childMenus) {
		this.childMenus = childMenus;
	}

	public List<CoreMenu> getChildSubMenus() {
		return childSubMenus;
	}

	public void setChildSubMenus(List<CoreMenu> childSubMenus) {
		this.childSubMenus = childSubMenus;
	}
	
}
