package com.ewallet.security.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.EnumSet;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.authority.mapping.GrantedAuthoritiesMapper;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.provisioning.JdbcUserDetailsManager;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ewallet.Constants;
import com.ewallet.enumeration.ActionTypeEnum;
import com.ewallet.enumeration.ApplicationTypeEnum;
import com.ewallet.enumeration.ModuleTypeEnum;
import com.ewallet.persistence.dao.CoreRoleDao;
import com.ewallet.persistence.dao.CoreUserDao;
import com.ewallet.persistence.model.CorePermission;
import com.ewallet.persistence.model.CoreRole;
import com.ewallet.persistence.model.CoreSystem;
import com.ewallet.persistence.model.CoreUser;
import com.ewallet.security.bean.UserId;
import com.ewallet.service.CoreMenuManager;
import com.ewallet.service.CoreSystemManager;
import com.ewallet.service.CoreUserManager;

@Service
@Transactional(readOnly = true)
public class UserDetailLDAPService implements UserDetailsService,
		GrantedAuthoritiesMapper {

	private CoreUser domainUser;
	private CoreUserDao coreUserDao;
	private CoreRoleDao coreRoleDao;

	@Autowired
	private CoreUserManager coreUserManager;
	@Autowired
	private CoreSystemManager coreSystemManager;
	@Autowired
	private CoreMenuManager coreMenuManager;

//	@Autowired
//	private JdbcUserDetailsManager userDetailsManager;
	
	@Autowired
	public void setCoreUserDao(CoreUserDao coreUserDao) {
		this.coreUserDao = coreUserDao;
	}

	@Autowired
	public void setCoreRoleDao(CoreRoleDao coreRoleDao) {
		this.coreRoleDao = coreRoleDao;
	}

	@Override
	public UserDetails loadUserByUsername(String username)
			throws UsernameNotFoundException {

		Set<CorePermission> permission;
		domainUser = coreUserDao.getUserSpring(username);

		if (domainUser != null) {
			permission = domainUser.getCorePermissions();
			// DEPRECATED
			// String newSalt = KeyGenerators.string().generateKey();
			// String salt = domainUser.getUsername().concat(newSalt);

			// Check active locale
			if (StringUtils.isEmpty(domainUser.getPreferredLocale())) {
				domainUser.setPreferredLocale("en_US");
			}
			// Set active branch
			// if (domainUser.getActiveBranch() == null &&
			// domainUser.getCoreBranches().size() > 0) {
			// domainUser.setActiveBranch((CoreBranch)
			// domainUser.getCoreBranches().toArray()[0]);
			// }

			// Set active role
			if (domainUser.getActiveRole() == null
					&& domainUser.getCoreRoles().size() > 0) {
				domainUser.setActiveRole((CoreRole) domainUser.getCoreRoles()
						.toArray()[0]);
			}

			boolean enabled = true;
			boolean accountNonExpired = true;
			boolean credentialsNonExpired = true;
			boolean accountNonLocked = true;
			if (!domainUser.getAccountEnabled()) {
				enabled = false;
			}
			if (domainUser.getAccountExpired()) {
				accountNonExpired = false;
			}
			if (domainUser.getCredentialsExpired()) {
				credentialsNonExpired = false;
			}
			if (domainUser.getAccountExpired()) {
				accountNonLocked = false;
			}
			return new UserId(
					domainUser.getPasswordSalt(), 
					domainUser,
					domainUser.getUsername(),
					domainUser.getPassword(),
					enabled, 
					accountNonExpired,
					credentialsNonExpired,
					accountNonLocked, 
					getAuthorities(domainUser.getActiveRole().getId(), permission));
		} else {
			throw new UsernameNotFoundException(
					"Username or password not found");
		}
	}

	public List<GrantedAuthority> getAuthorities(Integer role,
			Set<CorePermission> permissions) {
		List<GrantedAuthority> authList = getGrantedAuthorities(getRoles(role,
				permissions));
		return authList;
	}

	public static List<GrantedAuthority> getGrantedAuthorities(
			List<String> roles) {
		List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();

		for (String role : roles) {
			authorities.add(new SimpleGrantedAuthority(role));
		}
		return authorities;
	}

	public List<String> getRoles(Integer role, Set<CorePermission> permissions) {

		List<String> roles = new ArrayList<String>();
		Integer idActiveRole = role.intValue();
		CoreRole coreRole = coreRoleDao.getById(idActiveRole);

		Set<String> moduleType = setModuleType();
		Set<String> applicationType = setApplicationType();
		Set<String> actionType = setActionType();

		if (role.intValue() != 0) {
			roles.add(coreRole.getName());
		}
		if (permissions != null) {
			for (CorePermission corePermission : permissions) {
				String module = corePermission.getModuleName();
				String application = corePermission.getApplicationName();
				String action = corePermission.getActionName();
				boolean trueModel = module.equals("*");
				boolean trueApplication = application.equals("*");
				boolean trueAction = action.equals("*");
				for (String moduleTypes : moduleType) {
					if (trueModel) {
						module = moduleTypes;
					}
					for (String applicationTypes : applicationType) {
						if (trueApplication) {
							application = applicationTypes;
						}
						for (String actionTypes : actionType) {
							if (trueAction) {
								action = actionTypes;
							}
							roles.add(module + ":" + application + ":" + action
									+ ":" + corePermission.getRecordId());
						}
					}
				}
			}
		}
		return roles;
	}

	private Set<String> setApplicationType() {
		Set<String> applicationType = new HashSet<String>();

		applicationType.add(ApplicationTypeEnum.ALL.getApplicationValue());
		applicationType.add(ApplicationTypeEnum.SYSTEM.getApplicationValue());
		applicationType.add(ApplicationTypeEnum.BRANCH.getApplicationValue());
		applicationType.add(ApplicationTypeEnum.CALENDAR.getApplicationValue());
		applicationType.add(ApplicationTypeEnum.CURRENCY.getApplicationValue());
		applicationType.add(ApplicationTypeEnum.CURRENCYRATE
				.getApplicationValue());
		applicationType.add(ApplicationTypeEnum.BATCH.getApplicationValue());
		applicationType.add(ApplicationTypeEnum.CLOSING.getApplicationValue());
		applicationType.add(ApplicationTypeEnum.INTERESTBASIS
				.getApplicationValue());
		applicationType.add(ApplicationTypeEnum.INTERESTIBT
				.getApplicationValue());
		applicationType.add(ApplicationTypeEnum.MENU.getApplicationValue());
		applicationType.add(ApplicationTypeEnum.SECURITY.getApplicationValue());
		applicationType.add(ApplicationTypeEnum.SECURITYPOLICY
				.getApplicationValue());
		/* TODO: Add by Nana */
		applicationType.add(ApplicationTypeEnum.ACCOUNTOFFICER
				.getApplicationValue());
		applicationType.add(ApplicationTypeEnum.DEPARTMENT
				.getApplicationValue());
		applicationType.add(ApplicationTypeEnum.TRANSACTIONCODE
				.getApplicationValue());
		applicationType.add(ApplicationTypeEnum.DENOMINATION
				.getApplicationValue());
		applicationType.add(ApplicationTypeEnum.VAULT.getApplicationValue());
		applicationType.add(ApplicationTypeEnum.TELLERACCOUNT
				.getApplicationValue());
		applicationType.add(ApplicationTypeEnum.APPLICATION
				.getApplicationValue());
		applicationType.add(ApplicationTypeEnum.LEVEL.getApplicationValue());
		applicationType.add(ApplicationTypeEnum.INTERESTBASE
				.getApplicationValue());
		applicationType.add(ApplicationTypeEnum.INTERESTPARAMETER
				.getApplicationValue());
		applicationType.add(ApplicationTypeEnum.PRODUCT.getApplicationValue());
		applicationType.add(ApplicationTypeEnum.TAX.getApplicationValue());
		applicationType.add(ApplicationTypeEnum.COA.getApplicationValue());
		applicationType.add(ApplicationTypeEnum.ACCOUNTING
				.getApplicationValue());
		// applicationType.add(ApplicationTypeEnum.TRANSACTION.getApplicationValue());
		// applicationType.add(ApplicationTypeEnum.TRANSACTIONCODE.getApplicationValue());
		// applicationType.add(ApplicationTypeEnum.VIRTUALMAP.getApplicationValue());
		// applicationType.add(ApplicationTypeEnum.AMORTIZATION.getApplicationValue());
		// applicationType.add(ApplicationTypeEnum.SUMMARYAMORTIZATION.getApplicationValue());
		// applicationType.add(ApplicationTypeEnum.COLLECTIVE.getApplicationValue());
		// applicationType.add(ApplicationTypeEnum.SUMMARYCOLLECTIVE.getApplicationValue());
		// applicationType.add(ApplicationTypeEnum.INDIVIDUAL.getApplicationValue());
		// applicationType.add(ApplicationTypeEnum.SUMMARYINDIVIDUAL.getApplicationValue());
		// applicationType.add(ApplicationTypeEnum.ACCOUNT.getApplicationValue());
		// applicationType.add(ApplicationTypeEnum.ACCOUNTTEMPLATE.getApplicationValue());
		// applicationType.add(ApplicationTypeEnum.RECURRING.getApplicationValue());
		// applicationType.add(ApplicationTypeEnum.REPORT.getApplicationValue());
		// applicationType.add(ApplicationTypeEnum.SOURCE.getApplicationValue());
		// applicationType.add(ApplicationTypeEnum.MAPPING.getApplicationValue());
		// applicationType.add(ApplicationTypeEnum.DEFINITION.getApplicationValue());
		// applicationType.add(ApplicationTypeEnum.DIMENSION.getApplicationValue());
		// applicationType.add(ApplicationTypeEnum.DOUBLEENTRY.getApplicationValue());
		// applicationType.add(ApplicationTypeEnum.HIERARCHY.getApplicationValue());
		// applicationType.add(ApplicationTypeEnum.BATCHJOURNAL.getApplicationValue());
		// applicationType.add(ApplicationTypeEnum.COSTCENTER.getApplicationValue());
		// applicationType.add(ApplicationTypeEnum.BUDGET.getApplicationValue());
		return applicationType;
	}

	private Set<String> setModuleType() {
		Set<String> moduleType = new HashSet<String>();
		moduleType.add(ModuleTypeEnum.ALL.getModuleValue());
		moduleType.add(ModuleTypeEnum.CORE.getModuleValue());
		moduleType.add(ModuleTypeEnum.TRX.getModuleValue());
		// moduleType.add(ModuleTypeEnum.LEDGER.getModuleValue());
		return moduleType;
	}

	private Set<String> setActionType() {
		Set<String> actionType = new HashSet<String>();
		actionType.add(ActionTypeEnum.ALL.getId());
		actionType.add(ActionTypeEnum.AUTHORIZE.getId());
		actionType.add(ActionTypeEnum.DELETE.getId());
		actionType.add(ActionTypeEnum.INPUT.getId());
		actionType.add(ActionTypeEnum.EDIT.getId());
		actionType.add(ActionTypeEnum.READ.getId());
		return actionType;
	}

	@Override
	public Collection<? extends GrantedAuthority> mapAuthorities(
			Collection<? extends GrantedAuthority> authorities) {
		Set<RoleEnum> roles = EnumSet.noneOf(RoleEnum.class);
		for (GrantedAuthority a : authorities) {
			if ("ADMIN".equals(a.getAuthority())) {
				roles.add(RoleEnum.ROLE_ADMIN);
			} else if ("USER".equals(a.getAuthority())) {
				roles.add(RoleEnum.ROLE_USER);
			}
		}
		return roles;
	}
	
	enum RoleEnum implements GrantedAuthority {
	    ROLE_ADMIN,
	    ROLE_USER;

	    public String getAuthority() {
	        return name();
	    }
	}

//	@Override
//	public void createUser(UserDetails user) {
//		Set<CorePermission> permission;
//		domainUser = coreUserDao.getUserSpring(user.getUsername());
//
//		if (domainUser != null) {
//			permission = domainUser.getCorePermissions();
//			// DEPRECATED
//			// String newSalt = KeyGenerators.string().generateKey();
//			// String salt = domainUser.getUsername().concat(newSalt);
//
//			// Check active locale
//			if (StringUtils.isEmpty(domainUser.getPreferredLocale())) {
//				domainUser.setPreferredLocale("en_US");
//			}
//			// Set active branch
//			// if (domainUser.getActiveBranch() == null &&
//			// domainUser.getCoreBranches().size() > 0) {
//			// domainUser.setActiveBranch((CoreBranch)
//			// domainUser.getCoreBranches().toArray()[0]);
//			// }
//
//			// Set active role
//			if (domainUser.getActiveRole() == null
//					&& domainUser.getCoreRoles().size() > 0) {
//				domainUser.setActiveRole((CoreRole) domainUser.getCoreRoles()
//						.toArray()[0]);
//			}
//
//			boolean enabled = true;
//			boolean accountNonExpired = true;
//			boolean credentialsNonExpired = true;
//			boolean accountNonLocked = true;
//			if (!domainUser.getAccountEnabled()) {
//				enabled = false;
//			}
//			if (domainUser.getAccountExpired()) {
//				accountNonExpired = false;
//			}
//			if (domainUser.getCredentialsExpired()) {
//				credentialsNonExpired = false;
//			}
//			if (domainUser.getAccountExpired()) {
//				accountNonLocked = false;
//			}
//		
//		UserDetails userDetails = new UserId(
//				domainUser.getPasswordSalt(), 
//				domainUser,
//				domainUser.getUsername(),
//				domainUser.getPassword(),
//				enabled, 
//				accountNonExpired,
//				credentialsNonExpired,
//				accountNonLocked, 
//				getAuthorities(domainUser.getActiveRole().getId(), permission));
//		
//		userDetailsManager.createUser(userDetails);
//		
//		}
//	}
//
//	@Override
//	public void updateUser(UserDetails user) {
//		// TODO Auto-generated method stub
//		
//	}
//
//	@Override
//	public void deleteUser(String username) {
//		// TODO Auto-generated method stub
//		
//	}
//
//	@Override
//	public void changePassword(String oldPassword, String newPassword) {
//		// TODO Auto-generated method stub
//		
//	}
//
//	@Override
//	public boolean userExists(String username) {
//		// TODO Auto-generated method stub
//		return false;
//	}
	
}
