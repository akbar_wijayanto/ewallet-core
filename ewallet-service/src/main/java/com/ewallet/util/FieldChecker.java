package com.ewallet.util;

import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.EmbeddedId;
import javax.persistence.Id;

import org.springframework.stereotype.Component;

import com.ewallet.Constants;
import com.ewallet.persistence.model.BaseGenericObject;


/**
 * 
 * Parse the value to the class that got from parameter
 * 
 * @author Harits Fahreza Christyonotoputra
 *
 */
@Component
public class FieldChecker {

	public static Object getFieldValue(String fieldValue, Class<?> fieldType, boolean isTrim) throws Exception{
		Object object = new Object();
		if(fieldValue == null){
			return null;
		}
		if(isTrim){
			fieldValue = fieldValue.trim();
		}
		if(fieldType.equals(String.class)){
			object = fieldValue;
		} else if(fieldType.equals(Integer.class)){
				object = Integer.parseInt(fieldValue.trim());
		} else if(fieldType.equals(Long.class)){
			object = Long.parseLong(fieldValue.trim());
		} else if(fieldType.equals(Double.class)){
			object = Double.parseDouble(fieldValue.trim());
		} else if(fieldType.equals(Boolean.class) || fieldType.equals(boolean.class)){
			object = Boolean.valueOf(fieldValue.trim());
		} else if(fieldType.equals(BigDecimal.class)){
			if(fieldValue.contains(",")){
				fieldValue = fieldValue.replace(",", "");
			}
			object = new BigDecimal(fieldValue.trim());
		} else if(fieldType.equals(Date.class)){
			//SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat(Constants.DATE_TIME);
			object = simpleDateFormat.parse(fieldValue);
		} else if(fieldType.newInstance() instanceof BaseGenericObject){
			object = fieldType.newInstance();
			Method[] methods = fieldType.getMethods();
			for (Method method : methods) {
				Method setMethod = null;
				if(method.isAnnotationPresent(Id.class)){
					setMethod = fieldType.getMethod("set"+method.getName().substring(3), method.getReturnType());
					setMethod.invoke(object, FieldChecker.getFieldValue(fieldValue, setMethod.getParameterTypes()[0], true));
					break;
				}else if(method.isAnnotationPresent(EmbeddedId.class)){
					//TODO make for embeddedId Dr32n
				}
			}
		} else{
			object = null;
		}
		return object;
	}
	
}
