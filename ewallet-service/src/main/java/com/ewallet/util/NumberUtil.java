package com.ewallet.util;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;

public class NumberUtil {


	private static DecimalFormat decimalFormat = new DecimalFormat("###,##0.00");
	private static NumberFormat numberFormat = NumberFormat.getPercentInstance();
	
	/**
	 * 
	 * @param BigDecimal
	 * @return ###,##0.00 EQ 1,000,000.00
	 */
	public static final String toDecimalFormat(BigDecimal decimal){
		if(decimal == null) decimal = new BigDecimal(0);
		return decimalFormat.format(decimal);
	}
	
	/**
	 * 
	 * @param BigDecimal
	 * @return String (decimal*100)% 
	 */
	public static final String toPercentageFormat(BigDecimal decimal){
		return numberFormat.format(decimal);
	}
	

	/**
	 * 
	 * @param String
	 * @return BigDecimal
	 */
	public static final BigDecimal removeDecimalFormat(String formatted){
//		String unformatted = formatted.replaceAll("(?<=^\\d+)\\.0*$", ""); 
		String unformatted = formatted.replace(",", ""); 
		
		return new BigDecimal(unformatted);
	}
}
