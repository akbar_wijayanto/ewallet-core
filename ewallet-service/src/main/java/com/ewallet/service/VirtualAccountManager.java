package com.ewallet.service;

import com.ewallet.persistence.model.VirtualAccount;

/**
 * @author akbar.wijayanto
 * Date Oct 23, 2015 11:24:46 PM
 */
public interface VirtualAccountManager extends GenericTransactionManager<VirtualAccount, String> {
}
