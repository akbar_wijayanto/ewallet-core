package com.ewallet.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ewallet.exception.RetailException;
import com.ewallet.persistence.dao.TrxSellerPaymentDao;
import com.ewallet.persistence.model.TrxSellerPayment;
import com.ewallet.service.TrxSellerPaymentManager;

/**
 * @author akbar.wijayanto
 * Date Oct 23, 2015 11:25:35 PM
 */
@Service("trxSellerPaymentManager")
public class TrxSellerPaymentManagerImpl extends GenericTransactionManagerImpl<TrxSellerPayment, String>
		implements TrxSellerPaymentManager {
	
	private TrxSellerPaymentDao trxSellerPaymentDao;
	
	@Autowired
	public void setTrxSellerPaymentDao(TrxSellerPaymentDao trxSellerPaymentDao) {
		this.trxSellerPaymentDao = trxSellerPaymentDao;
		this.dao = trxSellerPaymentDao;
		this.transactionDao = trxSellerPaymentDao;
	}

	@Override
	public List<TrxSellerPayment> getAllSellerPayment() {
		return trxSellerPaymentDao.getAllSellerPayment();
	}

	@Override
	public TrxSellerPayment getTrxSellerPaymentByPaymentId(String paymentId) {
		return trxSellerPaymentDao.getTrxSellerPaymentByPaymentId(paymentId);
	}

	@Override
	public List<TrxSellerPayment> getSettlementBySellerId(String sellerid) {
		return this.trxSellerPaymentDao.getSettlementBySellerId(sellerid);
	}

	@Override
	public TrxSellerPayment getBySellerIdAndPaidDate(String sellerId,
			Date paidDate) {
		return trxSellerPaymentDao.getBySellerIdAndPaidDate(sellerId, paidDate);
	}

	@Override
	public List<TrxSellerPayment> getBySellerIdAndDate(String sellerId,
			Date startDate, Date endDate) {
		return trxSellerPaymentDao.getBySellerIdAndDate(sellerId, startDate, endDate);
	}

	@Override
	public List<TrxSellerPayment> getAllSellerPaymentPaid() {
		return trxSellerPaymentDao.getAllSellerPaymentPaid();
	}

	@Override
	public List<TrxSellerPayment> getAllSellerPaymentReport(String startDate,
			String endDate) {
		// TODO Auto-generated method stub
		return trxSellerPaymentDao.getAllSellerPaymentReport(startDate, endDate);
	}

	@Override
	@Transactional
	public void saveAll(List<TrxSellerPayment> trxSellerPayments)
			throws RetailException {
		for (TrxSellerPayment trxSellerPayment : trxSellerPayments) {
			trxSellerPaymentDao.save(trxSellerPayment);
		}
	}
	
}