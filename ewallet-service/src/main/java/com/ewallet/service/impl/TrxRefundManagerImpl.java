package com.ewallet.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ewallet.persistence.dao.TrxRefundDao;
import com.ewallet.persistence.model.TrxRefund;
import com.ewallet.service.TrxRefundManager;

/**
 * @author akbar.wijayanto
 * Date Oct 16, 2015 8:04:57 PM
 */
@Service("trxRefundManager")
public class TrxRefundManagerImpl extends GenericTransactionManagerImpl<TrxRefund, Long>
		implements TrxRefundManager {

	private TrxRefundDao trxRefundDao;
	
	@Autowired
	public void setTrxRefundDao(TrxRefundDao trxRefundDao) {
		this.trxRefundDao = trxRefundDao;
		this.dao = trxRefundDao;
		this.transactionDao = trxRefundDao;
	}
	
}