/**
 * 
 */
package com.ewallet.service.impl;

import java.io.Serializable;
import java.util.UUID;

import org.springframework.transaction.annotation.Transactional;

import com.ewallet.persistence.dao.GenericTransactionDao;
import com.ewallet.persistence.model.BaseTransaction;
import com.ewallet.service.GenericTransactionManager;

public class GenericTransactionManagerImpl<T extends BaseTransaction, PK extends Serializable> extends
		GenericManagerImpl<T, PK> implements GenericTransactionManager<T, PK> {

	protected GenericTransactionDao<T, PK> transactionDao;
	
	@Override
	@Transactional
	public T save(T object) {
		if (object.getReference()==null) {
			object.setReference(UUID.randomUUID().toString());
		}
		return transactionDao.save(object);
	}

	@Override
	public T getByTransactionReference(String transactionReference) {
		return transactionDao.getByTransactionReference(transactionReference);
	}

}
