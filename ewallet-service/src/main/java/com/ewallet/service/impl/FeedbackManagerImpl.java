package com.ewallet.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ewallet.persistence.dao.FeedbackDao;
import com.ewallet.persistence.model.Feedback;
import com.ewallet.service.FeedbackManager;

/**
 * @author akbar.wijayanto
 * Date Oct 16, 2015 8:04:57 PM
 */
@Service("feedbackManager")
public class FeedbackManagerImpl extends GenericManagerImpl<Feedback, Long>
		implements FeedbackManager {
	
	private FeedbackDao feedbackDao;
	
	@Autowired
	public void setFeedbackDao(FeedbackDao feedbackDao) {
		this.feedbackDao = feedbackDao;
		this.dao = feedbackDao;
	}

	@Override
	public List<Feedback> getAllSellerFeedbacks() {
		return feedbackDao.getAllSellerFeedbacks();
	}

	@Override
	public List<Feedback> getAllManagementFeedbacks() {
		return feedbackDao.getAllManagementFeedbacks();
	}

	@Override
	public List<Feedback> getSellerFeedbacks(String sellerId) {
		return feedbackDao.getSellerFeedbacks(sellerId);
	}
	
}