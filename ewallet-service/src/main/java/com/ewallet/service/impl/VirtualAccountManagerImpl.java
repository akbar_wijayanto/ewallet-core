package com.ewallet.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ewallet.persistence.dao.VirtualAccountDao;
import com.ewallet.persistence.model.VirtualAccount;
import com.ewallet.service.VirtualAccountManager;

/**
 * @author akbar.wijayanto
 * Date Oct 23, 2015 11:25:35 PM
 */
@Service("virtualAccountManager")
public class VirtualAccountManagerImpl extends GenericTransactionManagerImpl<VirtualAccount, String>
		implements VirtualAccountManager {
	
	private VirtualAccountDao virtualAccountDao;
	
	@Autowired
	public void setVirtualAccountDao(VirtualAccountDao virtualAccountDao) {
		this.virtualAccountDao = virtualAccountDao;
		this.dao = virtualAccountDao;
		this.transactionDao = virtualAccountDao;
	}
	
}