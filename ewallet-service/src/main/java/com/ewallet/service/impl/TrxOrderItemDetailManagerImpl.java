package com.ewallet.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ewallet.persistence.dao.TrxOrderItemDetailDao;
import com.ewallet.persistence.dao.TrxRecordDao;
import com.ewallet.persistence.model.TrxOrderItemDetail;
import com.ewallet.persistence.model.TrxOrderItemId;
import com.ewallet.persistence.model.TrxRecord;
import com.ewallet.service.TrxOrderItemDetailManager;
import com.ewallet.service.TrxRecordManager;

/**
 * @author akbar.wijayanto
 * Date Nov 25, 2015 5:28:37 PM
 */
@Service("trxOrderItemDetailManager")
public class TrxOrderItemDetailManagerImpl extends GenericTransactionManagerImpl<TrxOrderItemDetail, TrxOrderItemId>
		implements TrxOrderItemDetailManager {
	
	private TrxOrderItemDetailDao trxOrderItemDetailDao;
	
	@Autowired
	public void setTrxOrderItemDetailDao(
			TrxOrderItemDetailDao trxOrderItemDetailDao) {
		this.trxOrderItemDetailDao = trxOrderItemDetailDao;
		this.dao = trxOrderItemDetailDao;
		this.transactionDao = trxOrderItemDetailDao;
	}

	@Override
	@Transactional
	public void removeByOrderId(String orderId) {
			
	}
	
}