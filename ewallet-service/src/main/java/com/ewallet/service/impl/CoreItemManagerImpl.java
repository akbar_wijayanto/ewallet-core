package com.ewallet.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ewallet.persistence.dao.CoreItemDao;
import com.ewallet.persistence.model.CoreItem;
import com.ewallet.service.CoreItemManager;


/**
 * @author akbar.wijayanto
 * Date Oct 16, 2015 7:51:48 PM
 */
@Service("coreItemManager")
public class CoreItemManagerImpl extends GenericManagerImpl<CoreItem, Long> implements CoreItemManager {

	private CoreItemDao coreItemDao;

	@Autowired
	public void setCoreItemDao(CoreItemDao coreItemDao) {
		this.coreItemDao = coreItemDao;
		this.dao = coreItemDao;
	}

	@Override
	public CoreItem getByItemId(String itemId) {
		return coreItemDao.getByItemId(itemId);
	}

	@Override
	public List<CoreItem> getBySellerId(String sellerId) {
		return coreItemDao.getBySellerId(sellerId);
	}

	@Override
	public List<CoreItem> getAllAuthItem() {
		// TODO Auto-generated method stub
		return coreItemDao.getAllAuthItem();
	}

}
