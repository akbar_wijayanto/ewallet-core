package com.ewallet.service.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.orm.jpa.JpaSystemException;
import org.springframework.security.authentication.encoding.ShaPasswordEncoder;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.keygen.KeyGenerators;
import org.springframework.stereotype.Service;

import com.ewallet.enumeration.ActionTypeEnum;
import com.ewallet.enumeration.ApplicationTypeEnum;
import com.ewallet.enumeration.ModuleTypeEnum;
import com.ewallet.exception.UserExistsException;
import com.ewallet.persistence.dao.CoreRoleDao;
import com.ewallet.persistence.dao.CoreUserDao;
import com.ewallet.persistence.model.CorePermission;
import com.ewallet.persistence.model.CoreRole;
import com.ewallet.persistence.model.CoreUser;
import com.ewallet.security.bean.UserId;
import com.ewallet.service.CoreUserManager;


/**
 * Implementation of UserManager interface.
 *
 * @author <a href="mailto:ab.annas@gmail.com">Andi Baso Annas</a>
 */

/**
 * Implementing of UserDetailService
 * 
 * @author muchamad.girinata
 *
 */
@Service("coreUserManager")
public class CoreUserManagerImpl extends GenericManagerImpl<CoreUser, Long> implements CoreUserManager, UserDetailsService {
	private CoreUserDao coreUserDao;
	@Autowired
	private CoreRoleDao coreRoleDao;

	@Autowired
	public void setCoreUserDao(@Qualifier("coreUserDao") CoreUserDao coreUserDao) {
		this.dao = coreUserDao;
		this.coreUserDao = coreUserDao;
	}


	/**
	 * {@inheritDoc}
	 */
	public CoreUser getUser(String userId) {
		return coreUserDao.get(new Long(userId));
	}

	/**
	 * {@inheritDoc}
	 */
	public List<CoreUser> getUsers() {
		return coreUserDao.getAllDistinct();
	}

	/**
	 * {@inheritDoc}
	 */
	public CoreUser saveUser(CoreUser coreUser) throws UserExistsException {
		log.debug("CoreUser saveUser........");
		if (coreUser.getVersion() == null) {
			// if new coreUser, lowercase userId
			coreUser.setUsername(coreUser.getUsername().toLowerCase());
		}

		//Unmark to activate
		//Get and prepare password management-related artifacts
		//boolean passwordChanged = false;

		try {
			return coreUserDao.saveUser(coreUser);
		} catch (DataIntegrityViolationException e) {
			//e.printStackTrace();
			log.warn(e.getMessage());
			throw new UserExistsException("User '" + coreUser.getUsername() + "' already exists!");
		} catch (JpaSystemException e) { // needed for JPA
			//e.printStackTrace();
			log.warn(e.getMessage());
			throw new UserExistsException("User '" + coreUser.getUsername() + "' already exists!");
		}
	}


	@Override
	public CoreUser save(CoreUser object) {

		//password encoder
		ShaPasswordEncoder encoder = new ShaPasswordEncoder(512);
		encoder.setEncodeHashAsBase64(true);
		encoder.setIterations(1024);
		String newSalt = KeyGenerators.string().generateKey();
		String salt = object.getUsername().concat(newSalt);
		String hash = encoder.encodePassword(object.getPassword(), salt);
		//password encoder

		if (object.getId() != null) {
			CoreUser oldUser = coreUserDao.get(object.getId());

			if(!StringUtils.equals(oldUser.getPassword(), object.getPassword())){
				object.setPasswordSalt(salt);
				object.setPassword(hash);
			}
		} else {
			object.setPasswordSalt(salt);
			object.setPassword(hash);
		}

		//Integrated with Activiti table
//		Set<CoreRole> coreRoles = object.getCoreRoles();
		
		return coreUserDao.save(object);
	}

	/**
	 * {@inheritDoc}
	 */
	public void removeUser(String userId) throws ConstraintViolationException {
		log.debug("removing coreUser: " + userId);
		coreUserDao.remove(new Long(userId));
	}

	/**
	 * {@inheritDoc}
	 *
	 * @param username the login name of the human
	 * @return CoreUser the populated coreUser object
	 * @throws org.springframework.security.core.userdetails.UsernameNotFoundException
	 *          thrown when username not found
	 */
	public CoreUser getUserByUsername(String username) throws UsernameNotFoundException {
		return (CoreUser) coreUserDao.loadUserByUsername(username);
	}

	@Override
	public String getUserBranch(String username) {
		CoreUser user= (CoreUser) coreUserDao.loadUserByUsername(username);
		if(user!= null){
//			return user.getActiveBranch().getId();
		}
		return null;
	}

	@Override
	public CoreUser getWithListAll(Long id) {
		return coreUserDao.getWithListAll(id);
	}

	@Override
	public List<CoreUser> getBranch(Long id) {
		return coreUserDao.getBranchById(id);
	}

	@Override
	public List<CoreUser> getCheckBranchOnUser(String id) {
		return coreUserDao.getCheckBranchOnUser(id);
	}

	@Override
	public List<CoreUser> getCheckRoleOnUser(Integer id) {
		return coreUserDao.getCheckRoleUsedByUser(id);
	}

	@Override
	public List<CoreUser> getCheckPermissionOnUser(Integer id) {
		return coreUserDao.getCheckPermissionUsedByUser(id);
	}

	@Override
	public List<CoreUser> getUserRoleList(Integer id) {
		return coreUserDao.getUserRoleList(id);
	}

	@Override
	public Long getRecordCount(Integer id) {
		return coreUserDao.getRecordCount(id);
	}

	@Override
	public List<CoreUser> getSearchResults(Integer id, Long iDisplayStart,Long iDisplayLength) {
		return coreUserDao.getSearchResult(id, iDisplayStart,iDisplayLength);
	}

	@Override
	public CoreUser updateLoginAttempt(CoreUser coreUser) {
		return coreUserDao.updateLoginAttempt(coreUser);
	}

	@Override
	public List<CoreUser> getAllOrderByUsername() {
		return coreUserDao.getAllOrderByUsername();
	}

	@Override
	public List<CoreUser> getPagingResults(Long firstResult, Long maxResults) {
		return coreUserDao.getPagingResults(firstResult, maxResults);
	}

	@Override
	public List<CoreUser> getAllWithListAll() {
		return coreUserDao.getAllWithListAll();
	}

	@Override
	public CoreUser getUserSpring(String username) {
		return coreUserDao.getUserSpring(username);
	}


	@Override
	public UserDetails loadUserByUsername(String login)            throws UsernameNotFoundException {
		CoreUser domainUser ;
		Set<CorePermission> permission;
		domainUser= coreUserDao.getUserSpring(login);

		if(domainUser != null){
			permission=domainUser.getCorePermissions();
			//DEPRECATED
			//        String newSalt = KeyGenerators.string().generateKey();
			//        String salt = domainUser.getUsername().concat(newSalt);

			//Check active locale
			if (StringUtils.isEmpty(domainUser.getPreferredLocale())) {
				domainUser.setPreferredLocale("en_US");
			}
			//Set active branch
//			if (domainUser.getActiveBranch() == null && domainUser.getCoreBranches().size() > 0) {
//				domainUser.setActiveBranch((CoreBranch) domainUser.getCoreBranches().toArray()[0]);
//			}

			//Set active role
			if (domainUser.getActiveRole() == null && domainUser.getCoreRoles().size() > 0) {
				domainUser.setActiveRole((CoreRole) domainUser.getCoreRoles().toArray()[0]);
			}

			boolean enabled = true;
			boolean accountNonExpired = true;
			boolean credentialsNonExpired = true;
			boolean accountNonLocked = true;
			if(!domainUser.getAccountEnabled()){
				enabled = false;
			}
			if(domainUser.getAccountExpired()){
				accountNonExpired = false; 
			}
			if(domainUser.getCredentialsExpired()){
				credentialsNonExpired = false; 
			}
			if(domainUser.getAccountExpired()){
				accountNonLocked = false; 
			}
			return new UserId(
					domainUser.getPasswordSalt(),
					domainUser,
					domainUser.getUsername(),
					domainUser.getPassword(),
					enabled,
					accountNonExpired,
					credentialsNonExpired,
					accountNonLocked,
					getAuthorities(domainUser.getActiveRole().getId(),permission));
		} else {
			throw new UsernameNotFoundException("Username or password not found");
		}

	}

	public List<GrantedAuthority> getAuthorities(Integer role, Set<CorePermission> permissions) {
		List<GrantedAuthority> authList = getGrantedAuthorities(getRoles(role, permissions));
		return authList;
	}

	public List<String> getRoles(Integer role, Set<CorePermission> permissions) {

		List<String> roles = new ArrayList<String>();
		Integer idActiveRole = role.intValue();
		CoreRole coreRole = coreRoleDao.getById(idActiveRole);

		Set<String> moduleType = setModuleType();
		Set<String> applicationType = setApplicationType();
		Set<String> actionType = setActionType();


		if(role.intValue() != 0){
			roles.add(coreRole.getName());
		}
		if(permissions != null){
			for(CorePermission corePermission : permissions){
				String module = corePermission.getModuleName();
				String application = corePermission.getApplicationName();
				String action = corePermission.getActionName();
				boolean trueModel = module.equals("*");
				boolean trueApplication = application.equals("*");
				boolean trueAction = action.equals("*");
				for(String moduleTypes : moduleType ){
					if(trueModel){
						module = moduleTypes;
					}
					for(String applicationTypes : applicationType){
						if(trueApplication){
							application = applicationTypes;
						}
						for(String actionTypes : actionType){
							if(trueAction){
								action = actionTypes;
							}
							roles.add(module+":"+application+":"+action+":"+corePermission.getRecordId());
						}
					}
				}	
			}
		}
		return roles;
	}

	private Set<String> setApplicationType() {
		Set<String> applicationType = new HashSet<String>();

		applicationType.add(ApplicationTypeEnum.ALL.getApplicationValue());
		applicationType.add(ApplicationTypeEnum.SYSTEM.getApplicationValue());
		applicationType.add(ApplicationTypeEnum.BRANCH.getApplicationValue());
		applicationType.add(ApplicationTypeEnum.CALENDAR.getApplicationValue());
		applicationType.add(ApplicationTypeEnum.CURRENCY.getApplicationValue());
		applicationType.add(ApplicationTypeEnum.CURRENCYRATE.getApplicationValue());
		applicationType.add(ApplicationTypeEnum.BATCH.getApplicationValue());
		applicationType.add(ApplicationTypeEnum.CLOSING.getApplicationValue());
		applicationType.add(ApplicationTypeEnum.INTERESTBASIS.getApplicationValue());
		applicationType.add(ApplicationTypeEnum.INTERESTIBT.getApplicationValue());
		applicationType.add(ApplicationTypeEnum.MENU.getApplicationValue());
		applicationType.add(ApplicationTypeEnum.SECURITY.getApplicationValue());
		applicationType.add(ApplicationTypeEnum.SECURITYPOLICY.getApplicationValue());
		/*TODO: Add by Nana*/
		applicationType.add(ApplicationTypeEnum.ACCOUNTOFFICER.getApplicationValue());
		applicationType.add(ApplicationTypeEnum.DEPARTMENT.getApplicationValue());
		applicationType.add(ApplicationTypeEnum.TRANSACTIONCODE.getApplicationValue());
		applicationType.add(ApplicationTypeEnum.DENOMINATION.getApplicationValue());
		applicationType.add(ApplicationTypeEnum.VAULT.getApplicationValue());
		applicationType.add(ApplicationTypeEnum.TELLERACCOUNT.getApplicationValue());
		applicationType.add(ApplicationTypeEnum.APPLICATION.getApplicationValue());
		applicationType.add(ApplicationTypeEnum.LEVEL.getApplicationValue());
		applicationType.add(ApplicationTypeEnum.INTERESTBASE.getApplicationValue());
		applicationType.add(ApplicationTypeEnum.INTERESTPARAMETER.getApplicationValue());
		applicationType.add(ApplicationTypeEnum.PRODUCT.getApplicationValue());
		applicationType.add(ApplicationTypeEnum.TAX.getApplicationValue());
		applicationType.add(ApplicationTypeEnum.COA.getApplicationValue());
		applicationType.add(ApplicationTypeEnum.ACCOUNTING.getApplicationValue());
//		applicationType.add(ApplicationTypeEnum.TRANSACTION.getApplicationValue());
//		applicationType.add(ApplicationTypeEnum.TRANSACTIONCODE.getApplicationValue());
//		applicationType.add(ApplicationTypeEnum.VIRTUALMAP.getApplicationValue());
//		applicationType.add(ApplicationTypeEnum.AMORTIZATION.getApplicationValue());
//		applicationType.add(ApplicationTypeEnum.SUMMARYAMORTIZATION.getApplicationValue());
//		applicationType.add(ApplicationTypeEnum.COLLECTIVE.getApplicationValue());
//		applicationType.add(ApplicationTypeEnum.SUMMARYCOLLECTIVE.getApplicationValue());
//		applicationType.add(ApplicationTypeEnum.INDIVIDUAL.getApplicationValue());
//		applicationType.add(ApplicationTypeEnum.SUMMARYINDIVIDUAL.getApplicationValue());
//		applicationType.add(ApplicationTypeEnum.ACCOUNT.getApplicationValue());
//		applicationType.add(ApplicationTypeEnum.ACCOUNTTEMPLATE.getApplicationValue());
//		applicationType.add(ApplicationTypeEnum.RECURRING.getApplicationValue());
//		applicationType.add(ApplicationTypeEnum.REPORT.getApplicationValue());
//		applicationType.add(ApplicationTypeEnum.SOURCE.getApplicationValue());
//		applicationType.add(ApplicationTypeEnum.MAPPING.getApplicationValue());
//		applicationType.add(ApplicationTypeEnum.DEFINITION.getApplicationValue());
//		applicationType.add(ApplicationTypeEnum.DIMENSION.getApplicationValue());
//		applicationType.add(ApplicationTypeEnum.DOUBLEENTRY.getApplicationValue());
//		applicationType.add(ApplicationTypeEnum.HIERARCHY.getApplicationValue());
//		applicationType.add(ApplicationTypeEnum.BATCHJOURNAL.getApplicationValue());
//		applicationType.add(ApplicationTypeEnum.COSTCENTER.getApplicationValue());
//		applicationType.add(ApplicationTypeEnum.BUDGET.getApplicationValue());
		return applicationType;
	}


	private Set<String> setModuleType() {
		Set<String> moduleType = new HashSet<String>();
		moduleType.add(ModuleTypeEnum.ALL.getModuleValue());
		moduleType.add(ModuleTypeEnum.CORE.getModuleValue());
		moduleType.add(ModuleTypeEnum.TRX.getModuleValue());
//		moduleType.add(ModuleTypeEnum.LEDGER.getModuleValue());
		return moduleType;
	}


	private Set<String> setActionType() {
		Set<String> actionType = new HashSet<String>();
		actionType.add(ActionTypeEnum.ALL.getId());
		actionType.add(ActionTypeEnum.AUTHORIZE.getId());
		actionType.add(ActionTypeEnum.DELETE.getId());
		actionType.add(ActionTypeEnum.INPUT.getId());
		actionType.add(ActionTypeEnum.EDIT.getId());
		actionType.add(ActionTypeEnum.READ.getId());
		return actionType;
	}


	public static List<GrantedAuthority> getGrantedAuthorities(List<String> roles) {
		List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();

		for (String role : roles) {
			authorities.add(new SimpleGrantedAuthority(role));
		}
		return authorities;
	}


	@Override
	public CoreUser change(CoreUser coreUser) {
		return coreUserDao.change(coreUser);
	}
//	
//	private User mapCoreUser(CoreUser coreUser){
//		User user = new UserEntity();
//		user.setId(coreUser.getUsername());
//		user.setEmail(coreUser.getEmail());
//		user.setFirstName(coreUser.getFirstName());
//		user.setLastName(coreUser.getLastName());
//		return user;
//	}


		@Override
		public List<CoreUser> getByClientUser() {
			return coreUserDao.getByClientUser();
		}


		@Override
		public CoreUser getByToken(String token) {
			return coreUserDao.getByToken(token);
		}


		@Override
		public void setHistoryToSeller(Long id) {
			coreUserDao.setHistoryToSeller(id);
			
		}

}
