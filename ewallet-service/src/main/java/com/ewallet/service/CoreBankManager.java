package com.ewallet.service;

import com.ewallet.persistence.model.CoreBank;

/**
 * @author akbar.wijayanto
 * Date Nov 24, 2015 1:44:39 PM
 */
public interface CoreBankManager extends GenericManager<CoreBank, String> {
	
}
