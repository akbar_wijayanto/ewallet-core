package com.ewallet.service;

import java.util.List;

import com.ewallet.persistence.model.TrxOrderItemDetail;
import com.ewallet.persistence.model.TrxOrderItemId;
import com.ewallet.persistence.model.TrxRecord;

/**
 * @author akbar.wijayanto
 * Date Nov 25, 2015 5:27:15 PM
 */
public interface TrxOrderItemDetailManager extends GenericTransactionManager<TrxOrderItemDetail, TrxOrderItemId> {
	
	void removeByOrderId(String orderId);
	
}
