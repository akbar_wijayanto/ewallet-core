package com.ewallet.service;

import com.ewallet.persistence.model.TrxTopup;

/**
 * @author akbar.wijayanto
 * Date Oct 22, 2015 11:38:14 AM
 */
public interface TrxTopupManager extends GenericTransactionManager<TrxTopup, Long> {
   
}
