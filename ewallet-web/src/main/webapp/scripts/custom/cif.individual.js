jQuery(document).ready(function(){
	$.ajaxSetup ({
		cache: false
	});
	$('#save').click(function(){
        $('#cifindividualdto').validationEngine();
    });
    $('#cancel').click(function(){
        $('#cifindividualdto').validationEngine('detach');
    });
    
    $('#tableList').dataTable( {
        "aoColumnDefs": [
            { "mDataProp": "cifId", "aTargets": [0] },
            { "mDataProp": "fullName", "aTargets": [1] },
            { "mDataProp": "identityNumber", "aTargets": [2] },
            { "mDataProp": "gender", "aTargets": [3] },
            { "mDataProp": "motherMaidenName", "aTargets": [4] },   
            { "mDataProp": "dateBirth", "aTargets": [5] },   
            { "mDataProp": null, "sDefaultContent": "", "mRender" : function ( oObj ) {
                return '<a href=\"'+ base_url+ 'customer/cif/editindividuallong/'+oObj['id']+'\" title=\"Edit\"><i class=\"fa fa-edit\"></i></a>';
                }, "aTargets" : [6]}
        ],
        "bFilter": false,
        "bSort": false,
        "bProcessing": true,
        "bServerSide": true,
//        "sPaginationType": "full_numbers",
        "sAjaxSource": base_url +"/customer/cif/page",
        /*"sScrollX": "90%",
        "sScrollXInner":"100%",*/
        "bScrollCollapse": true
    });
});
