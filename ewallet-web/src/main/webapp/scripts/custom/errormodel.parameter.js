$(document).ready(function(){
	var id = 0;
	$.ajaxSetup ({
		cache: false
	});
	
	
	$('#add').click(function(){
		$('#ids').val(id);
		
	$('#save').click(function(){
	$('#errorModel').validationEngine();
	    });
	
	$('#cancel').click(function(){
	        $('#errorModel').validationEngine('detach');
	    });
		
		$('#selectedTable > tbody:last').append('<tr><td><input type=\"hidden\" name=\"errorModels['+id+'].language.id\" id=\"languageid'+id+'\" ><input type=\"text\" id=\"language'+id+'\" readonly=\"readonly\" class=\"form-control validate[required]\" /></td>'+
				'<td><input name=\"errorModels['+id+'].key\" id=\"key\" class=\"form-control validate[required]\" /></td>'+
				'<td><input name=\"errorModels['+id+'].value\" id=\"value\" class=\"form-control validate[required]\" /></td>'+
				'<td><i class=\"fa fa-trash remove\" style=\"color:red;font-size: 150%;\"></i></td></tr>');
		
		$('#languageid'+id).val($('.languagechoose option:selected').val());
		$('#language'+id).val($('.languagechoose option:selected').text());
		
		id = id+1;

		var rows = $('#selectedTable > tbody > tr');
		$(rows.find(".remove")).click(function () {
			$(this).closest("tr").remove();
		});
		 
	});
});