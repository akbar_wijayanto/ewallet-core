$(document).ready(function(){  
	$('#job').dataTable({
        "aoColumnDefs": [
            { "mDataProp": "jobInstanceInfoId", "aTargets": [0] },
            { "mDataProp": null, "mRender" : function ( oObj ) {
                return '<a href=\"' + base_url + 'core/batch/jobs/' + oObj['jobInstanceName'] + '/' + oObj['jobInstanceInfoId'] + '/executions\">executions</a>';
                }, "aTargets": [1] 
            },
            { "mDataProp": "jobExecutionCount", "aTargets": [2] },
            { "mDataProp": null, "mRender" : function ( oObj ) {
                return '<a href=\"' + base_url + 'core/batch/jobs/executions/' + oObj['lastJobExecutionId'] +'\">' + oObj['lastJobExecutionStatus'] + '</a>';
            	}, "aTargets": [3]
            },
            { "mDataProp": "jobParameters", "aTargets": [4] }
        ],
        "bFilter": false,
        "bSort": false,
        "bProcessing": true,
        "bServerSide": true,
        "sAjaxSource": base_url +"core/batch/closing/page",
//        "sScrollX": "100%",
//        "sScrollXInner": "100%",
//        "sPaginationType": "bootstrap",
        "bScrollCollapse": true
    });
	
});