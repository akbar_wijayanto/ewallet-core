$(document).ready(function(){
	$('#addPhone').click(
			function(index) {
			/*	alert("masuk");*/
				$('#phones tr:last').clone().insertAfter(
				'#phones tr:last').slideDown(
						function() {
							var elements = $(this).find("input");
							for (i = 0; i < elements.length; i++) {
								$(elements[i]).val('');
								getElementPhone();
							}
						});

			});

	$('body').on('click', '#delPhone', function() {
		if ($('#phones tr').length > 1) {
			$(this).parent().parent().remove();
			getElementPhone();
		}
	});

	function getElementPhone() {
		$('#phones tr').each(
				function(index) {
					$(this).find("[id$=phoneId]").attr("name",
							"phone[" + index + "].id");
					$(this).find("[id$=phone]").attr("name",
							"phone[" + index + "].numberPhone");
				});
	}
	
	$('#addMobilePhone').click(
			function(index) {
//				alert("masuk");
				$('#mobilePhones tr:last').clone().insertAfter(
				'#mobilePhones tr:last').slideDown(
						function() {
							var elements = $(this).find("input");
							for (i = 0; i < elements.length; i++) {
								$(elements[i]).val('');
								getElementMobilePhone();
							}
						});

			});

	$('body').on('click', '#delMobilePhones', function() {
		if ($('#mobilePhones tr').length > 1) {
			$(this).parent().parent().remove();
			getElementMobilePhone();
		}
	});

	function getElementMobilePhone() {
		$('#mobilePhones tr').each(
				function(index) {
					$(this).find("[id$=mobilePhoneId]").attr("name",
							"mobilePhone[" + index + "].id");
					$(this).find("[id$=mobilePhone]").attr("name",
							"mobilePhone[" + index + "].numberPhone");
				});
	}
	
	$('#addEmail').click(
			function(index) {
//				alert("masuk");
				$('#emails tr:last').clone().insertAfter(
				'#emails tr:last').slideDown(
						function() {
							var elements = $(this).find("input");
							for (i = 0; i < elements.length; i++) {
								$(elements[i]).val('');
								getElementEmail();
							}
						});

			});

	$('body').on('click', '#delEmail', function() {
		if ($('#emails tr').length > 1) {
			$(this).parent().parent().remove();
			getElementEmail();
		}
	});

	function getElementEmail() {
		$('#emails tr').each(
				function(index) {
					$(this).find("[id$=emailId]").attr("name",
							"email[" + index + "].id");
					$(this).find("[id$=email]").attr("name",
							"email[" + index + "].emailAddress");
				});
	}
	
	$('#addAlternativeAddress').click(
			function(index) {
//				alert("masuk");
				$('#alternativeAddressS tr:last').clone().insertAfter(
				'#alternativeAddressS tr:last').slideDown(
						function() {
							var elements = $(this).find("input");
							for (i = 0; i < elements.length; i++) {
								$(elements[i]).val('');
								getElementAlternativeAddress();
							}
						});

			});

	$('body').on('click', '#delAlternativeAddress', function() {
		if ($('#alternativeAddressS tr').length > 1) {
			$(this).parent().parent().remove();
			getElementAlternativeAddress();
		}
	});

	function getElementAlternativeAddress() {
		$('#alternativeAddressS tr').each(
				function(index) {
					$(this).find("[id$=alternativeAddressId]").attr("name",
							"alternativeAddress[" + index + "].id");
					$(this).find("[id$=alternativeAddress]").attr("name",
							"alternativeAddress[" + index + "].nameAlternativeAddress");
				});
	}
	
	$('#addCompanyAddress').click(
			function(index) {
//				alert("masuk");
				$('#companyAddressS tr:last').clone().insertAfter(
				'#companyAddressS tr:last').slideDown(
						function() {
							var elements = $(this).find("input");
							for (i = 0; i < elements.length; i++) {
								$(elements[i]).val('');
								getElementCompanyAddress();
							}
						});

			});

	$('body').on('click', '#delCompanyAddress', function() {
		if ($('#companyAddressS tr').length > 1) {
			$(this).parent().parent().remove();
			getElementCompanyAddress();
		}
	});

	function getElementCompanyAddress() {
		$('#companyAddressS tr').each(
				function(index) {
					$(this).find("[id$=companyAddressId]").attr("name",
							"companyAddress[" + index + "].id");
					$(this).find("[id$=companyAddress]").attr("name",
							"companyAddress[" + index + "].nameCorporateAddress");
				});
	}
	
	$('#addSourcesOfFunds').click(
			function(index) {
//				alert("masuk");
				$('#sourcesOfFundS tr:last').clone().insertAfter(
				'#sourcesOfFundS tr:last').slideDown(
						function() {
							var elements = $(this).find("input");
							for (i = 0; i < elements.length; i++) {
								$(elements[i]).val('');
								getElementSourcesFunds();
							}
						});
			});

	$('body').on('click', '#delSourcesOfFund', function() {
		if ($('#sourcesOfFundS tr').length > 1) {
			$(this).parent().parent().remove();
			getElementSourcesFunds();
		}
	});

	function getElementSourcesFunds() {
		$('#sourcesOfFundS tr').each(
				function(index) {
					$(this).find("[id$=sourcesOfFundsId]").attr("name",
							"sourcesOfFunds[" + index + "].id");
					$(this).find("[id$=sourcesOfFunds]").attr("name",
							"sourcesOfFunds[" + index + "].sourcesOfFund");
					
				});

	}
	
	$('#addWorkProhibition').click(
			function(index) {
//				alert("masuk");
				$('#workstationProhibitions tr:last').clone().insertAfter(
				'#workstationProhibitions tr:last').slideDown(
						function() {
							var elements = $(this).find("input");
							for (i = 0; i < elements.length; i++) {
								$(elements[i]).val('');
								getElementworkstationProhibition();
							}
						});

			});

	$('body').on('click', '#delWorkstationProhibition', function() {
		if ($('#workstationProhibitions tr').length > 1) {
			$(this).parent().parent().remove();
			getElementworkstationProhibition();
		}
	});

	function getElementworkstationProhibition() {
		$('#workstationProhibitions tr').each(
				function(index) {
					$(this).find("[id$=workstationProhibitionId]").attr("name",
							"workstationProhibition[" + index + "].id");
					$(this).find("[id$=sourcesOfFunds]").attr("name",
							"sourcesOfFunds[" + index + "].nameWorkStation");
				});
	}
	
	$('#addWorkRestriction').click(
			function(index) {
//				alert("masuk");
				$('#workstationRestrictions tr:last').clone().insertAfter(
				'#workstationRestrictions tr:last').slideDown(
						function() {
							var elements = $(this).find("input");
							for (i = 0; i < elements.length; i++) {
								$(elements[i]).val('');
								getElementworkstationRestriction();
							}
						});
			});

	$('body').on('click', '#delWorkstationRestriction', function() {
		if ($('#workstationRestrictions tr').length > 1) {
			$(this).parent().parent().remove();
			getElementworkstationRestriction();
		}
	});

	function getElementworkstationRestriction() {
		$('#workstationRestrictions tr').each(
				function(index) {
					$(this).find("[id$=workstationRestrictionId]").attr("name",
							"workstationRestriction[" + index + "].id");
					$(this).find("[id$=workstationRestriction]").attr("name",
							"workstationRestriction[" + index + "].nameWorkStation");
				});
	}
	
	$('#addNamePersonInContact').click(
			function(index) {
//				alert("masuk");
				$('#namePersonInContacts tr:last').clone().insertAfter(
				'#namePersonInContacts tr:last').slideDown(
						function() {
							var elements = $(this).find("input");
							for (i = 0; i < elements.length; i++) {
								$(elements[i]).val('');
								getElementPersonInContacts();
							}
						});

			});

	$('body').on('click', '#delNamePersonInContact', function() {
		if ($('#namePersonInContacts tr').length > 1) {
			$(this).parent().parent().remove();
			getElementPersonInContacts();
		}
	});

	function getElementPersonInContacts() {
		$('#namePersonInContacts tr').each(
				function(index) {
					$(this).find("[id$=namePersonInContact]").attr("name",
							"namePersonInContact[" + index + "].namePersonInContact");
					$(this).find("[id$=namePersonInContact]").attr("name",
							"namePersonInContact[" + index + "].namePersonInContact");
					
				});

	}
	
	$('#addNoLegality').click(
			function(index) {
//				alert("masuk");
				$('#noLegalitys tr:last').clone().insertAfter(
				'#noLegalitys tr:last').slideDown(
						function() {
							var elements = $(this).find("input");
							for (i = 0; i < elements.length; i++) {
								$(elements[i]).val('');
								getElementNoLegality();
							}
						});

			});

	$('body').on('click', '#delNoLegality', function() {
		if ($('#noLegalitys tr').length > 1) {
			$(this).parent().parent().remove();
			getElementNoLegality();
		}
	});

	function getElementNoLegality() {
		$('#noLegalitys tr').each(
				function(index) {
					$(this).find("[id$=noLegality]").attr("name",
							"noLegality[" + index + "].id");
					$(this).find("[id$=noLegality]").attr("name",
							"noLegality[" + index + "].noLegality");
				});
	}
	
	
	
});
