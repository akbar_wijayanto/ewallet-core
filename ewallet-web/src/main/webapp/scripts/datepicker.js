$('input:text.datepicker').datepicker({
	format: dateformat,
	orientation: "auto left",
	autoclose: true,
//	daysOfWeekDisabled: "0,6",
	setDate :  new Date(tyear,tmonth,tdate)
});

$('input:text.customDate').datepicker({
	format: dateformat,
	orientation: "auto left",
	autoclose: true,
//	daysOfWeekDisabled: "0,6"
});

$('input:text.datepicker').each(function(i, obj) {
	if(!obj.value){
		$(this).datepicker("setDate",  new Date(tyear,tmonth,tdate));
	}
});

///$("input:text.datepicker").datepicker("setDate",  new Date(tyear,tmonth,tdate));

$("#productLimitDate.input-daterange").datepicker({
	format: dateformat,
	orientation: "auto right",
	autoclose: true,
	startDate: new Date(tyear,tmonth,tdate)
//,
//	daysOfWeekDisabled: "0,6"
});

$('.input-daterange').datepicker({
	format: dateformat,
	orientation: "auto right",
	autoclose: true,
//	daysOfWeekDisabled: "0,6"
});
