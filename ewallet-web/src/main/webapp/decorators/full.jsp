<%@ include file="/common/taglibs.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <%@ include file="/common/meta.jsp" %>
    <title><decorator:title/> | <fmt:message key="webapp.name"/></title>
    <link rel="stylesheet" href="<c:url value='/styles/bootstrap.css' />" />
    <link rel="stylesheet" href="<c:url value='/styles/font-awesome.css'/>" />
    <link rel="stylesheet" href="<c:url value='/styles/datepicker3.css' />" />
    <link rel="stylesheet" href="<c:url value='/styles/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css' />" />
    <link rel="stylesheet" href="<c:url value='/styles/bootstrap-slider/slider.css' />" />
    <link rel="stylesheet" href="<c:url value='/styles/colorpicker/bootstrap-colorpicker.min.css' />" />
    <link rel="stylesheet" href="<c:url value='/styles/datatables/dataTables.bootstrap.css' />" />
    <link rel="stylesheet" href="<c:url value='/styles/daterangepicker/daterangepicker-bs3.css' />" />
    <link rel="stylesheet" href="<c:url value='/styles/fullcalendar/fullcalendar.css' />" />
    <link rel="stylesheet" href="<c:url value='/styles/fullcalendar/fullcalendar.print.css' />" />
    <link rel="stylesheet" href="<c:url value='/styles/iCheck/all.css' />" />
    <link rel="stylesheet" href="<c:url value='/styles/ionslider/ion.rangeSlider.css' />" />
    <link rel="stylesheet" href="<c:url value='/styles/ionslider/ion.rangeSlider.skinFlat.css' />" />
    <link rel="stylesheet" href="<c:url value='/styles/ionslider/ion.rangeSlider.skinNice.css' />" />
    <link rel="stylesheet" href="<c:url value='/styles/jQueryUI/jquery-ui-1.10.3.custom.min.css' />" />
    <link rel="stylesheet" href="<c:url value='/styles/jvectormap/jquery-jvectormap-1.2.2.css' />" />
    <link rel="stylesheet" href="<c:url value='/styles/morris/morris.css' />" />
    <link rel="stylesheet" href="<c:url value='/styles/timepicker/bootstrap-timepicker.min.css' />" />
    <link rel="stylesheet" href="<c:url value='/styles/flags.css' />" />
    <link rel="stylesheet" href="<c:url value='/styles/validationEngine.jquery.css' />" />
    <link rel="stylesheet" href="<c:url value='/styles/AdminLTE.css' />" />
   
	
	<script type="text/javascript" src="<c:url value='/scripts/jquery.min.js' />"></script>
	<script type="text/javascript" src="<c:url value='/scripts/breadcumb.js' />"></script>
	<script type="text/javascript" src="<c:url value='/scripts/notify.js' />"></script>
	<%-- <script type="text/javascript" src="<c:url value='/scripts/common.js' />"></script> --%>
	
	<script type="text/javascript">
	     $(function() {
	       $("#tableList").dataTable({"bStateSave": true});
	     });
 	</script>
	
    <!--[if lte IE 8]>
    <link rel="stylesheet" href="<c:url value='/styles/ie/ie.css' />" />
    <![endif]-->
    <!--[if lt IE 9]>
    <script src="<c:url value='/scripts/ie/html5.js' />"></script>
    <script src="<c:url value='/scripts/ie/respond.min.js' />"></script>
    <script src="<c:url value='/scripts/excanvas.min.js' />"></script>
    <![endif]-->
    <fmt:formatDate pattern="${datePattern}" value="${htoday}" var="today"/>
	<script type="text/javascript">
        <%@ include file="/scripts/variable.js"%>
    </script>
    
    <decorator:head/>
</head>
<body class="skin-orange fixed">
	<jsp:include page="/common/header.jsp"/>
    <div class="wrapper row-offcanvas row-offcanvas-left" style='background : #fff;'>
    	<jsp:include page="/common/menu.jsp"/>
    	
    	<aside class="right-side">
    		 <!-- Content Header (Page header) -->
             <section class="content-header">  
             	 <h1 id="breadcumbTitles">
                     &nbsp;
                 </h1>           	 
                 <ol class="breadcrumb">
                 	 <i class="fa fa-dashboard"></i>&nbsp;
                 	 <c:set var="crumbs" value=""/>
	             	 <c:forEach  var="entry" items="${CB_BREADCRUMBS}">
	                     <c:set var="crumbs" value="${crumbs}/${entry}"/>
	                     <li>
	                         <a href="<c:url value='${crumbs}' />"><fmt:message key="menu.${entry}" /></a>
	                     </li>
	                 </c:forEach>
                 </ol>
             </section>
             
             <section class="content">
				<jsp:include page="/common/messages.jsp"/>        
	            <decorator:body/>	            	            
             </section>
             <jsp:include page="/common/footer.jsp"/>
    	</aside>
    </div>
     
    <script type="text/javascript" src="<c:url value='/scripts/jquery-ui-1.10.3.min.js' />"></script>
    <script type="text/javascript" src="<c:url value='/scripts/jquery.number.min.js' />"></script>
	<script type="text/javascript" src="<c:url value='/scripts/format.amount.js' />"></script>
    <script type="text/javascript" src="<c:url value='/scripts/bootstrap.min.js' />"></script>
    <script type="text/javascript" src="<c:url value='/scripts/bootstrap.datepicker.min.js' />"></script>
    <script type="text/javascript" src="<c:url value='/scripts/AdminLTE/app.js' />"></script> 
    <script type="text/javascript" src="<c:url value='/scripts/notification.js' />"></script>  
    <script type="text/javascript" src="<c:url value='/scripts/jquery.validationEngine.js' />"></script>
	<script type="text/javascript" src="<c:url value='/scripts/jquery.validationEngine-id.js' />"></script>  
    <script type="text/javascript" src="<c:url value='/scripts/jquery.validationEngine-en.js' />"></script>  
    <script type="text/javascript" src="<c:url value='/scripts/plugins/datatables/jquery.dataTables.min.js' />"></script>
    <script type="text/javascript" src="<c:url value='/scripts/plugins/datatables/dataTables.bootstrap.js' />"></script>
    <script type="text/javascript" src="<c:url value='/scripts/plugins/highcharts/highcharts.js' />"></script>
    <script type="text/javascript" src="<c:url value='/scripts/plugins/highcharts/modules/exporting.js' />"></script>
	<script type="text/javascript" src="<c:url value='/scripts/datepicker.js' />"></script>
	<script type="text/javascript" src="<c:url value='/scripts/custom/custom.breadcrumb.js' />"></script>
	<script type="text/javascript" src="<c:url value='/scripts/jshashtable-3.0.js' />"></script>
	<script type="text/javascript" src="<c:url value='/scripts/jquery.numberformatter-1.2.3.min.js' />"></script>
	<script type="text/javascript" src="<c:url value='/scripts/custom/sidebar.custom.js' />"></script>
	<script type="text/javascript" src="<c:url value='/scripts/dateFormat.js' />"></script>
	<script type="text/javascript" src="<c:url value='/scripts/jquery.dateFormat.js' />"></script>
	<script type="text/javascript" src="<c:url value='/scripts/particles.js' />"></script>
    
    <script type="text/javascript">
	    $(function() {
	        $("#tableList").dataTable();
	    });
	</script>

	<script>
		/*         */
		/* jQuery(document).ready(function() {
			App.init(); // init the rest of plugins and elements
		}); */
		
		function getParent(parentId) {
			if (document.getElementById(parentId)) {
				$("ul #" + parentId).addClass("active open");
				$("ul #" + parentId + " a .arrow").addClass("open");
			} else {
				$("ul #0").addClass("active open");
				$("ul #0 a .arrow").addClass("open");
			}
		}
		/*   */
	</script> 
</body>
</html>