<%@ include file="/common/taglibs.jsp"%>
<head>
<title><fmt:message key='coreerrormodel.title' /></title>
<meta name="heading" content="<fmt:message key='coreerrormodel'/>" />
</head>

<div class="row">
	<div class="col-xs-12">
		<div class="box">
			<div id="breadcumbTitle">
				<fmt:message key='error.message.add' />
			</div>

			<spring:url var="action" value='/error/add' />
			<form:form commandName="error" method="post"
				action="${action}" id="error" class="form-horizontal">
				<div class="box-body">
					<input type="hidden" name="from"
						value="<c:out value="${param.from}"/>" />
					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label path="language" for="language"
								cssClass="control-label pull-right"
								csscoreerrormodelClass="control-label pull-right">
								<fmt:message key="errormodel.language" />
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<form:input path="language" id="language" cssClass="form-control validate[required, max[50]]"
								csscoreerrormodelClass=" form-control has-coreerrormodel validate[required, max[50]]" />
							<form:coreerrormodels path="language" cssClass="coreerrormodel" />
						</div>
					</div>

					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label path="key" for="key"
								cssClass="control-label pull-right"
								csscoreerrormodelClass="control-label pull-right">
								<fmt:message key="coreerrormodel.key" />
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<form:input path="key" id="key" cssClass="form-control validate[required, max[50]]"
								 csscoreerrormodelClass="form-control has-coreerrormodel validate[required]" />
							<form:coreerrormodels path="key" cssClass="coreerrormodel" />
						</div>
					</div>

					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label path="value" for="value"
								cssClass="control-label pull-right" csscoreerrormodelClass="control-label pull-right">
								<fmt:message key="coreerrormodel.value" />
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<form:input path="value" id="value"
								cssClass="form-control validate[required, max[50]]"
								csscoreerrormodelClass="form-control has-coreerrormodel validate[required, max[255]]" />
							<form:coreerrormodels path="value" cssClass="coreerrormodel" />
						</div>
					</div>

				</div>
				<div class="box-footer">
					<div class="form-group">
						<div class="col-xs-12 col-md-9">
							<div class="pull-right">
								<input type="submit" class="btn btn-primary" name="save" id="save" value="<fmt:message key="button.add"/>" /> 
                                <a href="/casa-web/coreerrormodel"><input type="button" class="btn btn-danger" name="cancel" id="cancel" value="<fmt:message key="button.cancel"/>" /></a>
							</div>
						</div>
					</div>
				</div>

			</form:form>

		</div>
	</div>
</div>


<div class="row">
	<div class="col-xs-12">
		<div class="col-xs-12">
		<div class="box">
			<div id="breadcumbTitle">
				<fmt:message key='coreerrormodel.message.list' />
			</div>

			<div class="box-body table-responsive">
				<table id="tableList" class="table table-bordered table-striped">
					<thead>
						<tr>
							
							<th><fmt:message key='coreerrormodel.language' /></th>
							<th><fmt:message key='coreerrormodel.key' /></th>
							<th><fmt:message key='coreerrormodel.value' /></th>
							<th></th>
							
						</tr>
					</thead>
					<tbody>
						<c:forEach items="${coreerrormodels }" var="er">
						<tr>
							
							<td>${ er.language}</td>
							<td>${ er.key}</td>
							<td>${ ip.value }</td>
							<td><a href="<c:url value='/coreerrormodel/edit/${ er.id}' />" title="Edit"><i class="fa fa-edit"></i></a>
							<a href="javascript:remove('${ er.id}')" title="Delete"><input type="hidden" id="removeid" value="" /><i class="fa fa-trash-o"></i></a></td>
						</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
		</div>
	</div>
	</div>
</div>

