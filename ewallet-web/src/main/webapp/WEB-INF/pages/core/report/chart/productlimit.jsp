<%@ include file="/common/taglibs.jsp"%>

<head>
<title><fmt:message key='coreReport.title' /></title>
<meta name="heading"
	content="<fmt:message key='coreReport.heading'/>" />
<%-- <script type="text/javascript" src="<c:url value='/scripts/custom/core.report.chart.productlimit.js' />"></script> --%>
<script>
$(function(){
	var base_url = '<c:url value='/' />';
	$.ajax({
		url : base_url + '/core/report/chart/data/1',
		success : function(data) {
			$('#container').highcharts({
				chart: {
					plotBackgroundColor: null,
					plotBorderWidth: null,
					plotShadow: false
				},
				title: {
					text: 'Total Account Per Product'
				},
				tooltip: {
					pointFormat: '{series.name}: <b>{point.y} Account</b>'
				},
				plotOptions: {
					pie: {
						allowPointSelect: true,
						cursor: 'pointer',
						dataLabels: {
							enabled: true,
							format: '<b>{point.name}</b>: {y} Account',
							style: {
								color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
							}
						}
					}
				},
				series: [{
					type: 'pie',
					name: 'Total Account Per Product',
					data: data
				}]
			});
		}
	});
});
</script>
</head>
<div class="row">
	<div class="col-xs-12">
	    <div class="box">
	       	<div id="breadcumbTitle"><fmt:message key="coreReport.message" /></div>
	       	
	        <div class="box-body">
	        
	        <div id="container"></div>
	        	
			</div>
		</div>
    </div>
</div>