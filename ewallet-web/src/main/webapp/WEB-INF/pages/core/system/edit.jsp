<%@ include file="/common/taglibs.jsp"%>
<head>
<title><fmt:message key="systemWide.title" /></title>
<meta name="heading" content="<fmt:message key='systemWide.heading'/>" />

</head>
<div class="row">
	<div class="col-xs-12">
		<div class="box">
			<div id="breadcumbTitle">
				<fmt:message key='systemWide.message.edit' />
			</div>

			<spring:url var="action" value='/core/system/edit' />
			<form:form commandName="systemWide" method="post" action="${action}"
				id="systemWide" class="form-horizontal">
				<div class="box-body">

					<input type="hidden" name="from"
						value="<c:out value="${param.from}"/>" />

					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label for="" path="" cssClass="control-label pull-right"
								cssErrorClass="control-label">
								<fmt:message key="systemWide.online" />
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<input type="text" readonly="readonly" value="${status}"
								class="form-control" />
							<form:errors path="" cssClass="has-error" />
						</div>
					</div>

					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label for="todayDate" path="todayDate"
								cssClass="control-label pull-right"
								cssErrorClass="control-label">
								<fmt:message key="systemWide.today" />
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<div class="input-group">
								<form:input path="todayDate" readonly="true"
									class="form-control" />
								<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
							</div>
							<form:errors path="todayDate" cssClass="has-error" />
						</div>
					</div>

					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label for="nextDate" path="nextDate"
								cssClass="control-label pull-right"
								cssErrorClass="control-label">
								<fmt:message key="systemWide.nextDate" />
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<div class="input-group">
								<form:input path="nextDate" readonly="true" class="form-control" />
								<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
							</div>
							<form:errors path="nextDate" cssClass="has-error" />
						</div>
					</div>

					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label for="previousDate" path="previousDate"
								cssClass="control-label pull-right"
								cssErrorClass="control-label">
								<fmt:message key="systemWide.prevDate" />
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<div class="input-group">
								<form:input path="previousDate" readonly="true"
									class="form-control" />
								<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
							</div>
							<form:errors path="previousDate" cssClass="has-error" />
						</div>
					</div>

<!-- 					<div class="form-group"> -->
<!-- 						<div class="col-xs-4 col-md-3"> -->
<%-- 							<form:label for="defaultHoliday" path="defaultHoliday" cssClass="control-label pull-right" --%>
<%-- 								cssErrorClass="control-label"> --%>
<%-- 								<fmt:message key="systemWide.defaultHoliday" /> --%>
<%-- 							</form:label> --%>
<!-- 						</div> -->
<!-- 						<div class="col-xs-8 col-md-6"> -->
<%-- 							<form:input path="defaultHoliday" class="form-control" maxlength="7" /> --%>
<%-- 							<form:errors path="defaultHoliday" cssClass="has-error" /> --%>
<!-- 						</div> -->
<!-- 					</div> -->

					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label for="companyName" path="companyName"
								cssClass="control-label pull-right"
								cssErrorClass="control-label">
								<fmt:message key="systemWide.companyName" />
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<form:input path="companyName" id="companyName"
								cssClass="form-control" />
							<form:errors path="companyName" cssClass="has-error" />
						</div>
					</div>

					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label for="status" path="status"
								cssClass="control-label pull-right"
								cssErrorClass="control-label">
								<fmt:message key="systemWide.status" />
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<form:input path="status" readonly="true" id="status"
								cssClass="form-control" />
							<form:errors path="status" cssClass="has-error" />
						</div>
					</div>

<!-- 					<div class="form-group"> -->
<!-- 						<div class="col-xs-4 col-md-3"> -->
<%-- 							<form:label for="status" path="status" --%>
<%-- 								cssClass="control-label pull-right" --%>
<%-- 								cssErrorClass="control-label"> --%>
<%-- 								<fmt:message key="systemWide.holidayPosting" /> --%>
<%-- 							</form:label> --%>
<!-- 						</div> -->
<!-- 						<div class="col-xs-8 col-md-6"> -->
<%-- 							<form:select path="holidayPosting" cssClass="form-control"> --%>
<%-- 								<c:forEach items="${holidayPosting }" var="item"> --%>
<%-- 									<form:option value="${item }">${item }</form:option> --%>
<%-- 								</c:forEach> --%>
<%-- 							</form:select> --%>
<%-- 							<form:errors path="holidayPosting" cssClass="error" /> --%>
<!-- 						</div> -->
<!-- 					</div> -->
				</div>

				<div class="box-footer">
					<div class="form-group">
						<div class="col-xs-12 col-md-9">
							<div class="pull-right">
								<input type="submit" class="btn btn-primary" name="save"
									id="save" value="<fmt:message key="button.save"/>" />
							</div>
						</div>
					</div>
				</div>
			</form:form>
		</div>
	</div>
</div>