<%@ include file="/common/taglibs.jsp"%>
<head>
<title><fmt:message key="coreAuditTrail.title" /></title>
<meta name="heading"
	content="<fmt:message key='coreAuditTrail.heading'/>" />
<script type="text/javascript"
	src="<c:url value='/scripts/custom/core.audittrail.js' />"></script>
</head>
<div class="row">
	<div id="breadcumbTitle">
		<fmt:message key='coreAuditTrail.message' />
	</div>

	<form class="form-horizontal">
		<div class="col-xs-12">
			<div class="box">
				<div class="box-body">
					<div class="form-group">
						<div class="col-xs-2">
							<label class="control-label pull-right"> <fmt:message
									key="coreAuditTrail.objectName" />
							</label>
						</div>
						<div class="col-xs-4">
							<input type="text" name="objectName" id="objectName"
								value="${objectName}"
								class="form-control validate[required,maxSize[255]]" />
						</div>
					</div>

					<div class="form-group">
						<div class="col-xs-2">
							<label class="control-label pull-right"> <fmt:message
									key="coreAuditTrail.fieldName" />
							</label>
						</div>
						<div class="col-xs-4">
							<input type="text" name="fieldName" id="fieldName"
								value="${fieldName}"
								class="form-control validate[required,maxSize[255]]" />
						</div>
					</div>

					<div class="form-group">
						<div class="col-xs-2">
							<label class="control-label pull-right"> <fmt:message
									key="coreAuditTrail.dateFrom" />
							</label>
						</div>
						<div class="col-xs-4">
							<div class="input-group">
								<input type="text" name="dateFrom" id="dateFrom"
									value="${dateFrom}"
									class="form-control datepicker validate[required,custom[date],future[now]]" />
								<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
							</div>
						</div>
					</div>

					<div class="form-group">
						<div class="col-xs-2">
							<label class="control-label pull-right"> <fmt:message
									key="coreAuditTrail.dateTo" />
							</label>
						</div>
						<div class="col-xs-4">
							<div class="input-group">
								<input type="text" name="dateTo" id="dateTo" value="${dateTo}"
									class="form-control datepicker validate[required,custom[date],future[now]]" />
								<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
							</div>
						</div>
					</div>

					<div class="form-group">
						<div class="col-xs-2">
							<label class="control-label pull-right"> <fmt:message
									key="coreAuditTrail.actor" />
							</label>
						</div>
						<div class="col-xs-4">
							<input type="text" name="actor" id="actor" value="${actor}"
								class="form-control validate[required,maxSize[255]]" />
						</div>
					</div>


					<div class="form-group">
						<div class="col-xs-2">
							<label class="control-label pull-right"> <fmt:message
									key="coreAuditTrail.action" />
							</label>
						</div>
						<div class="col-xs-4">
							<input type="text" name="action" id="action" value="${action}"
								class="form-control validate[required,maxSize[255]]" />
						</div>
					</div>

					<div class="box-footer">
						<div class="form-group">
							<div class="col-xs-6">
								<div class="pull-right">
									<input type="submit" class="btn btn-primary" name="search"
										id="searchBtn" value="<fmt:message key="button.search"/>" />
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</form>

	<div class="col-xs-12">
		<div class="box">
			<div class="box-body table-responsive">
				<table class="table table-bordered table-striped" id="auditTable">
					<thead>
						<tr>
							<th width="5%"><fmt:message key='coreAuditTrail.actor' />
							<th width="33"><fmt:message key='coreAuditTrail.objectName'></fmt:message>
							<th width="15%"><fmt:message key='coreAuditTrail.fieldName' /></th>
							<th width="10%"><fmt:message key='coreAuditTrail.before' /></th>
							<th width="25%"><fmt:message key='coreAuditTrail.after' /></th>
							<th width="10%"><fmt:message key='coreAuditTrail.time' /></th>
							<th width="12%"><fmt:message key='coreAuditTrail.action' /></th>
							<th width="40px">&nbsp;</th>
						</tr>
					</thead>
					<tbody>
<%-- 						<c:forEach var="audits" items="${coreAuditTrail}"> --%>
<!-- 							<tr> -->
<%-- 								<td>${audits.actor}</td> --%>
<%-- 								<td><c:set var="object" value="${audits.objectName}" /> --%>
<%-- 									${fn:substringAfter(object, --%>
<%-- 									'com.anabatic.casa.persistence.model.')}</td> --%>
<%-- 								<td>${audits.fieldName}</td> --%>
<%-- 								<td>${audits.before}</td> --%>
<%-- 								<td>${audits.after}</td> --%>
<%-- 								<td>${audits.time}</td> --%>
<%-- 								<td>${audits.action}</td> --%>
<!-- 								<td><a -->
<%-- 									href="<c:url value="/core/audittrail/view/${audits.id}" />" --%>
<!-- 									class="fa fa-eye" title="View"></a></td> -->
<!-- 							</tr> -->
<%-- 						</c:forEach> --%>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>