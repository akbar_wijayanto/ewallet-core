<%@ include file="/common/taglibs.jsp"%>

<head>
<title><fmt:message key="coreUser.title" /></title>
<meta name="heading" content="<fmt:message key='coreUser.heading'/>" />
</head>
<div class="row">
	<div class="col-xs-12">
		<div class="box">
			<div id="breadcumbTitle">
				<fmt:message key='coreBranch.message.list' />
			</div>

			<c:if test="${not empty user}">
				<div class="box-body table-responsive">
					<table class="table table-condensed table-striped table-bordered"
						id="tableList">
						<thead>
							<tr>
								<th width="21%" class="head0"><fmt:message
										key='coreUser.roleId' /></th>
								<th width="21%" class="head1"><fmt:message
										key='coreUser.roleName' /></th>
								<th>&nbsp;</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="us" items="${user.coreRoles}">

								<tr>
									<td>${us.id}</td>
									<td>${us.name}</td>

									<td class="center"><c:if
											test="${us.id != user.activeRole.id }">
											<a href="<c:url value='/core/user/changerole/${us.id}' />"
												title="Activate"><i class="fa fa-play"></i></a>&nbsp;
	                              	</c:if> <c:if
											test="${us.id == user.activeRole.id }">
	                                	Already active
	                                </c:if></td>
								</tr>

							</c:forEach>
						</tbody>
					</table>
				</div>
			</c:if>
		</div>
	</div>
</div>


