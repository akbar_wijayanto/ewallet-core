<%@ include file="/common/taglibs.jsp"%>
<head>
    <title><fmt:message key="coreUser.title"/></title>
    <meta name="heading" content="<fmt:message key='coreUser.heading'/>"/>
    <script type="text/javascript" src="<c:url value="/scripts/custom/core.coreuser.js"/>"></script>
</head>
<div class="row-fluid">
    <div class="span12">
        <h3 class="heading"><fmt:message key="coreUser.message.delete"/></h3>
            <spring:bind path="coreUser.*">
            <c:if test="${not empty status.errorMessages}">
            <div class="alert alert-error">
                <a class="close" data-dismiss="alert">x</a>
                <c:forEach var="error" items="${status.errorMessages}">
                    <c:out value="${error}" escapeXml="false"/><br />
                </c:forEach>
            </div>
        </c:if>
        </spring:bind>
        <spring:url var = "action" value='/core/coreuser/delete/' />
        <form:form commandName="coreUser" method="post" action="${action}"  id="coreUser" class="form-horizontal">
            <input type="hidden" name="from" value="<c:out value="${param.from}"/>"/>

			<div class="control-group">
                <form:label path="id" cssClass="control-label" cssErrorClass="control-label has_error" ><fmt:message key="coreUser.id" /></form:label>
                <div class="controls">
                    <form:input path="id" readonly="true" id="username" cssClass="input-large validate[required]" cssErrorClass="input-large has_error validate[required]"/>
                    <form:errors path="id" cssClass="has_error"/>
                </div>
            </div>

            <div class="control-group">
                <form:label path="username" cssClass="control-label" cssErrorClass="control-label has_error" ><fmt:message key="coreUser.username" /></form:label>
                <div class="controls">
                    <form:input path="username" readonly="true" id="username" cssClass="input-large validate[required]" cssErrorClass="input-large has_error validate[required]"/>
                    <form:errors path="username" cssClass="has_error"/>
                </div>
            </div>

            <div class="control-group">
                <form:label path="email" cssClass="control-label" cssErrorClass="control-label has_error" ><fmt:message key="coreUser.email" /></form:label>
                <div class="controls">
                    <form:input path="email" readonly="true" id="email" cssClass="input-large" cssErrorClass="input-large has_error"/>
                    <form:errors path="email" cssClass="has_error"/>
                </div>
            </div>
            
            <div class="control-group">
                <form:label path="firstName" cssClass="control-label" cssErrorClass="control-label has_error" ><fmt:message key="coreUser.firstName" /></form:label>
                <div class="controls">
                    <form:input path="firstName" readonly="true" id="firstName" cssClass="input-large" cssErrorClass="input-large has_error validate[required]"/>
                    <form:errors path="firstName" cssClass="has_error"/>
                </div>
            </div>
            
            <div class="control-group">
                <form:label path="lastName" cssClass="control-label" cssErrorClass="control-label has_error" ><fmt:message key="coreUser.lastName" /></form:label>
                <div class="controls">
                    <form:input path="lastName" readonly="true" id="lastName" cssClass="input-large" cssErrorClass="input-large has_error"/>
                    <form:errors path="lastName" cssClass="has_error"/>
                </div>
            </div>
            
            <div class="control-group">
                <form:label path="personnelCode" cssClass="control-label" cssErrorClass="control-label has_error" ><fmt:message key="coreUser.personnelCode" /></form:label>
                <div class="controls">
                    <form:input path="personnelCode" readonly="true" id="personnelCode" cssClass="input-large" cssErrorClass="input-large has_error"/>
                    <form:errors path="personnelCode" cssClass="has_error"/>
                </div>
            </div>
            
            <div class="form-actions">
                <input type="submit" class="btn btn-primary" name="delete" id="delete"  value="<fmt:message key="button.delete"/>"/>
                <input type="submit" class="btn btn-danger" name="cancel" id="cancel"  value="<fmt:message key="button.cancel"/>"/>
            </div>
        </form:form>
    </div>
</div>