<%@ include file="/common/taglibs.jsp"%>
<head>
    <title><fmt:message key="coreUser.title"/></title>
    <meta name="heading" content="<fmt:message key='coreUser.heading'/>"/>
    <script type="text/javascript" src="<c:url value="/scripts/custom/core.coreuser.js"/>"></script>
</head>
<div class="row">
	<div class="col-xs-12">
		<div class="box">
			<div id="breadcumbTitle">
				<fmt:message key='coreUser.message.edit' />
			</div>
	        <spring:url var = "action" value='/core/coreuser/edit/${coreUser.id}' />
	        <form:form commandName="coreUser" method="post" action="${action}"  id="coreUser" class="form-horizontal">
				<div class="box-body">
		            <input type="hidden" name="from" value="<c:out value="${param.from}"/>"/>
					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label for="username" path="username" cssClass="control-label pull-right"
								cssErrorClass="control-label pull-right">
								<fmt:message key="coreUser.username" />
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<form:input path="username" id="username" cssClass="form-control validate[required]"
								cssErrorClass="form-control has-error validate[required]" readonly="true" />
							<form:errors path="username" cssClass="has-error" />
						</div>
					</div>
		            
		            <div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label for="accountEnabled" path="accountEnabled" cssClass="control-label pull-right"
								cssErrorClass="control-label pull-right">
								<fmt:message key="coreUser.accountEnabled" />
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
		                    <form:checkbox  path="accountEnabled" id="accountEnabled" cssErrorClass="has-error"/>
		                    <form:errors path="accountEnabled" cssClass="has-error"/>
						</div>
					</div>
		            
		            <div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label for="accountExpired" path="accountExpired" cssClass="control-label pull-right"
								cssErrorClass="control-label pull-right">
								<fmt:message key="coreUser.accountExpired" />
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
		                    <form:checkbox  path="accountExpired" id="accountExpired" cssErrorClass="has-error"/>
		                    <form:errors path="accountExpired" cssClass="has-error"/>
						</div>
					</div>
		            	
					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label for="email" path="email" cssClass="control-label pull-right"
								cssErrorClass="control-label pull-right">
								<fmt:message key="User.email" />
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<form:input path="email" id="email" cssClass="form-control validate[required]"
								cssErrorClass="form-control has-error validate[required]" />
							<form:errors path="email" cssClass="has-error" />
						</div>
					</div>
		            
		            <div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label for="firstName" path="firstName" cssClass="control-label pull-right"
								cssErrorClass="control-label pull-right">
								<fmt:message key="User.firstName" />
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<form:input path="firstName" id="firstName" cssClass="form-control validate[required]"
								cssErrorClass="form-control has-error validate[required]" />
							<form:errors path="firstName" cssClass="has-error" />
						</div>
					</div>
					
					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label for="lastName" path="lastName" cssClass="control-label pull-right"
								cssErrorClass="control-label pull-right">
								<fmt:message key="coreUser.lastName" />
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<form:input path="lastName" id="lastName" cssClass="form-control"
								cssErrorClass="form-control has-error" />
							<form:errors path="lastName" cssClass="has-error" />
						</div>
					</div>
					
					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label for="personnelCode" path="personnelCode" cssClass="control-label pull-right"
								cssErrorClass="control-label pull-right">
								<fmt:message key="coreUser.personnelCode" />
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<form:input path="personnelCode" id="personnelCode" cssClass="form-control"
								cssErrorClass="form-control has-error" />
							<form:errors path="personnelCode" cssClass="has-error" />
						</div>
					</div>
					
					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label for="limitAmount" path="limitAmount" cssClass="control-label pull-right"
								cssErrorClass="control-label pull-right">
								<fmt:message key="User.limitAmount" />
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<form:input path="limitAmount" id="limitAmount" cssClass="form-control amount validate[required,maxSize[19]]"
								cssErrorClass="form-control amount has-error validate[required,maxSize[19]]" />
							<form:errors path="limitAmount" cssClass="has-error" />
						</div>
					</div>
					
		            <div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label for="accountLocked" path="accountLocked" cssClass="control-label pull-right"
								cssErrorClass="control-label pull-right">
								<fmt:message key="coreUser.accountLocked" />
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
		                    <form:checkbox  path="accountLocked" id="accountLocked" cssErrorClass="has-error"/>
		                    <form:errors path="accountLocked" cssClass="has-error"/>
						</div>
					</div>
		            <div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label for="credentialsExpired" path="credentialsExpired" cssClass="control-label pull-right"
								cssErrorClass="control-label pull-right">
								<fmt:message key="user.credentialsExpired" />
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
		                    <form:checkbox  path="credentialsExpired" id="credentialsExpired" cssErrorClass="has-error"/>
		                    <form:errors path="credentialsExpired" cssClass="has-error"/>
						</div>
					</div>
		            	
					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label for="sessionTimeout" path="sessionTimeout" cssClass="control-label pull-right"
								cssErrorClass="control-label pull-right">
								<fmt:message key="coreUser.sessionTimeout" />
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<form:input path="sessionTimeout" id="sessionTimeout" cssClass="form-control"
								cssErrorClass="form-control has-error" />
							<form:errors path="sessionTimeout" cssClass="has-error" />
						</div>
					</div>
					
					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label for="departmentCode" path="departmentCode.id" cssClass="control-label pull-right"
								cssErrorClass="control-label pull-right">
								<fmt:message key="coreUser.departmentCode" />
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<form:select path="departmentCode.id" cssClass="form-control">
								<c:forEach items="${departments }" var="dept">
									<c:choose>
										<c:when test="${dept.id==coreUser.departmentCode.id }"><form:option value="${dept.id }" selected="true">[${dept.departmentCode }] - ${dept.description }</form:option></c:when>
										<c:otherwise><form:option value="${dept.id }">[${dept.departmentCode }] - ${dept.description }</form:option></c:otherwise>
									</c:choose>
									
								</c:forEach>
							</form:select>
							<form:errors path="departmentCode.id" cssClass="has-error" />
						</div>
					</div>
				</div>
				
	            <div class="box-footer">
					<div class="form-group">
						<div class="col-xs-12 col-md-9">
							<div class="pull-right">
					               <input type="submit" class="btn btn-primary" name="save" id="save"  value="<fmt:message key="button.edit"/>"/>
					               <input type="submit" class="btn btn-danger" name="cancel" id="cancel"  value="<fmt:message key="button.cancel"/>"/>
		            		</div>
		            	</div>
	            	</div>
	            </div>
	        </form:form>
        </div>
     </div>
</div>