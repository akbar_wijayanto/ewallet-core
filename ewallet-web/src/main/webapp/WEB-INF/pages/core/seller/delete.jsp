<%@ include file="/common/taglibs.jsp"%>
<head>
<title><fmt:message key="coreSeller.title" /></title>
<meta name="heading" content="<fmt:message key='coreSeller.heading'/>" />
<script type="text/javascript"
	src="<c:url value="/scripts/custom/core.coreseller.js"/>"></script>
</head>
<div class="row">
	<div class="col-xs-12">
		<div class="box">
			<div id="breadcumbTitle">
				<fmt:message key='coreSeller.message.view' />
			</div>
			<spring:url var="action" value='/core/seller/delete/${coreSeller.id}' />
			<form:form commandName="coreSeller" method="post" action="${action}"
				id="coreSeller" class="form-horizontal">
				<div class="box-body">
					<input type="hidden" name="from"
						value="<c:out value="${param.from}"/>" />
					<form:hidden path="sellerId" />

					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label for="sellerId" path="sellerId"
								cssClass="control-label pull-right"
								cssErrorClass="control-label pull-right">
								<fmt:message key="coreSeller.sellerId" />
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<%-- 							<form:input path="sellerId" id="sellerId" cssClass="form-control validate[required]" --%>
							<%-- 								cssErrorClass="form-control has-error validate[required]" readonly="true" /> --%>
							<img src='<c:out value="${image}"></c:out>' width="200"
								height="200">
						</div>
					</div>

					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label for="firstName" path="firstName"
								cssClass="control-label pull-right"
								cssErrorClass="control-label pull-right">
								<fmt:message key="coreSeller.firstName" />
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<form:input path="firstName" id="firstName"
								cssClass="form-control validate[required]"
								cssErrorClass="form-control has-error validate[required]"
								readonly="true" />
							<form:errors path="firstName" cssClass="has-error" />
						</div>
					</div>

					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label for="lastName" path="lastName"
								cssClass="control-label pull-right"
								cssErrorClass="control-label pull-right">
								<fmt:message key="coreSeller.lastName" />
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<form:input path="lastName" id="lastName"
								cssClass="form-control validate[required]"
								cssErrorClass="form-control has-error validate[required]"
								readonly="true" />
							<form:errors path="lastName" cssClass="has-error" />
						</div>
					</div>

					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label for="gender" path="gender"
								cssClass="control-label pull-right"
								cssErrorClass="control-label pull-right">
								<fmt:message key="coreSeller.gender" />
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<select id="gender" name="gender" class="form-control"
								readonly="true">
								<c:forEach items="${ genders }" var="genders">
									<option value="${ genders.value }">${ genders.name }</option>
								</c:forEach>
							</select>
						</div>
					</div>

					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label for="title" path="title"
								cssClass="control-label pull-right"
								cssErrorClass="control-label pull-right">
								<fmt:message key="coreSeller.sellerTitle" />
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<select id="title" name="title" class="form-control"
								readonly="true">
								<c:forEach items="${ titles }" var="titles">
									<option value="${ titles.statusValue }">${
										titles.statusType }</option>
								</c:forEach>
							</select>
						</div>
					</div>

					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label for="dateOfBirth" path="dateOfBirth"
								cssClass="control-label pull-right"
								cssErrorClass="control-label pull-right">
								<fmt:message key="coreSeller.dateOfBirth" />
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<div class="input-group">
								<form:input path="dateOfBirth" id="dateOfBirth"
									cssClass="datepicker form-control"
									cssErrorClass=" error datepicker form-control validate[required]"
									readonly="true" />
								<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
								<form:errors path="dateOfBirth" cssClass="error" />
							</div>
						</div>
					</div>

					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label for="idNumber" path="idNumber"
								cssClass="control-label pull-right "
								cssErrorClass="control-label pull-right">
								<fmt:message key="coreSeller.idNumber" />
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<form:input path="idNumber" id="idNumber"
								cssClass="form-control validate[required]"
								cssErrorClass="form-control has-error validate[required]"
								readonly="true" />
							<form:errors path="idNumber" cssClass="has-error" />
						</div>
					</div>

					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label for="phoneNumber" path="phoneNumber"
								cssClass="control-label pull-right"
								cssErrorClass="control-label pull-right">
								<fmt:message key="coreSeller.phoneNumber" />
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<form:input path="phoneNumber" id="phoneNumber"
								cssClass="form-control validate[required]"
								cssErrorClass="form-control has-error validate[required]"
								readonly="true" />
							<form:errors path="phoneNumber" cssClass="has-error" />
						</div>
					</div>

					<%-- <div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label for="pin" path="pin"
								cssClass="control-label pull-right"
								cssErrorClass="control-label pull-right">
								<fmt:message key="coreSeller.pin" />
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<form:input path="pin" id="pin" cssClass="form-control"
								cssErrorClass="form-control has-error validate[required]"
								readonly="true" />
							<form:errors path="pin" cssClass="has-error" />
						</div>
					</div> --%>

					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label for="streetAddress1" path="streetAddress1"
								cssClass="control-label pull-right"
								cssErrorClass="control-label pull-right">
								<fmt:message key="coreSeller.streetAddress1" />
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<form:input path="streetAddress1" id="streetAddress1"
								cssClass="form-control"
								cssErrorClass="form-control has-error validate[required]"
								readonly="true" />
							<form:errors path="streetAddress1" cssClass="has-error" />
						</div>
					</div>
					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label for="streetAddress2" path="streetAddress2"
								cssClass="control-label pull-right"
								cssErrorClass="control-label pull-right">
								<fmt:message key="coreSeller.streetAddress2" />
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<form:input path="streetAddress2" id="streetAddress2"
								cssClass="form-control"
								cssErrorClass="form-control has-error validate[required]"
								readonly="true" />
							<form:errors path="streetAddress2" cssClass="has-error" />
						</div>
					</div>
					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label for="houseNo" path="houseNo"
								cssClass="control-label pull-right"
								cssErrorClass="control-label pull-right">
								<fmt:message key="coreSeller.houseNo" />
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<form:input path="houseNo" id="houseNo" cssClass="form-control"
								cssErrorClass="form-control has-error validate[required]"
								readonly="true" />
							<form:errors path="houseNo" cssClass="has-error" />
						</div>
					</div>
					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label for="city" path="city"
								cssClass="control-label pull-right"
								cssErrorClass="control-label pull-right">
								<fmt:message key="coreSeller.city" />
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<form:input path="city" id="city" cssClass="form-control"
								cssErrorClass="form-control has-error validate[required]"
								readonly="true" />
							<form:errors path="city" cssClass="has-error" />
						</div>
					</div>
					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label for="postalCode" path="postalCode"
								cssClass="control-label pull-right"
								cssErrorClass="control-label pull-right">
								<fmt:message key="coreSeller.postalCode" />
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<form:input path="postalCode" id="postalCode"
								cssClass="form-control"
								cssErrorClass="form-control has-error validate[required]"
								readonly="true" />
							<form:errors path="postalCode" cssClass="has-error" />
						</div>
					</div>
					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label for="province" path="province"
								cssClass="control-label pull-right"
								cssErrorClass="control-label pull-right">
								<fmt:message key="coreSeller.province" />
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<form:input path="province" id="province" cssClass="form-control"
								cssErrorClass="form-control has-error validate[required]"
								readonly="true" />
							<form:errors path="province" cssClass="has-error" />
						</div>
					</div>
					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label for="country" path="country"
								cssClass="control-label pull-right"
								cssErrorClass="control-label pull-right">
								<fmt:message key="coreSeller.country" />
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<form:input path="country" id="country" cssClass="form-control"
								cssErrorClass="form-control has-error validate[required]"
								readonly="true" />
							<form:errors path="country" cssClass="has-error" />
						</div>
					</div>
					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label for="accountNumber" path="accountNumber"
								cssClass="control-label pull-right"
								cssErrorClass="control-label pull-right">
								<fmt:message key="coreSeller.accountNumber" />
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<form:input path="accountNumber" id="accountNumber"
								cssClass="form-control"
								cssErrorClass="form-control has-error validate[required]"
								readonly="true" />
							<form:errors path="accountNumber" cssClass="has-error" />
						</div>
					</div>

					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:hidden path="coreBank.code" />
							<label for="coreBank" class="control-label pull-right"> <fmt:message
									key="coreSeller.coreBank" />
							</label>
						</div>
						<div class="col-xs-8 col-md-6">
							<input id="coreBank" value="${coreSeller.coreBank.name}"
								class="form-control s_error" readonly="true" />
							<form:errors path="coreBank.code" cssClass="s_error" />
						</div>
					</div>
					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label for="descriptions" path="descriptions"
								cssClass="control-label pull-right"
								cssErrorClass="control-label pull-right">
								<fmt:message key="coreSeller.descriptions" />
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<form:input path="descriptions" id="descriptions"
								cssClass="form-control validate[required]"
								cssErrorClass="form-control has-error validate[required]"
								readonly="true" />
							<form:errors path="descriptions" cssClass="has-error" />
						</div>
					</div>

					<div class="box-footer">
						<div class="form-group">
							<div class="col-xs-12 col-md-9">
								<div class="pull-right">
									<input type="submit" class="btn btn-primary" name="save"
										id="save" value="<fmt:message key="button.delete"/>" /> <input
										type="submit" class="btn btn-danger" name="cancel" id="cancel"
										value="<fmt:message key="button.cancel"/>" />
								</div>
							</div>
						</div>
					</div>
				</div>
			</form:form>
		</div>
	</div>
</div>