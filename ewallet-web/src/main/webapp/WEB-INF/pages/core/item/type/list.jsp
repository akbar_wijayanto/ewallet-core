<%@ include file="/common/taglibs.jsp"%>
<head>
    <title><fmt:message key="coreItemType.title"/></title>
    <meta name="heading" content="<fmt:message key='coreItemType.heading'/>"/>
    <script type="text/javascript" src="<c:url value='/scripts/custom/core.coreitemtype.js' />"></script>
</head>
<div class="row">
	<div class="col-xs-12">	 
<%-- 		<security:authorize access="hasRole('CORE:USER:INPUT:*')"> --%>
	       	<a href="<c:url value='/core/item/type/add' />" class="btn btn-primary pull-right">
	       		<i class="fa fa-plus"></i>
	       		<fmt:message key="coreItemType.message.add" /></a>
<%-- 	    </security:authorize> --%>
	</div>
	<div class="col-xs-12">
	    <div class="box">
	       	<div id="breadcumbTitle"><fmt:message key='coreItemType.message.list'/></div>
	       	
	        <div class="box-body table-responsive">
	        	<table id="tableList" class="table table-bordered table-striped">
		            <thead>
			            <tr>
			                <th><fmt:message key='coreItemType.id'/></th>
			                <th><fmt:message key='coreItemType.name'/></th>
			                <th width="100px">&nbsp;</th>
			            </tr>
		            </thead>
		            <tbody>
		            	<c:forEach var="itemType" items="${coreItemTypes}">
			            	<tr>
			            		
				            	<td>${itemType.id}</td>
				            	<td>${itemType.name}</td>
				            	<td>
		                            <a href="<c:url value='/core/item/type/detail/${itemType.id}' />" title="View"><i class="fa fa-eye"></i></a>&nbsp;
		                            <a href="<c:url value='/core/item/type/edit/${itemType.id}' />" title="Edit"><i class="fa fa-edit"></i></a>&nbsp;
				            	</td>
				            </tr>
				    	</c:forEach>
		            </tbody>
		        </table>
			</div>
		</div>
    </div>
</div>