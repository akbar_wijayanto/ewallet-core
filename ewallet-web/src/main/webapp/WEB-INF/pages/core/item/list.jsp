<%@ include file="/common/taglibs.jsp"%>
<head>
    <title><fmt:message key="coreItem.title"/></title>
    <meta name="heading" content="<fmt:message key='coreItem.heading'/>"/>
    <script type="text/javascript" src="<c:url value='/scripts/custom/core.coreitem.js' />"></script>
</head>
<div class="row">
	<div class="col-xs-12">	 
<%-- 		<security:authorize access="hasRole('CORE:USER:INPUT:*')"> --%>
	       	<a href="<c:url value='/core/item/add' />" class="btn btn-primary pull-right">
	       		<i class="fa fa-plus"></i>
	       		<fmt:message key="coreItem.message.add" /></a>
<%-- 	    </security:authorize> --%>
	</div>
	<div class="col-xs-12">
	    <div class="box">
	       	<div id="breadcumbTitle"><fmt:message key='coreItem.message.list'/></div>
	       	
	        <div class="box-body table-responsive">
	        	<table id="tableList" class="table table-bordered table-striped">
		            <thead>
			            <tr>
			                <th><fmt:message key='coreItem.itemId'/></th>
			                <th><fmt:message key='coreItem.itemName'/></th>
			                <th><fmt:message key='coreItem.description'/></th>			                
			                <th><fmt:message key='coreItem.costPrice'/></th>
			                <th><fmt:message key='coreItem.sellingPrice'/></th>
			                <th><fmt:message key='coreItem.coreSeller'/></th>
			                <th width="100px">&nbsp;</th>
			            </tr>
		            </thead>
		            <tbody>
		            	<c:forEach var="item" items="${coreItems}">
			            	<tr>
				            	<td>${item.itemId}</td>
				            	<td>${item.itemName}</td>
				            	<td>${item.description}</td>
				            	<td><fmt:formatNumber pattern="${curr}" value="${item.costPrice}" /></td>
				            	<td><fmt:formatNumber pattern="${curr}" value="${item.sellingPrice}" /></td>
				            	<td>${item.coreSeller.firstName} ${item.coreSeller.lastName}</td>
				            	<td>
		                            <a href="<c:url value='/core/item/detail/${item.id}' />" title="View"><i class="fa fa-eye"></i></a>&nbsp;
		                            <a href="<c:url value='/core/item/edit/${item.id}' />" title="Edit"><i class="fa fa-edit"></i></a>&nbsp;
		                            <a href="<c:url value='/core/item/delete/${item.id}' />" title="Delete"><i class="fa fa-trash-o"></i></a>&nbsp;
				            	</td>
				            </tr>
				    	</c:forEach>
		            </tbody>
		        </table>
			</div>
		</div>
    </div>
</div>