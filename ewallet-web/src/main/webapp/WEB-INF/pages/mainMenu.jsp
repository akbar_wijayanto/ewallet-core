<%@ include file="/common/taglibs.jsp"%>

<head>
<title><fmt:message key="mainMenu.title" /></title>
<meta name="heading" content="<fmt:message key='mainMenu.heading'/>" />
</head>

<div class="box">

	<div class="box-header">
		<div id="breadcumbTitle" class="box-title">
			<fmt:message key='mainMenu.welcome' />
			${coreUser.firstName} ${coreUser.lastName}
		</div>
		<div class="row">
			<div class="col-md-6">
				<div class="box-body boxCustom">
					<h3 class="box-title">
						<fmt:message key='mainMenu.userInformation' />
					</h3>
					<table class="table table-striped">
						<tr>
							<td><fmt:message key='mainMenu.systemDate' /></td>
							<td><fmt:formatDate pattern="dd-MMM-yyyy" value="${htoday}" /></td>
						</tr>
						<tr>
							<td><fmt:message key='user.fullName' /></td>
							<td>${coreUser.firstName} ${coreUser.lastName}</td>
						</tr>
						<tr>
							<td><fmt:message key='user.activeRole' /></td>
							<td><c:forEach var="role" items="${coreUser.coreRoles}">
                           			${role.description}<br>
								</c:forEach></td>
						</tr>

					</table>
				</div>
				<!-- /.box-body -->

				<div class="box-body  boxCustom">
					<h3 class="box-title">
						<fmt:message key='mainMenu.userInformation' />
					</h3>
					<table class="table table-striped">

						<tr>
							<td><fmt:message key='user.lastLogon' /></td>
							<td><fmt:formatDate pattern="${format.format} HH:mm:ss"
									value="${coreUser.lastLogon}" /></td>
						</tr>
						<tr>
							<td><fmt:message key='user.limitAmount' /></td>
							<td><fmt:formatNumber pattern="${curr}"
									value="${coreUser.limitAmount}" /></td>
						</tr>
						<tr>
							<td><fmt:message key='user.preferredLocale' /></td>
							<td>${coreUser.preferredLocale}</td>
						</tr>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
