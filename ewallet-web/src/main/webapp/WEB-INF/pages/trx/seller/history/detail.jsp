<%@ include file="/common/taglibs.jsp"%>
<head>
    <title><fmt:message key="coreSeller.title"/></title>
    <meta name="heading" content="<fmt:message key='sellerHistory.heading'/>"/>
    <script type="text/javascript" src="<c:url value="/scripts/custom/trx.sellerpayment.js"/>"></script>
</head>
<div class="row">
	<div class="col-xs-12">
		<div class="box">
<!-- 			<div id="breadcumbTitle"> -->
<%-- 				<fmt:message key='sellerHistory.message.view' /> --%>
<!-- 			</div> -->
	        <spring:url var="action" value='/trx/seller/payment/history/detail/${trxSellerPayment.coreSeller.sellerId}/${trxSellerPayment.paymentDate}' />
	        <form:form commandName="trxSellerPayment" method="post" action="${action}"  id="trxSellerPayment" class="form-horizontal">
	        	<div class="box-body">
		            <input type="hidden" name="from" value="<c:out value="${param.from}"/>"/>
					
					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label for="sellerId" path="coreSeller.sellerId" cssClass="control-label pull-right"
								cssErrorClass="control-label pull-right">
								<fmt:message key="coreSeller.sellerId" />
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<form:input path="coreSeller.sellerId" id="sellerId" cssClass="form-control validate[required]"
								cssErrorClass="form-control has-error validate[required]" readonly="true" />
							<form:errors path="coreSeller.sellerId" cssClass="has-error" />
						</div>
					</div>
					
					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label for="firstName" path="coreSeller.firstName" cssClass="control-label pull-right"
								cssErrorClass="control-label pull-right">
								<fmt:message key="coreSeller.name" />
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<form:input path="coreSeller.firstName" id="firstName" cssClass="form-control validate[required]"
								cssErrorClass="form-control has-error validate[required]" readonly="true" />
							<form:errors path="coreSeller.firstName" cssClass="has-error" />
						</div>
					</div>
					
					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label for="grandTotal" path="grandTotal" cssClass="control-label pull-right"
								cssErrorClass="control-label pull-right">
								<fmt:message key="trxSellerPayment.grandTotal" />
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<form:input path="grandTotal" id="grandTotal" cssClass="form-control validate[required]"
								cssErrorClass="form-control has-error validate[required]" readonly="true" />
							<form:errors path="grandTotal" cssClass="has-error" />
						</div>
					</div>
					
					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label for="totalTransaction" path="totalTransaction" cssClass="control-label pull-right "
								cssErrorClass="control-label pull-right">
								<fmt:message key="trxSellerPayment.totalTransaction" />
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<form:input path="totalTransaction" id="totalTransaction" cssClass="form-control validate[required]"
								cssErrorClass="form-control has-error validate[required]" readonly="true" />
							<form:errors path="totalTransaction" cssClass="has-error" />
						</div>
					</div>
					
					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label for="percentage" path="percentage" cssClass="control-label pull-right"
								cssErrorClass="control-label pull-right">
								<fmt:message key="trxSellerPayment.percentage" />
							</form:label>
						</div>
						<div class="col-xs-1 col-md-3">
							<form:input path="percentage" id="percentage" cssClass="form-control validate[required]"
								cssErrorClass="form-control has-error validate[required]" readonly="true"/>
							<form:errors path="percentage" cssClass="has-error" />
						</div>
					</div>
					
					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<label class="control-label pull-right"> <fmt:message
									key="trxSet.serviceCash" />
							</label>
						</div>
						<div class="col-xs-8 col-md-6">
							<input type="text" name="serviceCash" id="serviceCash"
									value="${serviceCash}" class="form-control validate[required]" readonly="true"/>
						</div>
					</div>
					
					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label for="charge" path="charge" cssClass="control-label pull-right"
								cssErrorClass="control-label pull-right">
								<fmt:message key="trxSellerPayment.charge" />
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<form:input path="charge" id="charge" cssClass="form-control"
								cssErrorClass="form-control has-error validate[required]" readonly="true" />
							<form:errors path="charge" cssClass="has-error" />
						</div>
					</div>
					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label for="totalAmount" path="totalAmount" cssClass="control-label pull-right"
								cssErrorClass="control-label pull-right">
								<fmt:message key="trxSellerPayment.totalAmount" />
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<form:input path="totalAmount" id="totalAmount" cssClass="form-control"
								cssErrorClass="form-control has-error validate[required]" readonly="true" />
							<form:errors path="totalAmount" cssClass="has-error" />
						</div>
					</div>
					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label for="accountNumber" path="coreSeller.accountNumber" cssClass="control-label pull-right"
								cssErrorClass="control-label pull-right">
								<fmt:message key="trxSellerPayment.accountNumber" />
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<form:input path="coreSeller.accountNumber" id="accountNumber" cssClass="form-control"
								cssErrorClass="form-control has-error validate[required]" readonly="true" />
							<form:errors path="coreSeller.accountNumber" cssClass="has-error" />
						</div>
					</div>
		            <div class="box-footer">
						<div class="form-group">
							<div class="col-xs-12 col-md-9">
								<div class="pull-right">
					                <input type="submit" class="btn btn-danger" name="cancel" id="cancel"  value="<fmt:message key="button.cancel"/>"/>
			            		</div>
			            	</div>
		            	</div>
		            </div>
	            </div>
	        </form:form>
	        <div class="col-xs-12">
			    <div class="box">
			       	<div id="breadcumbTitle"><fmt:message key='sellerHistory.message.list'/></div>
			       	
			        <div class="box-body table-responsive">
			        	<table id="tableList" class="table table-bordered table-striped" >
				            <thead>
					            <tr>
					            	<th><fmt:message key='trxOrder.orderId' /></th>
									<th><fmt:message key='trxOrder.status' /></th>
									<th><fmt:message key='trxOrder.orderDate' /></th>
									<th><fmt:message key='trxOrder.itemName' /></th>
									<th><fmt:message key='trxOrder.totHapok' /></th>
									<th><fmt:message key='trxOrder.quantity' /></th>
					            </tr>
				            </thead>
				            <tbody id="trxSellerPayHistoryBody">
				            	<c:forEach var="order" items="${orders}">
				            		<c:forEach var="detail" items="${order.trxOrderItemDetails}" varStatus="status">
						            	<tr>
							            	<td>${order.orderId}</td>
							            	<td>${order.status}</td>
							            	<td>${order.orderDate}</td>
							            	<td>${detail.pk.coreItem.itemName}</td>
							            	<td>${detail.pk.coreItem.costPrice}</td>
							            	<td>${detail.quantity}</td>
							            </tr>
						            </c:forEach>
						    	</c:forEach>
				            </tbody>
				        </table>
					</div>
					<div id="loading" class="overlay process-loading hidden">
					  <i class="fa fa-refresh fa-spin"></i>
					</div>
				</div>
			</div>
			<div class="box-footer">
				<div class="form-group">
					<div class="col-xs-12 col-md-12">
						<div class="pull-right">
							<input type="button" class="btn btn-primary"
								name="downloadhistorypdf" id="downloadhistorypdf"
								value="<fmt:message key="button.downloadPdf"/>" /> 
							<input type="button" class="btn btn-primary"
								name="downloadhistoryxls" id="downloadhistoryxls"
								value="<fmt:message key="button.downloadExcel"/>" />
						</div>
					</div>
				</div>
			</div>
     	</div>
    </div>
</div>