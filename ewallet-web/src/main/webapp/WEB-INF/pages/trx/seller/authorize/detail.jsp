<%@ include file="/common/taglibs.jsp"%>
<head>
    <title><fmt:message key="coreSeller.title"/></title>
    <meta name="heading" content="<fmt:message key='settlementReportDto.heading'/>"/>
    <script type="text/javascript" src="<c:url value="/scripts/custom/core.coreseller.js"/>"></script>
</head>
<div class="row">
	<div class="col-xs-12">
		<div class="box">
			<div id="breadcumbTitle">
				<fmt:message key='settlementReportDto.message.view' />
			</div>
	        <spring:url var="action" value='/trx/seller/payment/auth/detail/${settlementDto.paymentId}' />
	        <form:form commandName="settlementDto" method="post" action="${action}"  id="settlementDto" class="form-horizontal">
	        	<div class="box-body">
		            <input type="hidden" name="from" value="<c:out value="${param.from}"/>"/>
					
					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label for="coreSeller.sellerId" path="coreSeller.sellerId" cssClass="control-label pull-right"
								cssErrorClass="control-label pull-right">
								<fmt:message key="settlementReportDto.sellerId" />
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<form:input path="coreSeller.sellerId" id="coreSeller.sellerId" cssClass="form-control validate[required]"
								cssErrorClass="form-control has-error validate[required]" readonly="true" />
							<form:errors path="coreSeller.sellerId" cssClass="has-error" />
						</div>
					</div>
					
					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label for="coreSeller.firstName" path="coreSeller.firstName" cssClass="control-label pull-right"
								cssErrorClass="control-label pull-right">
								<fmt:message key="settlementReportDto.sellerName" />
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<form:input path="coreSeller.firstName" id="coreSeller.firstName" cssClass="form-control validate[required]"
								cssErrorClass="form-control has-error validate[required]" readonly="true" />
							<form:errors path="coreSeller.firstName" cssClass="has-error" />
						</div>
					</div>
					
					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label for="grandTotal" path="grandTotal" cssClass="control-label pull-right"
								cssErrorClass="control-label pull-right">
								<fmt:message key="settlementReportDto.grandTotal" />
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<form:input path="grandTotal" id="grandTotal" cssClass="form-control validate[required]"
								cssErrorClass="form-control has-error validate[required]" readonly="true" />
							<form:errors path="grandTotal" cssClass="has-error" />
						</div>
					</div>
					
					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label for="totalTransaction" path="totalTransaction" cssClass="control-label pull-right "
								cssErrorClass="control-label pull-right">
								<fmt:message key="settlementReportDto.totalTransaction" />
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<form:input path="totalTransaction" id="totalTransaction" cssClass="form-control validate[required]"
								cssErrorClass="form-control has-error validate[required]" readonly="true" />
							<form:errors path="totalTransaction" cssClass="has-error" />
						</div>
					</div>
					
					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label for="percentage" path="percentage" cssClass="control-label pull-right"
								cssErrorClass="control-label pull-right">
								<fmt:message key="settlementReportDto.percentage" />
							</form:label>
						</div>
						<div class="col-xs-1 col-md-3">
							<form:input path="percentage" id="percentage" cssClass="form-control validate[required]"
								cssErrorClass="form-control has-error validate[required]" readonly="true"/>
							<form:errors path="percentage" cssClass="has-error" />
						</div>
					</div>
					
					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label for="charge" path="charge" cssClass="control-label pull-right"
								cssErrorClass="control-label pull-right">
								<fmt:message key="settlementReportDto.charge" />
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<form:input path="charge" id="charge" cssClass="form-control"
								cssErrorClass="form-control has-error validate[required]" readonly="true" />
							<form:errors path="charge" cssClass="has-error" />
						</div>
					</div>
					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label for="totalAmount" path="totalAmount" cssClass="control-label pull-right"
								cssErrorClass="control-label pull-right">
								<fmt:message key="settlementReportDto.totalAmountPaid" />
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<form:input path="totalAmount" id="totalAmount" cssClass="form-control"
								cssErrorClass="form-control has-error validate[required]" readonly="true" />
							<form:errors path="totalAmount" cssClass="has-error" />
						</div>
					</div>
					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label for="coreSeller.accountNumber" path="coreSeller.accountNumber" cssClass="control-label pull-right"
								cssErrorClass="control-label pull-right">
								<fmt:message key="settlementReportDto.accountNumber" />
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<form:input path="coreSeller.accountNumber" id="coreSeller.accountNumber" cssClass="form-control"
								cssErrorClass="form-control has-error validate[required]" readonly="true" />
							<form:errors path="coreSeller.accountNumber" cssClass="has-error" />
						</div>
					</div>
					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label for="status" path="status" cssClass="control-label pull-right"
								cssErrorClass="control-label pull-right">
								<fmt:message key="settlementReportDto.status" />
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<select id="status" name="status" class="form-control">
								<c:forEach items="${ status }" var="status">
									<option value="${ status.code }">${ status.name }</option>
								</c:forEach>
							</select>
						</div>
					</div>
					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label for="createdBy" path="createdBy" cssClass="control-label pull-right"
								cssErrorClass="control-label pull-right">
								<fmt:message key="coreSeller.authorize" />
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<form:input type="password" path="createdBy" id="createdBy" cssClass="form-control"
								cssErrorClass="form-control has-error validate[required]" />
							<form:errors path="createdBy" cssClass="has-error" />
						</div>
					</div>
		            <div class="box-footer">
						<div class="form-group">
							<div class="col-xs-12 col-md-9">
								<div class="pull-right">
					                <input type="submit" class="btn btn-primary" name="save" id="save"  value="<fmt:message key="button.add"/>"/>
					                <input type="submit" class="btn btn-danger" name="cancel" id="cancel"  value="<fmt:message key="button.cancel"/>"/>
			            		</div>
			            	</div>
		            	</div>
		            </div>
	            </div>
	        </form:form>
     	</div>
    </div>
</div>