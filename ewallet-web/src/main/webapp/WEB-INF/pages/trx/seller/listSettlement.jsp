<%@ include file="/common/taglibs.jsp"%>
<head>
<title><fmt:message key="trxRefund.title" /></title>
<meta name="heading" content="<fmt:message key='trxRefund.heading'/>" />
<%-- <script type="text/javascript" src="<c:url value='/scripts/custom/trx.refund.js' />"></script> --%>
<script type="text/javascript"
	src="<c:url value='/scripts/custom/trx.sellerpayment.js' />"></script>
</head>
<div class="row">
	<div class="col-xs-12">
		<div class="box">
			<div id="breadcumbTitle">
				<fmt:message key='trxSellPay.message.list' />
			</div>

			<spring:url var="action" value='/trx/seller/payment/infoSettle' />
			<form:form commandName="trxSellPay" name="trxSellPay"
				action="${action}" id="trxSellPay" class="form-horizontal">
				<div class="box-body">
					<input type="hidden" name="from"
						value="<c:out value="${param.from}"/>" />
					<div class="form-group">
						<div class="col-xs-2 col-md-2">
							<label for="startDate" class="control-label pull-right"
								class="control-label pull-right"> <fmt:message
									key="trxOrder.startDate" />
							</label>
						</div>
						<div class="col-xs-2 col-md-2">
							<input id="startDate" name="startDate" value="${startDate}"
								class="datepicker form-control validate[required]"
								class="datepicker form-control has-error validate[required]"
								value="${startDate}" />
						</div>
						<div class="col-xs-1 col-md-1">
							<label for="endDate" class="control-label pull-right"
								class="control-label pull-right"> <fmt:message
									key="trxOrder.endDate" />
							</label>
						</div>
						<div class="col-xs-2 col-md-2">
							<input id="endDate" name="endDate" value="${endDate}"
								class="datepicker form-control validate[required]"
								class="datepicker form-control has-error validate[required]"
								value="${endDate}" />
						</div>

					<div class="box-footer">
						<div class="form-group">
							<div class="col-xs-12 col-md-12">
								<div class="pull-right">
									<input type="submit" class="btn btn-primary" name="saveSettle"
										id="saveSettle" value="<fmt:message key="button.submit"/>" />
									<input type="button" class="btn btn-primary"
										name="trxSellerSettle" id="trxSellerSettle"
										value="<fmt:message key="button.settle"/>" />
								</div>
							</div>
						</div>
					</div>
			</form:form>

			<div class="box-body table-responsive">
	        	<table id="tableList" class="table table-bordered table-striped">
		            <thead>
			            <tr>
			            	<th><fmt:message key='trxSellerPayment.sellerName'/></th>
			                <th><fmt:message key='trxSellerPayment.totalTransaction'/></th>
			                <th><fmt:message key='trxSellerPayment.grandTotal'/></th>
			                <th><fmt:message key='trxSellerPayment.percentage'/></th>
			                <th><fmt:message key='trxSellerPayment.charge'/></th>
			                <th><fmt:message key='trxSellerPayment.totalAmount'/></th>
			            	<th><fmt:message key='trxSellerPayment.accountNumber'/></th>
			            </tr>
		            </thead>
		            <tbody>
		            	<c:forEach var="settlement" items="${settlementReportDtos}">
			            	<tr>
			            		<td>${settlement.sellerName}</td>
				            	<td><fmt:formatNumber pattern="${curr}" value="${settlement.totalTransaction}" /></td>
				            	<td><fmt:formatNumber pattern="${curr}" value="${settlement.grandTotal}" /></td>
				            	<td>${settlement.percentage}</td>
				            	<td><fmt:formatNumber pattern="${curr}" value="${settlement.charge}" /></td>
				            	<td><fmt:formatNumber pattern="${curr}" value="${settlement.totalAmountPaid}" /></td>
				            	<td>${settlement.accountNumber}</td>
				            </tr>
				    	</c:forEach>
		            </tbody>
		        </table>
			</div>
		</div>
	</div>
</div>