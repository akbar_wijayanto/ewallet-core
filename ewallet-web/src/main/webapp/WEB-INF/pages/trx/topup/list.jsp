<%@ include file="/common/taglibs.jsp"%>
<head>
    <title><fmt:message key="trxTopup.title"/></title>
    <meta name="heading" content="<fmt:message key='trxTopup.heading'/>"/>
    <script type="text/javascript" src="<c:url value='/scripts/custom/trx.topup.js' />"></script>
</head>
<div class="row">
<!-- 	<div class="col-xs-12">	  -->
<%-- 		<security:authorize access="hasRole('CORE:USER:INPUT:*')"> --%>
<%-- 	       	<a href="<c:url value='/core/seller/add' />" class="btn btn-primary pull-right"> --%>
<!-- 	       		<i class="fa fa-plus"></i> -->
<%-- 	       		<fmt:message key="trxTopup.message.add" /></a> --%>
<%-- 	    </security:authorize> --%>
<!-- 	</div> -->
	<div class="col-xs-12">
	    <div class="box">
	       	<div id="breadcumbTitle"><fmt:message key='trxTopup.message.list'/></div>
	       	
	        <div class="box-body table-responsive">
	        	<table id="tableList" class="table table-bordered table-striped">
		            <thead>
			            <tr>
			                <th><fmt:message key='trxTopup.topupId'/></th>
			                <th><fmt:message key='trxTopup.amount'/></th>
			                <th><fmt:message key='trxTopup.topupDate'/></th>
			                <th><fmt:message key='trxTopup.coreUser'/></th>
			                <th width="280px">&nbsp;</th>
			            </tr>
		            </thead>
		            <tbody>
		            	<c:forEach var="topup" items="${trxTopups}">
			            	<tr>
			            		
				            	<td>${topup.topupId}</td>
				            	<td>${topup.amount}</td>
				            	<td>${topup.topupDate}</td>
				            	<td>${topup.coreUser.firstName} ${topup.coreUser.lastName}</td>
				            	<td>
		                            <a href="<c:url value='/trx/topup/detail/${topup.id}' />" title="View"><i class="fa fa-eye"></i></a>&nbsp;
				            	</td>
				            </tr>
				    	</c:forEach>
		            </tbody>
		        </table>
			</div>
		</div>
    </div>
</div>