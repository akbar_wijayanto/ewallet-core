<%@ include file="/common/taglibs.jsp"%>
<head>
    <title><fmt:message key="trxTopup.title"/></title>
    <meta name="heading" content="<fmt:message key='trxTopup.heading'/>"/>
    <script type="text/javascript" src="<c:url value="/scripts/custom/core.trxtopup.js"/>"></script>
</head>
<div class="row">
	<div class="col-xs-12">
		<div class="box">
			<div id="breadcumbTitle">
				<fmt:message key='trxTopup.message.add' />
			</div>
	        <spring:url var="action" value='/trx/topup/add' />
	        <form:form commandName="trxTopup" method="post" action="${action}"  id="trxTopup" class="form-horizontal">
	        	<div class="box-body">
		            <input type="hidden" name="from" value="<c:out value="${param.from}"/>"/>
					
					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label for="itemId" path="itemId" cssClass="control-label pull-right"
								cssErrorClass="control-label pull-right">
								<fmt:message key="coreItem.itemId" />
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<form:input path="itemId" id="itemId" cssClass="form-control validate[required]"
								cssErrorClass="form-control has-error validate[required]" />
							<form:errors path="itemId" cssClass="has-error" />
						</div>
					</div>
					
					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label for="itemName" path="itemName" cssClass="control-label pull-right"
								cssErrorClass="control-label pull-right">
								<fmt:message key="coreItem.itemName" />
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<form:input path="itemName" id="itemName" cssClass="form-control validate[required]"
								cssErrorClass="form-control has-error validate[required]" />
							<form:errors path="itemName" cssClass="has-error" />
						</div>
					</div>
					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label for="coreItemType" path="coreItemType.id" cssClass="control-label pull-right"
								cssErrorClass="control-label pull-right">
								<fmt:message key="coreItem.coreItemType" />
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<form:select id="coreItemType" name="coreItemType" path="coreItemType.id" class="form-control">
								<c:forEach items="${ coreItemTypes }" var="coreItemTypes">
									<option value="${ coreItemTypes.id }">${ coreItemTypes.name }</option>
								</c:forEach>
							</form:select>
							<form:errors path="coreItemType.id" cssClass="error" />
						</div>
					</div>	
						
					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label for="costPrice" path="costPrice" cssClass="control-label pull-right "
								cssErrorClass="control-label pull-right">
								<fmt:message key="coreItem.costPrice" />
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<form:input path="costPrice" id="costPrice" cssClass="form-control  validate[required]"
								cssErrorClass="form-control has-error validate[required]" />
							<form:errors path="costPrice" cssClass="has-error" />
						</div>
					</div>
					
					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label for="sellingPrice" path="sellingPrice" cssClass="control-label pull-right"
								cssErrorClass="control-label pull-right">
								<fmt:message key="coreItem.sellingPrice" />
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<form:input path="sellingPrice" id="sellingPrice" cssClass="form-control validate[required]"
								cssErrorClass="form-control has-error validate[required]" />
							<form:errors path="sellingPrice" cssClass="has-error" />
						</div>
					</div>
					
					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label for="entryDate" path="entryDate" cssClass="control-label pull-right"
								cssErrorClass="control-label pull-right">
								<fmt:message key="coreItem.entryDate" />
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<div class="input-group">
								<form:input path="entryDate" id="entryDate" cssClass="datepicker form-control"
										cssErrorClass=" error datepicker form-control validate[required]" />
								<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
								<form:errors path="entryDate" cssClass="error" />
							</div>
						</div>
					</div>
					
					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label for="expiredDate" path="expiredDate" cssClass="control-label pull-right"
								cssErrorClass="control-label pull-right">
								<fmt:message key="coreItem.expiredDate" />
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<div class="input-group">
								<form:input path="expiredDate" id="expiredDate" cssClass="datepicker form-control"
										cssErrorClass=" error datepicker form-control validate[required]" />
								<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
								<form:errors path="expiredDate" cssClass="error" />
							</div>
						</div>
					</div>
					
					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label for="description" path="description" cssClass="control-label pull-right"
								cssErrorClass="control-label pull-right">
								<fmt:message key="coreItem.description" />
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<form:input path="description" id="description" cssClass="form-control"
								cssErrorClass="form-control has-error validate[required]" />
							<form:errors path="description" cssClass="has-error" />
						</div>
					</div>
					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label path="coreSeller.id" for="coreSeller"
								cssClass="control-label pull-right"
								cssErrorClass="control-label pull-right">
								<fmt:message key="coreItem.coreSeller" />
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<div class="input-group">
								<form:hidden path="coreSeller.id" id="coreSeller"
									cssClass="form-control"
									cssErrorClass=" form-control has-error validate[required]" />
								<input type="text" class="form-control" readonly id="coreSeller_">
								<form:errors path="coreSeller.id" cssClass="error" />

								<span class="input-group-btn">
									<button style="height: 30px;" class="btn btn-info btn-flat"
										type="button" id="btnCoreSeller" data-toggle="modal"
										data-target="#coreSellerList">
										<i class="fa fa-check"></i>
									</button>
								</span>
							</div>
						</div>
					</div>
		            <div class="box-footer">
						<div class="form-group">
							<div class="col-xs-12 col-md-9">
								<div class="pull-right">
					                <input type="submit" class="btn btn-primary" name="save" id="save"  value="<fmt:message key="button.add"/>"/>
					                <input type="submit" class="btn btn-danger" name="cancel" id="cancel"  value="<fmt:message key="button.cancel"/>"/>
			            		</div>
			            	</div>
		            	</div>
		            </div>
	            </div>
	        </form:form>
     	</div>
    </div>
</div>
<div class="modal fade" id="coreSellerList" tabindex="-1" role="dialog"
	aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title" id="myModalLabel">
					<fmt:message key="coreSeller.message.list" />
				</h4>
			</div>
			<div class="modal-body">
				<table id="coreSellerTable" class="table dataTable table-bordered table-striped">
                <thead>
                    <tr>
                        <th><fmt:message key='coreSeller.sellerId'/></th>
                        <th><fmt:message key='coreSeller.name'/></th>
                        <th><fmt:message key='coreSeller.streetAddress1'/></th>
                        <th width="80px">&nbsp;</th>
                       
                    </tr>
                </thead>
                <tbody>
                    <c:forEach var="coreSeller" items="${coreSellers}">
                        <tr>
                            <td>${coreSeller.sellerId}</td>
                            <td>${coreSeller.firstName} ${coreSeller.lastName}</td>
                            <td>${coreSeller.streetAddress1}</td>
                            <td>
                            	<a onclick="doCopy('coreSeller','coreSeller_','coreSellerList','${coreSeller.id}','${coreSeller.firstName} ${coreSeller.lastName}');" title="check" class="btn btn-info btn-flat"><i class="fa fa-check"></i></a>
                            </td>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>