<%@ include file="/common/taglibs.jsp" %>
<a href="javascript:void(0)" class="sidebar_switch on_switch ttip_r" title="Hide Sidebar">Sidebar switch</a>
<div class="sidebar">
    <div class="antiScroll">
        <div class="antiscroll-inner">
            <div class="antiscroll-content">

                <div class="sidebar_inner">
                    <div id="side_accordion" class="accordion">

                        <div class="accordion-group">
                            <div class="accordion-heading">
                                <a href="#collapseOne" data-parent="#side_accordion" data-toggle="collapse" class="accordion-toggle">
                                    <i class="icon-folder-close"></i> Content
                                </a>
                            </div>
                            <div class="accordion-body collapse" id="collapseOne">
                                <div class="accordion-inner">
                                    <ul class="nav nav-list">
                                        <li><a href="javascript:void(0)">Articles</a></li>
                                        <li><a href="javascript:void(0)">News</a></li>
                                        <li><a href="javascript:void(0)">Newsletters</a></li>
                                        <li><a href="javascript:void(0)">Comments</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="accordion-group">
                            <div class="accordion-heading">
                                <a href="#collapseTwo" data-parent="#side_accordion" data-toggle="collapse" class="accordion-toggle">
                                    <i class="icon-th"></i> Modules
                                </a>
                            </div>
                            <div class="accordion-body collapse" id="collapseTwo">
                                <div class="accordion-inner">
                                    <ul class="nav nav-list">
                                        <li><a href="javascript:void(0)">Content blocks</a></li>
                                        <li><a href="javascript:void(0)">Tags</a></li>
                                        <li><a href="javascript:void(0)">Blog</a></li>
                                        <li><a href="javascript:void(0)">FAQ</a></li>
                                        <li><a href="javascript:void(0)">Formbuilder</a></li>
                                        <li><a href="javascript:void(0)">Location</a></li>
                                        <li><a href="javascript:void(0)">Profiles</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="accordion-group">
                            <div class="accordion-heading">
                                <a href="#collapseThree" data-parent="#side_accordion" data-toggle="collapse" class="accordion-toggle">
                            <i class="icon-user"></i></a></div>
                        </div>
                        <div class="accordion-group">
                          <div class="accordion-body collapse in" id="collapseFour">
                            <div class="accordion-inner"> </div>
                            </div>
                        </div>
                        <div class="accordion-group">
                          <div class="accordion-body collapse" id="collapseLong"></div>
                        </div>
                    </div>

                    <div class="push"></div>
                </div>
            </div>
        </div>
    </div>
</div>