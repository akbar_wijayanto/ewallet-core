<%@ include file="/common/taglibs.jsp"%>
<header class="header">
<div class="logo">
	<a href="<c:url value='/mainMenu' />" class="logo">
    	<!-- Add the class icon to your logo image or logo icon to add the margining -->
	    <img src="<c:url value="/images/logokos.png"/>">
	</a>
</div>

<!-- Header Navbar: style can be found in header.less -->
	<nav class="navbar navbar-static-top" role="navigation">
	    <!-- Sidebar toggle button-->
	    <a href="#" class="fa fa-bars iconNav" data-toggle="offcanvas" role="button">
	        <span class="sr-only">Toggle navigation</span>
	       <!--  <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span> -->
	    </a>
	    
	    <div class="navbar-right">
	        <ul class="nav navbar-nav">
	        	<li class="dropdown">
					<a href="#" class="dropdown-toggle nav_condensed" data-toggle="dropdown" style="text-transform:capitalize;">  ${fn:substring(coreUser.preferredLocale,0,2)}</a>
					<ul class="dropdown-menu">
						<li><a href="<c:url value='/core/user/changelocale/en_US' />"><i class="flag-en"></i> En</a></li>
						<li><a href="<c:url value='/core/user/changelocale/id_ID' />"><i class="flag-id"></i> Id</a></li>
					</ul>
				</li>
<!-- 				<li> -->
<!-- 					<a href="#"> -->
<!-- 						 <i class="glyphicon glyphicon-globe"></i> -->
<%-- 						 <span>${hbranch}</span> --%>
<!-- 					</a> -->
<!-- 				</li> -->
				<li>
					<a href="#"><fmt:formatDate pattern="dd-MMM-yy" value="${htoday}" /></a>
				</li>
				<li class="dropdown messages-menu">
					<a id="notificationButton" href="#"  data-toggle="dropdown">
						<span id="usernameLogin">${husername}</span> 
						<i class="fa fa-envelope"></i>
						<b style="display:inline-block"></b>
						<span id="notificationNumber" class="label label-success"></span>
					</a>
					<ul id="notifBody" class="dropdown-menu">
                       
                       <li class="footer"><a href="<c:url value='/trx/loancustomeroffer' />">See All Messages</a></li>
                    </ul>
				</li>
	            <!-- User Account: style can be found in dropdown.less -->
	            <li class="dropdown user">
	                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
	                    <i class="glyphicon glyphicon-user"></i>
	                    <span>${huser} <i class="caret"></i></span>
	                </a>
	                <ul class="dropdown-menu">
                        <li><a href="<c:url value='/core/user/changerole' />"><i class="glyphicon glyphicon-cog"></i><fmt:message key="user.myrole" /></a></li>
						<li><a href="<c:url value='/core/user/changebranch' />"><i class="glyphicon glyphicon-globe"></i><fmt:message key="user.mybranch" /></a></li>
						<li><a href="<c:url value='/core/user' />"><i class="glyphicon glyphicon-user"></i><fmt:message key="user.myprofile" /></a></li>
						<li class="divider"></li>
						<li><a href="<c:url value='/logout' />"><i class="glyphicon glyphicon-off"></i><fmt:message key="user.logout" /></a></li>
                    </ul>	                
	            </li>
	        </ul>
	    </div>
	</nav>
</header>