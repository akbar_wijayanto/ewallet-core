package com.ewallet.webapp.controller;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.StringUtils;
//import org.aspectj.apache.bcel.classfile.Constant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ewallet.Constants;
import com.ewallet.enumeration.SettleStatusEnum;
import com.ewallet.exception.RetailException;
import com.ewallet.persistence.model.CoreRole;
import com.ewallet.persistence.model.CoreSeller;
import com.ewallet.persistence.model.CoreUser;
import com.ewallet.persistence.model.TrxOrder;
import com.ewallet.persistence.model.TrxOrderItemDetail;
import com.ewallet.persistence.model.TrxSellerPayment;
import com.ewallet.report.dto.CoreSellerSettlementReportDto;
import com.ewallet.report.dto.HistoryDownloadBean;
import com.ewallet.report.dto.ReportSettlementDetail;
import com.ewallet.report.service.CoreSellerReportService;
import com.ewallet.report.service.GenerateReportService;
import com.ewallet.report.util.ClientReportGenerator;
import com.ewallet.service.CoreSellerManager;
import com.ewallet.service.CoreSystemManager;
import com.ewallet.service.CoreUserManager;
import com.ewallet.service.TrxOrderManager;
import com.ewallet.service.TrxSellerPaymentManager;
import com.ewallet.util.DateUtil;
import com.ewallet.webapp.util.FileDownloader;

/**
 * @author akbar.wijayanto
 * Date Dec 31, 2015 3:18:05 PM
 */
@Controller
@RequestMapping("/trx/seller/payment")
public class TrxSellerPaymentController extends BaseFormController {

	@Autowired
	private TrxOrderManager trxOrderManager;
	
	@Autowired
	private TrxSellerPaymentManager trxSellerPaymentManager;
	
	@Autowired
	private CoreSystemManager coreSystemManager;
	
	@Autowired
	private CoreSellerManager coreSellerManager;
	
	@Autowired
	private CoreUserManager coreUserManager;
	
	@Autowired
	private CoreSellerReportService coreSellerReportService;
	
	@Autowired
	private GenerateReportService generateReportService;
	
	private List<TrxOrder> orders = new ArrayList<TrxOrder>();
	
	private TrxSellerPayment trxSellerPayment = new TrxSellerPayment();
	
	private List<CoreSellerSettlementReportDto> listsettleinfo = new ArrayList<CoreSellerSettlementReportDto>();
	
	public TrxSellerPaymentController() {
		setCancelView("redirect:/trx/seller/payment");
		setSuccessView("redirect:/trx/seller/payment");
	}

	@RequestMapping(value = "")
	public String view(Model model) throws Exception {
		model.addAttribute("settlementReportDtos", coreSellerReportService.getAllSellerSettlementPayment());
		return "/trx/seller/list";
	}
	
	@RequestMapping(value = "/auth")
	public String viewAuth(Model model) throws Exception {
		List<TrxSellerPayment> bean = trxSellerPaymentManager.getAllSellerPayment();
		System.out.println("Size Auth -> " +bean.size());
		model.addAttribute("settlementDtos", bean);
		return "/trx/seller/authorize/list";
	}

	@RequestMapping(value = "/detail/{sellerId}", method = RequestMethod.GET)
	public String detailDisplay(@PathVariable("sellerId") String sellerId, Model model)
			throws Exception {
		List<CoreSellerSettlementReportDto> dtos = coreSellerReportService.getSellerSettlementReportBySellerId(sellerId);
		CoreSellerSettlementReportDto settlementReportDto = dtos.get(0);
		model.addAttribute("settlementReportDto", settlementReportDto);
		return "trx/seller/detail";
	}

	@RequestMapping(value = "/detail/{sellerId}", method = RequestMethod.POST)
	public String managePermissionProcess(@PathVariable("sellerId") String sellerId, HttpServletRequest request, Model model) throws Exception {
		if (request.getParameter("cancel") != null) {
			if (!StringUtils.equals(request.getParameter("from"), "list")) {
				return getCancelView();
			} else {
				return getSuccessView();
			}
		}
		
		//Date paidDate = new Date();
		CoreSeller coreSeller = coreSellerManager.getByQRCode(sellerId);
		
		List<CoreSellerSettlementReportDto> dtos = coreSellerReportService.getSellerSettlementReportBySellerId(sellerId);
		CoreSellerSettlementReportDto dto = dtos.get(0);
		
		TrxSellerPayment sellerPayment = new TrxSellerPayment();
		sellerPayment.setCharge(dto.getCharge());
		sellerPayment.setCoreSeller(coreSeller);
		sellerPayment.setGrandTotal(dto.getGrandTotal());
		//sellerPayment.setPaymentDate(paidDate);
		sellerPayment.setPaymentId(DateUtil.toBundleDateFormat(new Date()));
		sellerPayment.setPercentage(dto.getPercentage());
		sellerPayment.setTotalAmount(dto.getTotalAmountPaid());
		sellerPayment.setTotalTransaction(dto.getTotalTransaction());
		sellerPayment.setStatus(SettleStatusEnum.UNPAID.getName());
		//sellerPayment.setCreatedBy("SYSTEM");
		sellerPayment.setCreatedTime(new Date());
		sellerPayment = trxSellerPaymentManager.save(sellerPayment);
		
		/*List<TrxOrder> last7DaysSellerOrders = trxOrderManager.getLast7DaysOrdersBySeller(sellerId);
		for (TrxOrder trxOrder : last7DaysSellerOrders) {
			trxOrder.setPaidDate(paidDate);
			trxOrderManager.save(trxOrder);
		}*/
		saveMessage(request, getText("sellerPayment.saved", sellerPayment.getPaymentId(), request.getLocale()));
		
		return "redirect:/trx/seller/payment";
	}
	
	@RequestMapping(value = "/auth/detail/{paymentId}", method = RequestMethod.GET)
	public String detailAuthDisplay(@PathVariable("paymentId") String paymentId, Model model)
			throws Exception {
		TrxSellerPayment dtos = trxSellerPaymentManager.getTrxSellerPaymentByPaymentId(paymentId);
		model.addAttribute("status", SettleStatusEnum.values());
		model.addAttribute("settlementDto", dtos);
		return "trx/seller/authorize/detail";
	}

	@RequestMapping(value = "auth/detail/{paymentId}", method = RequestMethod.POST)
	public String authSettleProcess(@PathVariable("paymentId") String paymentId ,@Valid TrxSellerPayment tSellerPayment,
			HttpServletRequest request, Model model) throws Exception {
		if (request.getParameter("cancel") != null) {
			if (!StringUtils.equals(request.getParameter("from"), "list")) {
				return getCancelView();
			} else {
				return getSuccessView();
			}
		}
		
		Date paidDate = new Date();
		CoreSeller coreSeller = coreSellerManager.getByQRCode(tSellerPayment.getCoreSeller().getSellerId());
		
		TrxSellerPayment sellerPymt = trxSellerPaymentManager.getTrxSellerPaymentByPaymentId(paymentId);
		String startDat = sellerPymt.getStartDat();
		String endDat = sellerPymt.getEndDat();
		
		CoreUser user = coreUserManager.getUserByUsername(tSellerPayment.getCreatedBy());
		String authBy = tSellerPayment.getCreatedBy();
		  try {
		         if(!user.equals(null)){
		          CoreRole role = user.getActiveRole();
		          if (role.getId()==2){
		           tSellerPayment.setAuthoriser(authBy);
		           tSellerPayment.setAuthorizeTime(paidDate);
		           tSellerPayment.setPaymentDate(paidDate);
		           tSellerPayment.setStatus(tSellerPayment.getStatus().equals("0") ? SettleStatusEnum.PAID.getName() : SettleStatusEnum.UNPAID.getName());
		           tSellerPayment.setCoreSeller(coreSeller);
		           tSellerPayment.setCreatedBy("SYSTEM");
		           tSellerPayment.setStartDat(startDat);
		           tSellerPayment.setEndDat(endDat);
		           trxSellerPaymentManager.save(tSellerPayment);
		           saveMessage(request, getText("sellerPayment.saved", tSellerPayment.getPaymentId(), request.getLocale()));
		           
		           //List<TrxOrder> last7DaysSellerOrders = trxOrderManager.getLast7DaysOrdersBySeller(tSellerPayment.getCoreSeller().getSellerId());
		           List<TrxOrder> last7DaysSellerOrders = trxOrderManager.getListOrdersForSettlement(startDat, endDat);
		 		  for (TrxOrder trxOrder : last7DaysSellerOrders) {
		 		   trxOrder.setPaidDate(paidDate);
		 		   trxOrderManager.save(trxOrder);
		 		  }
		          return "redirect:/trx/seller/payment/auth";
		          } else {
		           saveError(request, getText("authorization.error", String.valueOf(tSellerPayment.getPaymentId()), request.getLocale()));
		           return "redirect:/trx/seller/payment/auth";
		          }
		   } else{
		    saveError(request, getText("authorization.error2", String.valueOf(tSellerPayment.getPaymentId()), request.getLocale()));
		          return "redirect:/trx/seller/payment/auth";
		   }
		         }catch(Exception ex) {
		          saveError(request, getText("authorization.error3", String.valueOf(tSellerPayment.getPaymentId()), request.getLocale()));
		         }  
		  
		    
		  
		  return "redirect:/trx/seller/payment/auth";
	}
	
	@RequestMapping(value = "/download/all/{type}", method = {RequestMethod.POST, RequestMethod.GET})
	public String downloadAllData(@PathVariable("type") String type,
			HttpServletRequest request, HttpServletResponse response, Model model) throws Exception{
		
		List<CoreSellerSettlementReportDto> dtos = coreSellerReportService.getAllSellerSettlementReport();
		
        if (dtos == null) {
        	dtos = new ArrayList<CoreSellerSettlementReportDto>();
        }
        if (type.equals("pdf")) {
            response.setContentType("application/pdf");
            File downloadFile = getFile("settlement-all", "pdf", "settlement_all.jrxml", dtos);
            FileDownloader.downloadFile(downloadFile, response, FilenameUtils.getExtension(downloadFile.getAbsolutePath()));
        }
        if (type.equals("xls")) {
            response.setContentType("application/vnd.xls");
            File downloadFile = getFile("settlement-all", "xls", "settlement_all.jrxml", dtos);
            FileDownloader.downloadFile(downloadFile, response, FilenameUtils.getExtension(downloadFile.getAbsolutePath()));
        }
		return null;
	}
    
	@RequestMapping(value = "/download/{type}", method = {RequestMethod.POST, RequestMethod.GET})
	public String downloadData(@PathVariable("type") String type, 
			@RequestParam("sellerId") String sellerId,
			HttpServletRequest request, HttpServletResponse response, Model model) throws Exception{
		
		List<CoreSellerSettlementReportDto> dtos = coreSellerReportService.getSellerSettlementReportBySellerId(sellerId);
		
        if (dtos == null) {
        	dtos = new ArrayList<CoreSellerSettlementReportDto>();
        }
        if (type.equals("pdf")) {
            response.setContentType("application/pdf");
            File downloadFile = getFile("customerActivity", "pdf", "customer_activity.jrxml", dtos);
            FileDownloader.downloadFile(downloadFile, response, FilenameUtils.getExtension(downloadFile.getAbsolutePath()));
        }
        if (type.equals("xls")) {
            response.setContentType("application/vnd.xls");
            File downloadFile = getFile("customerActivity", "xls", "customer_activity.jrxml", dtos);
            FileDownloader.downloadFile(downloadFile, response, FilenameUtils.getExtension(downloadFile.getAbsolutePath()));
        }
		return null;
	}
	
	@RequestMapping(value="/search", method = RequestMethod.POST)
	public @ResponseBody List<CoreSellerSettlementReportDto> getSellerSettlement(
			@RequestParam("sellerId") String sellerId,
			HttpServletRequest request) throws Exception{
        
		List<CoreSellerSettlementReportDto> list = coreSellerReportService.getSellerSettlementReportBySellerId(sellerId);
        return list;
	}
	
	@RequestMapping(value = "/history", method = RequestMethod.GET)
	public String history2(Model model) throws Exception{
		return "/trx/seller/history/list";
	}
	
	@RequestMapping(value = "/history", method = RequestMethod.POST)
	public String history(@RequestParam("startDate") @DateTimeFormat(iso=ISO.DATE) Date startDate, 
			@RequestParam("endDate") @DateTimeFormat(iso=ISO.DATE) Date endDate, HttpServletRequest request, Model model) throws Exception {
		String sDate = "";
    	String eDate ="";
    	
    	if(startDate.compareTo(endDate)>0){
    	      saveError(request, getText("startdate.error", request.getLocale()));      
    	     }else if (startDate.compareTo(new Date())>0 || endDate.compareTo(new Date())>0){
    	      saveError(request, getText("endstart.over.error", request.getLocale()));
    	     }else if (startDate.equals(null)){
    	      saveError(request, getText("startdate.null", request.getLocale()));
    	     }else if (endDate.equals(null)){
    	      SimpleDateFormat spdf = new SimpleDateFormat("yyyy-MM-dd");
    	      Date date = new Date();
    	      eDate = spdf.format(date);
    	      sDate = spdf.format(startDate);
    	     }else {
    	      SimpleDateFormat spdf = new SimpleDateFormat("yyyy-MM-dd");
    	         sDate = spdf.format(startDate);
    	         eDate = spdf.format(endDate);
    	     }
    	model.addAttribute("startDate", new SimpleDateFormat("dd-MM-yyyy").format(startDate));
		model.addAttribute("endDate", new SimpleDateFormat("dd-MM-yyyy").format(endDate));
		model.addAttribute("settlementHistory", trxSellerPaymentManager.getAllSellerPaymentReport(sDate, eDate));
		return "/trx/seller/history/list";
	}
	
	@RequestMapping(value = "/history/download/all/{type}/{category}", method = {RequestMethod.POST, RequestMethod.GET})
	public String downloadAllData(@PathVariable("type") String type, @PathVariable("category") String category,
			@RequestParam("startDate") @DateTimeFormat(iso = ISO.DATE) Date startDate,
			 @RequestParam("endDate") @DateTimeFormat(iso = ISO.DATE) Date endDate,
	   HttpServletRequest request, HttpServletResponse response, Model model) throws Exception{
		System.out.println("Category Report : "+category);
		String sDate = "";
		String eDate = "";
		SimpleDateFormat spdf = new SimpleDateFormat("yyyy-MM-dd");
		sDate = spdf.format(startDate);
		eDate = spdf.format(endDate);
		
		List<TrxSellerPayment> sellPay = new ArrayList<TrxSellerPayment>();
		sellPay = trxSellerPaymentManager.getAllSellerPaymentReport(sDate, eDate);
		/*SettlementDto dto = new SettlementDto();
		List<TrxSellerPayment> sellPay = trxSellerPaymentManager.getAllSellerPaymentPaid();*/
		
		
		return getExport(sellPay, type, response, category);
	}
	
	@RequestMapping(value = "/history/detail/{sellerId}/{paidDate}", method = RequestMethod.GET)
	public String historyDetail(
			@PathVariable("sellerId") String sellerId,
			@PathVariable("paidDate") String date,
			Model model)
			throws Exception {
		Date paidDate = DateUtil.fromDBFullFormat(date);
		
		trxSellerPayment = trxSellerPaymentManager.getBySellerIdAndPaidDate(sellerId, paidDate);
		orders = trxOrderManager.getPeriodicOrderBySellerid(sellerId, trxSellerPayment.getPaymentDate(), DateUtil.addDate(trxSellerPayment.getPaymentDate(), 1));
		
		model.addAttribute("trxSellerPayment", trxSellerPayment);
		model.addAttribute("serviceCash", coreSystemManager.get(Constants.SYSTEM).getServiceCash());
		model.addAttribute("orders", orders);
		return "trx/seller/history/detail";
	}
	
	@RequestMapping(value = "/history/detail/{sellerId}/{paidDate}", method = RequestMethod.POST)
	public String historyDetailPOST(
			@PathVariable("sellerId") String sellerId,
			@PathVariable("paidDate") String date, HttpServletRequest request,
			Model model)
			throws Exception {
			
		if (request.getParameter("cancel") != null) {
			if (!StringUtils.equals(request.getParameter("from"), "list")) {
				return "redirect:/trx/seller/payment/history";
			} else {
				return getSuccessView();
			}
		}
		
		return "redirect:/trx/seller/payment/history";
	}
	
	@RequestMapping(value = "/history/download/{type}/{category}", method = {RequestMethod.POST, RequestMethod.GET})
	public String downloadData1(@PathVariable("type") String type, @PathVariable("category") String category,
			HttpServletRequest request, HttpServletResponse response, Model model) throws Exception{
		System.out.println("category" + category);
        if (orders == null) {
        	orders = new ArrayList<TrxOrder>();
        }
        
		HistoryDownloadBean dto = new HistoryDownloadBean();
		List<ReportSettlementDetail> listdto = new ArrayList<ReportSettlementDetail>();
        for (TrxOrder order : orders) {
          
			for (TrxOrderItemDetail det : order.getTrxOrderItemDetails()) {
				ReportSettlementDetail detaildto = new ReportSettlementDetail();
				detaildto.setIdOrder(order.getOrderId());
				detaildto.setStatus(order.getStatus());
				detaildto.setDatOrder(order.getOrderDate().toString());
				detaildto.setItemName(det.getPk().getCoreItem().getItemName());
				detaildto.setHargaPokok(det.getPk().getCoreItem().getCostPrice());
				detaildto.setQuantity(det.getQuantity().toString());
				detaildto.setIdSeller(trxSellerPayment.getCoreSeller().getUsername());
				detaildto.setNamaSeller(trxSellerPayment.getCoreSeller().getFirstName());
				detaildto.setGrandTotal(trxSellerPayment.getGrandTotal());
				detaildto.setTotalTrx(trxSellerPayment.getTotalTransaction());
				detaildto.setPercentage(trxSellerPayment.getPercentage());
				detaildto.setServicecharge(coreSystemManager.get(Constants.SYSTEM).getServiceCash());
				detaildto.setCharge(trxSellerPayment.getCharge());
				detaildto.setTotalPaid(trxSellerPayment.getTotalAmount());
				detaildto.setAccountNo(trxSellerPayment.getCoreSeller().getAccountNumber());
				listdto.add(detaildto);
			}
			
			dto.setListdetail(listdto);
		}
        return getExport(dto.getListdetail(), type, response, category);
        
	}
	
	@RequestMapping(value="/searchSettlement", method = RequestMethod.POST)
	public @ResponseBody List<TrxSellerPayment> getCustomerData(
			@RequestParam("sellerId") String sellerId,
			@RequestParam("startDate") Date startDate,
			@RequestParam("endDate") Date endDate,
			HttpServletRequest request) throws Exception{
		
		List<TrxSellerPayment> sellerPayments = trxSellerPaymentManager.getBySellerIdAndDate(sellerId, startDate, endDate);
        
        return sellerPayments;
	}
	
	@RequestMapping(value = "/infoSettle", method = RequestMethod.GET)
	public String getSettlementInfo(Model model) throws Exception {
		return "/trx/seller/listSettlement";
	}
	
	@RequestMapping(value = "/infoSettle", method = RequestMethod.POST)
	public String viewSettlementInfo(@RequestParam("startDate") @DateTimeFormat(iso=ISO.DATE) Date startDate, 
			@RequestParam("endDate") @DateTimeFormat(iso=ISO.DATE) Date endDate,
			Model model, HttpServletRequest request) throws Exception {
		
		String sDate = "";
    	String eDate ="";    	
    	if(startDate.compareTo(endDate)>0){
    	      saveError(request, getText("startdate.error", request.getLocale()));      
    	     }else if (startDate.compareTo(new Date())>0 || endDate.compareTo(new Date())>0){
    	      saveError(request, getText("endstart.over.error", request.getLocale()));
    	     }else if (startDate.equals(null)){
    	      saveError(request, getText("startdate.null", request.getLocale()));
    	     }else if (endDate.equals(null)){
    	      SimpleDateFormat spdf = new SimpleDateFormat("yyyy-MM-dd");
    	      Date date = new Date();
    	      eDate = spdf.format(date);
    	      sDate = spdf.format(startDate);
    	     }else {
    	      SimpleDateFormat spdf = new SimpleDateFormat("yyyy-MM-dd");
    	         sDate = spdf.format(startDate);
    	         eDate = spdf.format(endDate);
    	     }
		model.addAttribute("startDate", new SimpleDateFormat("dd-MM-yyyy").format(startDate));
		model.addAttribute("endDate", new SimpleDateFormat("dd-MM-yyyy").format(endDate));
		listsettleinfo = new ArrayList<CoreSellerSettlementReportDto>();
		listsettleinfo = coreSellerReportService.getSettlementInfoPeriodic(sDate,eDate);
		model.addAttribute("settlementReportDtos", listsettleinfo);
		return "/trx/seller/listSettlement";
	}
	
	@RequestMapping(value = "/settle")
	public String prosesSettlement(HttpServletRequest request) throws Exception {
		insertSettle(listsettleinfo);
		saveMessage(request, getText("sellerPayment.saved", request.getLocale()));
		return "/trx/seller/listSettlement";
	}
	
	private void insertSettle(List<CoreSellerSettlementReportDto> settlelist) throws Exception {
		List<CoreSellerSettlementReportDto> settles = settlelist;
		System.out.println("jml :"+settles.size());
		List<TrxSellerPayment> trxSellerPayments = new ArrayList<TrxSellerPayment>();
		for (CoreSellerSettlementReportDto set : settles) {
			//Date paidDate = new Date();
			TrxSellerPayment sellerPayment = new TrxSellerPayment();
			CoreSeller coreSeller = coreSellerManager.getByQRCode(set.getSellerId());
			
			//Random rnd = new Random(System.currentTimeMillis());
			//String payId = DateUtil.toBundleDateFormat(new Date());//+rnd.nextInt(900)+100;
			String payId = DateUtil.toBundleDateFormat(new Date()).concat(UUID.randomUUID().toString());		
			//System.out.println("rnd:"+rnd);
			
			System.out.println("seller:"+coreSeller.getUsername()+"- id:"+payId);
			sellerPayment.setCharge(set.getCharge());
			sellerPayment.setCoreSeller(coreSeller);
			sellerPayment.setGrandTotal(set.getGrandTotal());
			sellerPayment.setPaymentId(payId);
			sellerPayment.setPercentage(set.getPercentage());
			sellerPayment.setTotalAmount(set.getTotalAmountPaid());
			sellerPayment.setTotalTransaction(set.getTotalTransaction());
			sellerPayment.setStatus(SettleStatusEnum.UNPAID.getName());
			sellerPayment.setCreatedTime(new Date());
			sellerPayment.setStartDat(set.getStartDat());
			sellerPayment.setEndDat(set.getEndDat());
			sellerPayment = trxSellerPaymentManager.save(sellerPayment);
			//trxSellerPayments.add(sellerPayment);
		}
		//trxSellerPaymentManager.saveAll(trxSellerPayments);
	}
	
	private <T> String getExport(List<T> dtos, String type, HttpServletResponse response, String category) throws Exception {
		String eDate = "";
		SimpleDateFormat spdf = new SimpleDateFormat("yyyy-MM-dd");
		Date date = new Date();
		eDate = spdf.format(date);
		if (type.equals("pdf")) {
			response.setContentType("application/pdf");
			File downloadFile = getFile(category.equals("history")? "HistorySettle_"+eDate : "Settlement_"+eDate, "pdf", category.equals("history")? "settlementHistory.jrxml" : "settlement_all.jrxml", dtos);
			FileDownloader.downloadFile(downloadFile, response, FilenameUtils.getExtension(downloadFile.getAbsolutePath()));
		}
		if (type.equals("xls")) {
			response.setContentType("application/vnd.xls");
			File downloadFile = getFile(category.equals("history")? "HistorySettle_"+eDate : "Settlement_"+eDate, "xls", category.equals("history")? "settlementHistory.jrxml" : "settlement_all.jrxml", dtos);
			FileDownloader.downloadFile(downloadFile, response, FilenameUtils.getExtension(downloadFile.getAbsolutePath()));
		}
		return null;
	}

	private <T> File getFile(String fileName, String format, String jrxmlFileName, List<T> dtos) throws RetailException {
		return getData(fileName, jrxmlFileName, format, dtos);
	}

	private <T> File getData(String fileName, String jrxmlFileName, String format, List<T> list) throws RetailException {
		File downloadFile = null;
		if (format.equals("pdf")) {
			downloadFile = ClientReportGenerator.generatePDF(jrxmlFileName, list, ClientReportGenerator.readProperty("jrxml.tempDir")+fileName);
		}
		if (format.equals("xls")) {
			downloadFile = ClientReportGenerator.generateXLS(jrxmlFileName, list, ClientReportGenerator.readProperty("jrxml.tempDir")+fileName);
		}
		return downloadFile;
	}
}
