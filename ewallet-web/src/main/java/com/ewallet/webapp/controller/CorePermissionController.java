package com.ewallet.webapp.controller;

//import com.anabatic.corebanking.Constants;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.ObjectRetrievalFailureException;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ewallet.enumeration.ActionTypeEnum;
import com.ewallet.enumeration.ApplicationTypeEnum;
import com.ewallet.enumeration.ModuleTypeEnum;
import com.ewallet.enumeration.RecordTypeEnum;
import com.ewallet.persistence.model.CorePermission;
import com.ewallet.service.CorePermissionManager;
import com.ewallet.service.CoreUserManager;

@Controller
@RequestMapping(value="/core/permission")
public class CorePermissionController extends BaseFormController {
	private CorePermissionManager corePermissionManager = null;
	private CoreUserManager coreUserManager;
	
	
	@Autowired
	public void setCoreUserManager(CoreUserManager coreUserManager) {
		this.coreUserManager = coreUserManager;
	}

	@Autowired
	public void setCorePermissionManager(CorePermissionManager corePermissionManager){
		this.corePermissionManager = corePermissionManager;
	}
	
	public CorePermissionController(){
		setCancelView("redirect:/core/permission");
		setSuccessView("redirect:/core/permission");
	}
	
//	@PreAuthorize("hasAnyRole('CORE:SECURITY:READ:*')")
    @RequestMapping(value="")
	public String allDisplay(Model model) throws Exception{
		model.addAttribute("corePermissions", corePermissionManager.getAll());
		return "core/permission/list";
	}
    
//	@PreAuthorize("hasAnyRole('CORE:SECURITY:INPUT:*')")
    @RequestMapping(value="/add", method = RequestMethod.GET)
	public String addDisplay(Model model) throws Exception{
		model.addAttribute("corePermission", new CorePermission());
		model.addAttribute("moduleTypeEnums", ModuleTypeEnum.values());
		model.addAttribute("applicationTypeEnums", ApplicationTypeEnum.values());
		model.addAttribute("actionTypeEnums", ActionTypeEnum.values());
		model.addAttribute("recordTypeEnums", RecordTypeEnum.values());
		return "core/permission/add";
	}
    
//	@PreAuthorize("hasAnyRole('CORE:SECURITY:INPUT:*')")
    @RequestMapping(value = "/add", method = RequestMethod.POST)
	public String addProcess(@Valid CorePermission corePermission,BindingResult errors, HttpServletRequest request,
			HttpServletResponse response, Model model) throws Exception{
		if (request.getParameter("cancel") != null) {
            if (!StringUtils.equals(request.getParameter("from"), "list")) {
                return getCancelView();
            } else {
                return getSuccessView();
            }
        }
            if (errors.hasErrors() && request.getParameter("delete") == null) {
            	model.addAttribute("moduleTypeEnums", ModuleTypeEnum.values());
        		model.addAttribute("applicationTypeEnums", ApplicationTypeEnum.values());
        		model.addAttribute("actionTypeEnums", ActionTypeEnum.values());
        		model.addAttribute("recordTypeEnums", RecordTypeEnum.values());
                return "core/permission/add";
            }
        
        try {
        	if(corePermission.getId() == null){
        		corePermission.setId(Integer.MIN_VALUE);
        	}
            corePermissionManager.get(corePermission.getId());
            saveError(request, getText("errors.exists", corePermission.getId().toString(), request.getLocale()));
            model.addAttribute("moduleTypeEnums", ModuleTypeEnum.values());
    		model.addAttribute("applicationTypeEnums", ApplicationTypeEnum.values());
    		model.addAttribute("actionTypeEnums", ActionTypeEnum.values());
    		model.addAttribute("recordTypeEnums", RecordTypeEnum.values());
            return "core/permission/add";

        } catch (ObjectRetrievalFailureException ex) {
        	if (corePermission.getId() == Integer.MIN_VALUE) corePermission.setId(null);
            corePermissionManager.save(corePermission);
            //TODO: Inspect this code
            //corePermission.setId(corePermissionManager.getLike(corePermission).getId());
            saveMessage(request, getText("corePermission.saved", " "+corePermission.getName(), request.getLocale()));
        }
        
        return "redirect:/core/permission";
	}
	
//    @PreAuthorize("hasAnyRole('CORE:SECURITY:DELETE:*')")
    @RequestMapping(value = "/delete/{id}", method = RequestMethod.GET)
	public String deleteDisplay(@PathVariable("id") Integer id, Model model) throws Exception{
		model.addAttribute("corePermission", corePermissionManager.get(id));
		return "core/permission/delete";
	}
	
//    @PreAuthorize("hasAnyRole('CORE:SECURITY:DELETE:*')")
    @RequestMapping(value = "/delete", method = RequestMethod.POST)
	public String deleteProcess(CorePermission corePermission, BindingResult errors, HttpServletRequest request,
			HttpServletResponse response) throws Exception{
		if (request.getParameter("cancel") != null) {
            if (!StringUtils.equals(request.getParameter("from"), "list")) {
                return getCancelView();
            } else {
                return getSuccessView();
            }
        }
		
		if(coreUserManager.getCheckPermissionOnUser(corePermission.getId()) != null){
			//System.out.println("lewat");
			saveError(request, getText("coreRole.roleUsedByUser", corePermission.getId().toString(), request.getLocale()));
			return "core/permission/delete";
		}

        corePermissionManager.remove(corePermission.getId());
        saveMessage(request, getText("corePermission.deleted", corePermission.getId().toString(), request.getLocale()));
        return "redirect:/core/permission";
	}
    
//	@PreAuthorize("hasAnyRole('CORE:SECURITY:INPUT:*')")
    @RequestMapping(value = "/edit/{id}", method = RequestMethod.GET)
	public String editDisplay(@PathVariable("id") Integer id, Model model) throws Exception{
		model.addAttribute("corePermission", corePermissionManager.get(id));
		model.addAttribute("moduleTypeEnums", ModuleTypeEnum.values());
		model.addAttribute("applicationTypeEnums", ApplicationTypeEnum.values());
		model.addAttribute("actionTypeEnums", ActionTypeEnum.values());
		model.addAttribute("recordTypeEnums", RecordTypeEnum.values());
		return "core/permission/edit";
	}
    
//	@PreAuthorize("hasAnyRole('CORE:SECURITY:INPUT:*')")
    @RequestMapping(value = "/edit/{id}", method = RequestMethod.POST)
	public String editProcess(@Valid CorePermission corePermission, BindingResult errors, HttpServletRequest request,
			HttpServletResponse response, Model model) throws Exception{
		if (request.getParameter("cancel") != null) {
            if (!StringUtils.equals(request.getParameter("from"), "list")) {
                return getCancelView();
            } else {
                return getSuccessView();
            }
        }

            if (errors.hasErrors() && request.getParameter("delete") == null) {
            	model.addAttribute("moduleTypeEnums", ModuleTypeEnum.values());
        		model.addAttribute("applicationTypeEnums", ApplicationTypeEnum.values());
        		model.addAttribute("actionTypeEnums", ActionTypeEnum.values());
        		model.addAttribute("recordTypeEnums", RecordTypeEnum.values());
                return "core/permission/edit";
            }
        corePermissionManager.save(corePermission);
        saveMessage(request, getText("corePermission.saved", corePermission.getId().toString(), request.getLocale()));
        return "redirect:/core/permission";
	}


    private List<ApplicationTypeEnum> getByModule(String module){

        List<ApplicationTypeEnum> applicationTypeEnums = new ArrayList<ApplicationTypeEnum>();

        for(ApplicationTypeEnum e : ApplicationTypeEnum.values()){
            if(e.getApplicationModule().equals(module)){
                applicationTypeEnums.add(e);
            }
        }
        return applicationTypeEnums;
    }
    
//    @PreAuthorize("hasAnyRole('CORE:SECURITY:READ:*')")
    @RequestMapping(value = "/getmodules", method = RequestMethod.POST)
    public @ResponseBody List<ApplicationTypeEnum> getApplication(@RequestParam("module") String module) {
        return getByModule(module);
    }

}
