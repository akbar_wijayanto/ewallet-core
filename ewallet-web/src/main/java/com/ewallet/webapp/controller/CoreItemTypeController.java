package com.ewallet.webapp.controller;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.ewallet.enumeration.CustomerGenderEnum;
import com.ewallet.enumeration.CustomerTitleEnum;
import com.ewallet.persistence.model.CoreItemType;
import com.ewallet.persistence.model.CorePermission;
import com.ewallet.persistence.model.CoreRole;
import com.ewallet.persistence.model.CoreSeller;
import com.ewallet.persistence.model.CoreUser;
import com.ewallet.persistence.model.LabelValueStatus;
import com.ewallet.service.CoreItemManager;
import com.ewallet.service.CoreItemTypeManager;
import com.ewallet.service.CoreSellerManager;

/**
 * @author akbar.wijayanto
 * Date Oct 19, 2015 8:17:07 PM
 */
@Controller
@RequestMapping("/core/item/type")
public class CoreItemTypeController extends BaseFormController {

	@Autowired
	private CoreSellerManager coreSellerManager;
	
	@Autowired
	private CoreItemManager coreItemManager;
	
	@Autowired
	private CoreItemTypeManager coreItemTypeManager;
	
	public CoreItemTypeController() {
		setCancelView("redirect:/core/item/type");
		setSuccessView("redirect:/core/item/type");
	}

	// @PreAuthorize("hasAnyRole('CORE:USER:READ:*')")
	@RequestMapping(value = "")
	public String view(Model model) throws Exception {
		model.addAttribute("coreItemTypes", coreItemTypeManager.getAllLive());
		return "/core/item/type/list";
	}

	// @PreAuthorize("hasAnyRole('CORE:SECURITY:INPUT:*')")
	@RequestMapping(value = "/add", method = RequestMethod.GET)
	public String addDisplay(Model model) throws Exception {
		model.addAttribute("coreItemType", new CoreItemType());
		return "core/item/type/add";
	}

	// @PreAuthorize("hasAnyRole('CORE:SECURITY:INPUT:*')")
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public String addProcess(@Valid CoreItemType coreItemType,
			BindingResult errors, HttpServletRequest request,
			HttpServletResponse response, Model model) throws Exception {
		if (request.getParameter("cancel") != null) {
			if (!StringUtils.equals(request.getParameter("from"), "list")) {
				return getCancelView();
			} else {
				return getSuccessView();
			}
		}
		
		coreItemTypeManager.save(coreItemType);
		saveMessage(request, getText("coreItemType.saved", coreItemType.getName(), request.getLocale()));
		return "redirect:/core/item/type";
	}
	
//	@PreAuthorize("hasAnyRole('CORE:ROLE:INPUT:*')")
    @RequestMapping(value="/edit/{id}", method = RequestMethod.GET)
	public String editDisplay(@PathVariable("id") Integer id, Model model) throws Exception{
		model.addAttribute("coreItemType", coreItemTypeManager.get(new Long(id)));
		return "core/item/type/edit";
	}
	
//	@PreAuthorize("hasAnyRole('CORE:ROLE:INPUT:*')")
    @RequestMapping(value="/edit/{id}", method = RequestMethod.POST)
	public String editProcess(@Valid CoreItemType coreItemType, BindingResult errors, HttpServletRequest request,
			HttpServletResponse response, Model model) throws Exception{
		if (request.getParameter("cancel") != null) {
            if (!StringUtils.equals(request.getParameter("from"), "list")) {
                return getCancelView();
            } else {
                return getSuccessView();
            }
        }

        if (errors.hasErrors() && request.getParameter("delete") == null) {
            return "core/item/type/edit";
        }
        coreItemTypeManager.save(coreItemType);
        saveMessage(request, getText("coreItemType.saved", coreItemType.getName(), request.getLocale()));
        return "redirect:/core/item/type";
	}
    
//	@PreAuthorize("hasAnyRole('CORE:USER:READ:*')")
	@RequestMapping(value = "/detail/{id}", method = RequestMethod.GET)
	public String detailDisplay(@PathVariable("id") Long id, Model model)
			throws Exception {
		model.addAttribute("coreItemType", coreItemTypeManager.getLiveById(id));
		return "core/item/type/detail";
	}

//	@PreAuthorize("hasAnyRole('CORE:USER:INPUT:*')")
	@RequestMapping(value = "/detail/{id}", method = RequestMethod.POST)
	public String managePermissionProcess(@PathVariable("id") Long id, HttpServletRequest request, Model model) throws Exception {
		if (request.getParameter("cancel") != null) {
			if (!StringUtils.equals(request.getParameter("from"), "list")) {
				return getCancelView();
			} else {
				return getSuccessView();
			}
		}
		CoreItemType coreItemType = coreItemTypeManager.getLiveById(id);
		coreItemTypeManager.save(coreItemType);

		return "redirect:/core/item/type";
	}
	
}
