package com.ewallet.webapp.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.ewallet.persistence.model.TrxTopup;
import com.ewallet.service.TrxTopupManager;

/**
 * @author akbar.wijayanto
 * Date Oct 22, 2015 11:32:27 AM
 */
@Controller
@RequestMapping("/trx/topup")
public class TrxTopupController extends BaseFormController {

	@Autowired
	private TrxTopupManager trxTopupManager;
	
	public TrxTopupController() {
		setCancelView("redirect:/trx/topup");
		setSuccessView("redirect:/trx/topup");
	}

	// @PreAuthorize("hasAnyRole('CORE:USER:READ:*')")
	@RequestMapping(value = "")
	public String view(Model model) throws Exception {
		model.addAttribute("trxTopups", trxTopupManager.getAllLive());
		return "/trx/topup/list";
	}

//	@PreAuthorize("hasAnyRole('CORE:USER:READ:*')")
	@RequestMapping(value = "/detail/{id}", method = RequestMethod.GET)
	public String detailDisplay(@PathVariable("id") Long id, Model model)
			throws Exception {
		TrxTopup trxTopup = trxTopupManager.getLiveById(id);
		model.addAttribute("trxTopup", trxTopup);
		return "trx/topup/detail";
	}

//	@PreAuthorize("hasAnyRole('CORE:USER:INPUT:*')")
	@RequestMapping(value = "/detail/{id}", method = RequestMethod.POST)
	public String managePermissionProcess(@PathVariable("id") Long id, HttpServletRequest request, Model model) throws Exception {
		if (request.getParameter("cancel") != null) {
			if (!StringUtils.equals(request.getParameter("from"), "list")) {
				return getCancelView();
			} else {
				return getSuccessView();
			}
		}
		TrxTopup trxTopup = trxTopupManager.getLiveById(id);
		model.addAttribute("trxTopup", trxTopup);
		return "redirect:/trx/topup";
	}
    
}
