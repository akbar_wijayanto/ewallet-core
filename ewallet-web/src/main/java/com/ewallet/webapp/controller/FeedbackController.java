package com.ewallet.webapp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.ewallet.service.FeedbackManager;

/**
 * @author akbar.wijayanto
 * Date Nov 10, 2015 9:42:37 AM
 */
@Controller
@RequestMapping("/feedback")
public class FeedbackController extends BaseFormController {

	@Autowired
	private FeedbackManager feedbackManager;
	
	public FeedbackController() {
		setCancelView("redirect:/feedback");
		setSuccessView("redirect:/feedback");
	}

	// @PreAuthorize("hasAnyRole('CORE:USER:READ:*')")
	@RequestMapping(value = "")
	public String view(Model model) throws Exception {
		model.addAttribute("feedbacks", feedbackManager.getAllLive());
		return "/feedback/list";
	}
	
	@RequestMapping(value = "/management")
	public String viewManagement(Model model) throws Exception {
		model.addAttribute("feedbacks", feedbackManager.getAllManagementFeedbacks());
		return "/feedback/management";
	}

	@RequestMapping(value = "/seller")
	public String viewSeller(Model model) throws Exception {
		model.addAttribute("feedbacks", feedbackManager.getAllSellerFeedbacks());
		return "/feedback/seller";
	}
	
}
