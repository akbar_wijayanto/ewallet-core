package com.ewallet.webapp.listener;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.web.context.WebApplicationContext;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

/**
 * @author : <a href="mailto:ab.annas@gmail.com">Andi Baso Annas</a>
 * @version : 1.0, 12/27/12, 10:59 AM
 */
public class SecuritySessionListener implements HttpSessionListener{
    private static final Log log = LogFactory.getLog(SecuritySessionListener.class);

    
    @Override
    public void sessionCreated(HttpSessionEvent se) {

    }

    @Override
    public void sessionDestroyed(HttpSessionEvent se) {
        ServletContext servletContext = se.getSession().getServletContext();
        WebApplicationContext appContext = (WebApplicationContext) servletContext.getAttribute(WebApplicationContext.ROOT_WEB_APPLICATION_CONTEXT_ATTRIBUTE);

//        CoreUserManager coreUserManager = (CoreUserManager )appContext.getBean("coreUserManager");
//
//        String username = (String) se.getSession().getAttribute("username");
//        if(StringUtils.isNotEmpty(username)){
//            log.debug("Username : "+username);
//            CoreUser coreUser = coreUserManager.getUserByUsername(username);
//            coreUser.setSession(null);
//            coreUserManager.updateLoginAttempt(coreUser);
//        }

    }
}
