/**
 * 
 */
package com.ewallet.webapp.util;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

/**
 * 
 * @version 1.0, Sep 9, 2015 2:21:12 PM
 */
public class ApplicationContextProvider implements ApplicationContextAware {
	private static ApplicationContext context;
    
    public ApplicationContext getApplicationContext() {
        return context;
    }
     
    @Override
    public void setApplicationContext(ApplicationContext ac) throws BeansException {
        context = ac;
    }
}
