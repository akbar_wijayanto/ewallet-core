/**
 * 
 */
package com.ewallet.service.endpoint;

import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import com.ewallet.businesslogic.util.MailObject;
import com.ewallet.exception.RetailException;
import com.ewallet.persistence.model.CoreUser;
import com.ewallet.persistence.model.Feedback;
import com.ewallet.service.CoreSellerManager;
import com.ewallet.service.CoreUserManager;
import com.ewallet.service.FeedbackManager;
import com.ewallet.service.contract.ActivationRequest;
import com.ewallet.service.contract.ActivationResponse;
import com.ewallet.service.contract.CreateFeedbackRequest;
import com.ewallet.service.contract.CreateFeedbackResponse;
import com.ewallet.service.contract.GetUserPreferenceRequest;
import com.ewallet.service.contract.GetUserPreferenceResponse;
import com.ewallet.service.contract.LoginRequest;
import com.ewallet.service.contract.LoginResponse;
import com.ewallet.service.contract.ProfileRequest;
import com.ewallet.service.contract.ProfileResponse;
import com.ewallet.service.contract.SettingUserPreferenceRequest;
import com.ewallet.service.contract.SettingUserPreferenceResponse;
import com.ewallet.service.contract.bean.BaseAccount;
import com.ewallet.service.contract.bean.KeyValuePair;
import com.ewallet.service.contract.bean.UserDetail;
import com.ewallet.service.converters.api.WebServiceConverter;

/**
 * @author akbar.wijayanto
 * Date Oct 23, 2015 6:35:23 PM
 */
@Endpoint
public class CustomerEndpoint extends BaseEndpoint {

	@Value("${domain}") 		protected String DOMAIN;
	@Value("${subdomain}") 		protected String SUBDOMAIN;
	
	@Autowired
	private CoreUserManager coreUserManager;
	
	@Autowired
	private FeedbackManager feedbackManager;
	
	@Autowired
	private AuthenticationManager authenticationManager;
	
	@Autowired
	@Qualifier("coreUserConverter")
	private WebServiceConverter<CoreUser, BaseAccount> coreUserConverter;
	
	@Autowired
	@Qualifier("mailObject")
	private MailObject mailObject;
	
	@Autowired
	private CoreSellerManager coreSellerManager;
	
	@PayloadRoot(localPart = "ActivationRequest", namespace = TARGET_NAMESPACE)
	public @ResponsePayload ActivationResponse activation(@RequestPayload ActivationRequest request) throws RetailException {
		ActivationResponse response = new ActivationResponse();
		
		try {
			// Cek data user ke LDAP
			authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(
					request.getUsername(), request.getPassword()));
		} catch (Exception e) {
			throw new RetailException("00 - "+e.getMessage());
		}
		
		try {
			CoreUser coreUser = coreUserManager.getUserByUsername(request.getUsername());
			if (coreUser==null) {
				throw new RetailException("901 - User not registered.");
			} else {
				// cek data sva
//				AccountDto accountDto = accountAdapter.createUserAccount(coreUser.getUsername());
//				if (!accountDto.isAsMember()) {
//					throw new RetailException("912 - You don't have an account. Please register in advance");
//				}else{
//					coreUser.setSva(accountDto.getAccountNumber());
//				}
				if (coreUser.getAccountEnabled()==false) {
					// Generate activation URL dan kirim ke email
					String token = UUID.randomUUID().toString();
					coreUser.setToken(token);
					coreUser = coreUserManager.save(coreUser);
					String url = DOMAIN+SUBDOMAIN+"/registration/"+token+" ";
					System.out.println(url);
					
					String from = "koperasi.ati@anabatic.com";
					String to = coreUser.getEmail();
					// Subject
					StringBuffer subjectResult = new StringBuffer();
					subjectResult.append("[KAMP] - Activation your KAMP account using this email");
					// Content
					StringBuffer contentResult = new StringBuffer();
					contentResult.append("\nDear "+coreUser.getFirstName()+" "+coreUser.getLastName()+",<br>");
					contentResult.append("<br>");
					contentResult.append("Thank you for your activation. Please press the link below to activate your account :<br>");
					contentResult.append("<br>");
					contentResult.append(""+url+" <br>");
					contentResult.append("<br>");
					contentResult.append("With this application, your transactions more easily, quickly and without having to wait a long time.<br>");
					contentResult.append("<br>");
					contentResult.append("Thank you and happy shopping !<br>");
					contentResult.append("<br>");
					contentResult.append("Best Regards,<br>");
					contentResult.append("<br>");
					contentResult.append("Koperasi Konsumen Anabatic Sejahtera\n");
					
					mailObject.sendMail(from, to, null, subjectResult.toString(), contentResult.toString());
					
					response.setStatus("910 - Thanks you for activation. Confirmation url has been send to your email. Check your email and confirm your account");
				} else {
					response.setStatus("911 - Account already active.");
				}
			}
		} catch (Exception e) {
			throw new RetailException(e.getMessage());
		}
		return response;
	}
	
	@PayloadRoot(localPart = "LoginRequest", namespace = TARGET_NAMESPACE)
	public @ResponsePayload LoginResponse loginCustomer(@RequestPayload LoginRequest request) throws RetailException {
		LoginResponse response = new LoginResponse();

		try {
			authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(
					request.getUsername(), request.getPassword()));
		} catch (Exception e) {
			throw new RetailException("00 - "+e.getMessage());
		}
		
		CoreUser coreUser = coreUserManager.getUserByUsername(request.getUsername());
		
		if (coreUser!=null) {
			if (!coreUser.getAccountEnabled()) {
				throw new RetailException("1002 - Your account not activated for this application. Please activation first");
			}
			response.setUser(coreUserConverter.toContract(coreUser));
		} else {
			throw new RetailException("1001 - Customer not found.");
		}
		
		return response;
	}
	
	@PayloadRoot(localPart = "ProfileRequest", namespace = TARGET_NAMESPACE)
	public @ResponsePayload ProfileResponse profil(@RequestPayload ProfileRequest request) throws RetailException {
		ProfileResponse response = new ProfileResponse();
		
		CoreUser coreUser = coreUserManager.getUserByUsername(request.getUsername());
		if (coreUser==null) {
			throw new RetailException("01 - Customer not found.");
		} else {
			UserDetail userDetail = new UserDetail();
			userDetail.setEmail(coreUser.getEmail());
			userDetail.setFirstName(coreUser.getFirstName());
			userDetail.setId(coreUser.getId().toString());
			userDetail.setLastName(coreUser.getLastName());
			userDetail.setUsername(coreUser.getUsername());
			userDetail.setAccountEnabled(coreUser.getAccountEnabled());
			userDetail.setCompany(coreUser.getCompany());
			userDetail.setQrCode(generateByteCode(coreUser.getSva()));
			// Get account balance dan set ke KeyValuePair -> key : accountNumber, value : balance
//			AccountDto dto = accountAdapter.balanceInquiry(coreUser.getSva());
			KeyValuePair keyValuePair = new KeyValuePair();
//			keyValuePair.setKey(dto.getAccountNumber());
//			keyValuePair.setValue(dto.getAmount().toString());
//			userDetail.getBalance().add(keyValuePair);
//			userDetail.setAsMember(dto.isAsMember());
			response.setUserDetail(userDetail);
		}
		
		return response;
	}
	
	@PayloadRoot(localPart = "SettingUserPreferenceRequest", namespace = TARGET_NAMESPACE)
	public @ResponsePayload SettingUserPreferenceResponse settingUserPreference(@RequestPayload SettingUserPreferenceRequest request) throws RetailException {
		SettingUserPreferenceResponse response = new SettingUserPreferenceResponse();
		
		// Get account ke kkas dan set untuk jumlah limit payroll nya
//		AccountDto dto = new AccountDto();
//		dto.setAccountNumber(request.getAccountNumber());
//		dto.setLimitPayroll(request.getLimitPayroll());
//		dto.setAllowedPayroll(request.isAllowedPayroll());
//		
//		dto = accountAdapter.updateLimitPayroll(dto);
		
//		response.setAccountNumber(dto.getAccountNumber());
//		response.setAllowedPayroll(dto.isAllowedPayroll());
//		response.setLimitPayroll(dto.getLimitPayroll());
		
		return response;
	}

	@PayloadRoot(localPart = "CreateFeedbackRequest", namespace = TARGET_NAMESPACE)
	public @ResponsePayload CreateFeedbackResponse createFeedback(@RequestPayload CreateFeedbackRequest request) throws RetailException {
		CreateFeedbackResponse response = new CreateFeedbackResponse();
		
		CoreUser coreUser = coreUserManager.getUserByUsername(request.getUsername());
		if (coreUser==null) {
			throw new RetailException("01 - Customer not found.");
		}
		
		Feedback feedback = new Feedback();
		feedback.setCoreUser(coreUser);
		feedback.setMessage(request.getMessage());
		feedback.setRating(request.getRating());
		
		switch (request.getType()) {
		case 0: // for seller
			feedback.setCoreSeller(coreSellerManager.getByQRCode(request.getSellerId()));
			break;
		case 1:
			feedback.setManagement("1");
			break;
		default:
			break;
		}
		
		feedback = feedbackManager.save(feedback);
		
		if (feedback != null) {
			response.setStatus("3601 - Success.");
		}
		
		return response;
	}
	
	@PayloadRoot(localPart = "GetUserPreferenceRequest", namespace = TARGET_NAMESPACE)
	public @ResponsePayload GetUserPreferenceResponse getUserPreference(@RequestPayload GetUserPreferenceRequest request) throws RetailException {
		GetUserPreferenceResponse response = new GetUserPreferenceResponse();
		
		CoreUser coreUser = coreUserManager.getUserByUsername(request.getUsername());
		if (coreUser==null) {
			throw new RetailException("01 - Customer not found.");
		}
		
		// Get account ke kkas dan set untuk jumlah limit payroll nya
//		AccountDto dto = new AccountDto();
//		dto.setAccountNumber(coreUser.getSva());
//		
//		dto = accountAdapter.getLimitPayroll(dto);
		
//		response.setAccountNumber(dto.getAccountNumber());
//		response.setAllowedPayroll(dto.isAllowedPayroll());
//		response.setLimitPayroll(dto.getLimitPayroll());
		
		return response;
	}
}
