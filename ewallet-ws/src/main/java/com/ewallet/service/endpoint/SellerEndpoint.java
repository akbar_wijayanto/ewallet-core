/**
 * 
 */
package com.ewallet.service.endpoint;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import com.ewallet.enumeration.OrderStatusEnum;
import com.ewallet.enumeration.RecordStatusEnum;
import com.ewallet.exception.RetailException;
import com.ewallet.persistence.model.CoreItem;
import com.ewallet.persistence.model.CoreSeller;
import com.ewallet.persistence.model.CoreUser;
import com.ewallet.persistence.model.TrxOrder;
import com.ewallet.persistence.model.TrxOrderItemDetail;
import com.ewallet.persistence.model.TrxOrderItemId;
import com.ewallet.persistence.model.TrxRecord;
import com.ewallet.persistence.model.TrxSellerPayment;
import com.ewallet.service.CoreItemManager;
import com.ewallet.service.CoreSellerManager;
import com.ewallet.service.CoreUserManager;
import com.ewallet.service.TrxOrderItemDetailManager;
import com.ewallet.service.TrxOrderManager;
import com.ewallet.service.TrxRecordManager;
import com.ewallet.service.TrxRefundManager;
import com.ewallet.service.TrxSellerPaymentManager;
import com.ewallet.service.contract.ActiveInactiveSellerRequest;
import com.ewallet.service.contract.ActiveInactiveSellerResponse;
import com.ewallet.service.contract.GetAllSellerRequest;
import com.ewallet.service.contract.GetAllSellerResponse;
import com.ewallet.service.contract.GetItemBySellerIdRequest;
import com.ewallet.service.contract.GetItemBySellerIdResponse;
import com.ewallet.service.contract.GetPaymentListRequest;
import com.ewallet.service.contract.GetPaymentListResponse;
import com.ewallet.service.contract.GetSellerOrderListRequest;
import com.ewallet.service.contract.GetSellerOrderListResponse;
import com.ewallet.service.contract.GetSellerPaymentTrxRequest;
import com.ewallet.service.contract.GetSellerPaymentTrxResponse;
import com.ewallet.service.contract.ListSellerPaymentTrxType;
import com.ewallet.service.contract.ProcessOrderRequest;
import com.ewallet.service.contract.ProcessOrderResponse;
import com.ewallet.service.contract.ProcessRefundRequest;
import com.ewallet.service.contract.ProcessRefundResponse;
import com.ewallet.service.contract.SellerLoginQRCodeRequest;
import com.ewallet.service.contract.SellerLoginQRCodeResponse;
import com.ewallet.service.contract.bean.BaseAccount;
import com.ewallet.service.contract.bean.Item;
import com.ewallet.service.contract.bean.Order;
import com.ewallet.service.contract.bean.Seller;
import com.ewallet.service.contract.bean.Transaction;
import com.ewallet.service.converters.api.WebServiceConverter;
import com.ewallet.util.DateUtil;

/**
 * @author akbar.wijayanto
 * Date Oct 23, 2015 10:58:36 PM
 */
@Endpoint
public class SellerEndpoint extends BaseEndpoint {

	@Autowired
	private CoreUserManager coreUserManager;
	
	@Autowired
	private CoreSellerManager coreSellerManager;
	
	@Autowired
	private AuthenticationManager authenticationManager;
	
	@Autowired
	private TrxRefundManager trxRefundManager;
	
	@Autowired
	private TrxOrderManager trxOrderManager;
	
	@Autowired
	private TrxRecordManager trxRecordManager;
	
	@Autowired
	private CoreItemManager coreItemManager;
	
	@Autowired
	private TrxSellerPaymentManager trxSellerPaymentManager;
	
	@Autowired
	@Qualifier("coreUserConverter")
	private WebServiceConverter<CoreUser, BaseAccount> coreUserConverter;
	
	@Autowired
	@Qualifier("transactionHistoryConverter")
	private WebServiceConverter<TrxRecord, Transaction> transactionHistoryConverter;
	
	@Autowired
	private TrxOrderItemDetailManager trxOrderItemDetailManager;
	
	@PayloadRoot(localPart = "SellerLoginQRCodeRequest", namespace = TARGET_NAMESPACE)
	public @ResponsePayload SellerLoginQRCodeResponse sellerLoginQRCode(@RequestPayload SellerLoginQRCodeRequest request) throws RetailException {
		SellerLoginQRCodeResponse response = new SellerLoginQRCodeResponse();
		
		CoreSeller coreSeller = coreSellerManager.getByQRCode(request.getQrcode());
		
		if (coreSeller!=null) {
			Seller seller = new Seller();
			seller.setId(coreSeller.getId().toString());
			seller.setPhoneNumber(coreSeller.getPhoneNumber());
			seller.setSellerId(coreSeller.getSellerId());
			seller.setSellerName(coreSeller.getFirstName().concat(" ").concat(coreSeller.getLastName()));
			seller.setStreetAddress1(coreSeller.getStreetAddress1());
			seller.setStreetAddress2(coreSeller.getStreetAddress2());
			seller.setQrCode(generateByteCode(coreSeller.getSellerId()));
			coreSeller.setStatus(RecordStatusEnum.LIVE.getName());
			coreSeller = coreSellerManager.save(coreSeller);
			
			response.setSeller(seller);
		} else {
			throw new RetailException("01 - Seller not found.");
		}
		
		return response;
	}
	
	@PayloadRoot(localPart = "GetSellerOrderListRequest", namespace = TARGET_NAMESPACE)
	public @ResponsePayload GetSellerOrderListResponse getsellerOrder(@RequestPayload GetSellerOrderListRequest request) throws RetailException {
		GetSellerOrderListResponse response = new GetSellerOrderListResponse();
		
		List<TrxOrder> orders = trxOrderManager.getOrdersBySeller(request.getSellerId());
		for (TrxOrder trxOrder : orders) {
			Order order = new Order();
			order.setId(trxOrder.getOrderId());
			order.setOrderId(trxOrder.getOrderId());
			if (trxOrder.getOrderDate()!=null) order.setOrderDate(DateUtil.toXMLGregorian(trxOrder.getOrderDate()));
			if (trxOrder.getPaidDate()!=null) order.setPaidDate(DateUtil.toXMLGregorian(trxOrder.getPaidDate()));
			order.setStatus(trxOrder.getStatus());
			order.setUserId(trxOrder.getCoreUser().getId().toString());
			order.setCustomerName(trxOrder.getCoreUser().getFirstName().concat(" ").concat(trxOrder.getCoreUser().getLastName()));
			order.setSellerName(trxOrder.getCoreSeller().getFirstName().concat(" ").concat(trxOrder.getCoreSeller().getLastName()));
			for (TrxOrderItemDetail orderItemDetail : trxOrder.getTrxOrderItemDetails()) {
				TrxOrderItemId pk = orderItemDetail.getPk();
				Item item = new Item();
				item.setDescription(pk.getCoreItem().getDescription());
				item.setId(pk.getCoreItem().getId().toString());
				item.setItemId(pk.getCoreItem().getItemId());
				item.setItemName(pk.getCoreItem().getItemName());
				item.setItemType(pk.getCoreItem().getCoreItemType().getName());
				item.setSellerName(pk.getCoreItem().getCoreSeller().getFirstName().concat(" ").concat(pk.getCoreItem().getCoreSeller().getLastName()));
				item.setSellingPrice(pk.getCoreItem().getSellingPrice());
				item.setQuantity(orderItemDetail.getQuantity());
				order.getItems().add(item);
				
			}
//			for (CoreItem coreItem : trxOrder.getCoreItems()) {
//				Item item = new Item();
//				item.setDescription(coreItem.getDescription());
//				item.setId(coreItem.getId().toString());
//				item.setItemId(coreItem.getItemId());
//				item.setItemName(coreItem.getItemName());
//				item.setItemType(coreItem.getCoreItemType().getName());
//				item.setSellerName(coreItem.getCoreSeller().getFirstName().concat(" ").concat(coreItem.getCoreSeller().getLastName()));
//				item.setSellingPrice(coreItem.getSellingPrice());
//				item.setQrCode(generateByteCode(coreItem.getItemId()));
//				item.setImage(generateByteCode(coreItem.getFilePath()));
//				order.getItems().add(item);
//			}
			response.getOrders().add(order);
			
		}
		
		return response;
	}
	
	@PayloadRoot(localPart = "ProcessOrderRequest", namespace = TARGET_NAMESPACE)
	public @ResponsePayload ProcessOrderResponse processOrder(@RequestPayload ProcessOrderRequest request) throws RetailException {
		ProcessOrderResponse response = new ProcessOrderResponse();
		
		TrxOrder order = trxOrderManager.getOrderByOrderId(request.getTransactionId());
		switch (request.getStatus()) {
		case 0: order.setStatus(OrderStatusEnum.PROCESS.getName()); break; // in progress
		case 1: order.setStatus(OrderStatusEnum.DONE.getName()); break; // ready
		case 2: order.setStatus(OrderStatusEnum.REFUND.getName()); break; // refund
		case 3: order.setStatus(OrderStatusEnum.DELIVERED.getName()); break; // delivered
		default:
			break;
		}
		order = trxOrderManager.save(order);
		response.setTransactionId(order.getOrderId());
		response.setStatus(order.getStatus());
		return response;
	}
	
	@PayloadRoot(localPart = "ProcessRefundRequest", namespace = TARGET_NAMESPACE)
	public @ResponsePayload ProcessRefundResponse processRefund(@RequestPayload ProcessRefundRequest request) throws RetailException {
		ProcessRefundResponse response = new ProcessRefundResponse();
		
		TrxOrder order = trxOrderManager.getOrderByOrderId(request.getOrderId());
		if (order != null) {
			try {
//				PaymentDto dto = paymentAdapter.refundTransaction(order.getPaymentReference());
//				if (dto != null) {
//					order.setStatus(OrderStatusEnum.REFUND.getName());
//					order = trxOrderManager.save(order);
//					response.setReference(dto.getReference());
//				}
			} catch (Exception e) {
				// TODO: handle exception
			}
		}
		return response;
	}
	
	@PayloadRoot(localPart = "GetPaymentListRequest", namespace = TARGET_NAMESPACE)
	public @ResponsePayload GetPaymentListResponse getPaymentList(@RequestPayload GetPaymentListRequest request) throws RetailException {
		GetPaymentListResponse response = new GetPaymentListResponse();
		
		List<TrxRecord> trxRecords = trxRecordManager.getBySellerId(request.getSellerId());
		for (TrxRecord trxRecord : trxRecords) {
			response.getPayments().add(transactionHistoryConverter.toContract(trxRecord));
		}
		
		return response;
	}
	
	@PayloadRoot(localPart = "GetAllSellerRequest", namespace = TARGET_NAMESPACE)
	public @ResponsePayload GetAllSellerResponse getAllSeller(@RequestPayload GetAllSellerRequest request) throws RetailException {
		GetAllSellerResponse response = new GetAllSellerResponse();
		
		List<CoreSeller> coreSellers = coreSellerManager.getAllLive();
		for (CoreSeller coreSeller : coreSellers) {
			Seller seller = new Seller();
			seller.setId(coreSeller.getId().toString());
			seller.setPhoneNumber(coreSeller.getPhoneNumber());
			seller.setSellerId(coreSeller.getSellerId());
			seller.setSellerName(coreSeller.getFirstName().concat(" ").concat(coreSeller.getLastName()));
			seller.setStreetAddress1(coreSeller.getStreetAddress1());
			seller.setStreetAddress2(coreSeller.getStreetAddress2());
			seller.setDescriptions(coreSeller.getDescriptions());
			seller.setQrCode(generateByteCode(coreSeller.getSellerId()));
			response.getSellers().add(seller);
		}
		
		return response;
	}
	
	@PayloadRoot(localPart = "GetItemBySellerIdRequest", namespace = TARGET_NAMESPACE)
	public @ResponsePayload GetItemBySellerIdResponse getItemBySeller(@RequestPayload GetItemBySellerIdRequest request) throws RetailException {
		GetItemBySellerIdResponse response = new GetItemBySellerIdResponse();
		List<CoreItem> coreItems = coreItemManager.getBySellerId(request.getSellerId());
		for (CoreItem coreItem : coreItems) {
			Item item = new Item();
			item.setId(coreItem.getId().toString());
			item.setItemId(coreItem.getItemId());
			item.setDescription(coreItem.getDescription());
			item.setItemName(coreItem.getItemName());
			item.setItemType(coreItem.getCoreItemType().getName());
			item.setSellingPrice(coreItem.getSellingPrice());
			item.setSellerName(coreItem.getCoreSeller().getFirstName().concat(" ").concat(coreItem.getCoreSeller().getLastName()));
			item.setQrCode(generateByteCode(coreItem.getItemId()));
			//item.setImage(generateByteCode(coreItem.getFilePath()));
			item.setImage(coreItem.getFilePath());
			response.getItems().add(item);
		}
		return response;
	}
	
	@PayloadRoot(localPart = "ActiveInactiveSellerRequest", namespace = TARGET_NAMESPACE)
	public @ResponsePayload ActiveInactiveSellerResponse activeInactive(@RequestPayload ActiveInactiveSellerRequest request) throws RetailException {
		ActiveInactiveSellerResponse response = new ActiveInactiveSellerResponse();
		
		CoreSeller coreSeller = coreSellerManager.getByQRCode(request.getSellerId());
		switch (request.getStatus()) {
		case 0:
			coreSeller.setStatus(RecordStatusEnum.LIVE.getName());
			break;
		case 1:
			coreSeller.setStatus(RecordStatusEnum.INACT.getName());
			break;
		default:
			break;
		}
		coreSeller = coreSellerManager.save(coreSeller);
		response.setMessage(coreSeller.getStatus());
		return response;
	}
	
	 @PayloadRoot(localPart = "GetSellerPaymentTrxRequest", namespace = TARGET_NAMESPACE)
	 public @ResponsePayload GetSellerPaymentTrxResponse getSettlementSeller(@RequestPayload GetSellerPaymentTrxRequest request) throws RetailException {
		 GetSellerPaymentTrxResponse response = new GetSellerPaymentTrxResponse();
		 List<TrxSellerPayment> listSettlement = this.trxSellerPaymentManager.getSettlementBySellerId(request.getSellerId());
		 for (TrxSellerPayment trxSellerPayment : listSettlement) {
			ListSellerPaymentTrxType listTrx = new ListSellerPaymentTrxType();
			listTrx.setTransactionAmount(trxSellerPayment.getTotalAmount());
			listTrx.setTransactionDate(trxSellerPayment.getCreatedTime().toString());
			response.getTrx().add(listTrx);
		}
		 return response;
	 }
}
