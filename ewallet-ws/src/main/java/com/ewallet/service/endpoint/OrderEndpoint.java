/**
 * 
 */
package com.ewallet.service.endpoint;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import com.ewallet.enumeration.OrderStatusEnum;
import com.ewallet.exception.RetailException;
import com.ewallet.persistence.model.CoreItem;
import com.ewallet.persistence.model.CoreUser;
import com.ewallet.persistence.model.TrxOrder;
import com.ewallet.persistence.model.TrxOrderItemDetail;
import com.ewallet.persistence.model.TrxOrderItemId;
import com.ewallet.persistence.model.TrxRecord;
import com.ewallet.service.CoreItemManager;
import com.ewallet.service.CoreSellerManager;
import com.ewallet.service.CoreUserManager;
import com.ewallet.service.TrxOrderItemDetailManager;
import com.ewallet.service.TrxOrderManager;
import com.ewallet.service.TrxRecordManager;
import com.ewallet.service.contract.CheckAccountAmountRequest;
import com.ewallet.service.contract.CheckAccountAmountResponse;
import com.ewallet.service.contract.GetOrderListRequest;
import com.ewallet.service.contract.GetOrderListResponse;
import com.ewallet.service.contract.OrderRequest;
import com.ewallet.service.contract.OrderResponse;
import com.ewallet.service.contract.ValidasiOrderRequest;
import com.ewallet.service.contract.ValidasiOrderResponse;
import com.ewallet.service.contract.bean.Item;
import com.ewallet.service.contract.bean.Order;
import com.ewallet.util.DateUtil;

/**
 * @author akbar.wijayanto 
 * Date Oct 23, 2015 10:58:32 PM
 */
@Endpoint
public class OrderEndpoint extends BaseEndpoint {

	BigDecimal totalAmount = BigDecimal.ZERO;

	@Autowired
	private CoreUserManager coreUserManager;

	@Autowired
	private CoreSellerManager coreSellerManager;

	@Autowired
	private TrxOrderManager trxOrderManager;

	@Autowired
	private CoreItemManager coreItemManager;

	@Autowired
	private TrxRecordManager trxRecordManager;

	@Autowired
	private TrxOrderItemDetailManager trxOrderItemDetailManager;

	@PayloadRoot(localPart = "OrderRequest", namespace = TARGET_NAMESPACE)
	public @ResponsePayload OrderResponse order(@RequestPayload OrderRequest request)
			throws RetailException, ParseException {
		OrderResponse response = new OrderResponse();

		CoreUser coreUser = coreUserManager.get(new Long(request.getUserId()));
		if (coreUser == null) {
			throw new RetailException("01 - Customer not found.");
		}

		try {
			Date transactionDate = new Date();
			String orderId = DateUtil.toBundleDateFormat(transactionDate);

			TrxOrder trxOrder = new TrxOrder();
			trxOrder.setOrderId(orderId);
			trxOrder.setCoreUser(coreUser);
			//
			Set<TrxOrderItemDetail> orderItems = new HashSet<TrxOrderItemDetail>();
			// List<CoreItem> orderItems = new ArrayList<CoreItem>();
			Long sellerId = null;
			String desc = "";
			totalAmount = BigDecimal.ZERO;
			// for (int i = 0; i < request.getItemId().size(); i++) { //custom
			// by silvi.zain merubah name di xsd
			for (int i = 0; i < request.getItemsOrder().size(); i++) {

				// totalAmount = BigDecimal.ZERO;
				// CoreItem coreItem =
				// coreItemManager.getByItemId(request.getItemId().get(i).getItemId());
				// //custom by silvi.zain merubah name di xsd
				CoreItem coreItem = coreItemManager.getByItemId(request.getItemsOrder().get(i).getItemId());

				TrxOrderItemId trxOrderItemId = new TrxOrderItemId();
				trxOrderItemId.setCoreItem(coreItem);
				trxOrderItemId.setTrxOrder(trxOrder);

				TrxOrderItemDetail orderItemDetail = new TrxOrderItemDetail();
				orderItemDetail.setPk(trxOrderItemId);
				// orderItemDetail.setQuantity(request.getItemId().get(i).getQuantity());
				// //custom by silvi.zain merubah name di xsd
				orderItemDetail.setQuantity(request.getItemsOrder().get(i).getQuantity());

				trxOrder = trxOrderManager.save(trxOrder);
				orderItemDetail = trxOrderItemDetailManager.save(orderItemDetail);

				orderItems.add(orderItemDetail);
				sellerId = coreItem.getCoreSeller().getId();
				desc += coreItem.getItemName().concat(" ");
				BigDecimal quantityItem = BigDecimal.valueOf(request.getItemsOrder().get(i).getQuantity());
				totalAmount = totalAmount.add(coreItem.getSellingPrice().multiply(quantityItem));
			}
			trxOrder.setStatus(OrderStatusEnum.BOOKED.getName());
			trxOrder.setTrxOrderItemDetails(orderItems);
			trxOrder.setOrderDate(new Date());
			trxOrder.setCoreSeller(coreSellerManager.get(sellerId));

//			PaymentDto paymentDto = new PaymentDto();
//			paymentDto.setAccountFrom(coreUser.getSva());
//			paymentDto.setDescription(desc);
//			paymentDto.setAmount(totalAmount);

//			PaymentDto dto = paymentAdapter.createTransaction(paymentDto);
//			if (dto != null) {
//				trxOrder.setPaymentReference(dto.getReference());
//				trxOrder = trxOrderManager.save(trxOrder);
//				TrxRecord record = createPaymentHistory(trxOrder, totalAmount);
//			} else {
//
//			}
			response.setResponseCode(0);
			response.setResponseDesc("SUKSES");
			response.setTransactionId(trxOrder.getOrderId());
//			response.setPaymentReference(dto.getReference());
			// }
		} catch (Exception e) {
			e.printStackTrace();
			throw new RetailException(e.getMessage());
		}

		return response;
	}

	@PayloadRoot(localPart = "GetOrderListRequest", namespace = TARGET_NAMESPACE)
	public @ResponsePayload GetOrderListResponse getOrderList(@RequestPayload GetOrderListRequest request)
			throws RetailException, ParseException {
		GetOrderListResponse response = new GetOrderListResponse();

		CoreUser coreUser = coreUserManager.getUserByUsername(request.getUsername());
		if (coreUser == null) {
			throw new RetailException("01 - Customer not found.");
		}

		List<TrxOrder> orders = trxOrderManager.getOrdersByUser(request.getUsername());
		for (TrxOrder trxOrder : orders) {
			Order order = new Order();
			order.setId(trxOrder.getOrderId());
			order.setOrderId(trxOrder.getOrderId());
			if (trxOrder.getOrderDate() != null)
				order.setOrderDate(DateUtil.toXMLGregorian(trxOrder.getOrderDate()));
			if (trxOrder.getPaidDate() != null)
				order.setPaidDate(DateUtil.toXMLGregorian(trxOrder.getPaidDate()));
			order.setStatus(trxOrder.getStatus());
			order.setUserId(trxOrder.getCoreUser().getId().toString());
			order.setCustomerName(
					trxOrder.getCoreUser().getFirstName().concat(" ").concat(trxOrder.getCoreUser().getLastName()));
			order.setSellerId(trxOrder.getCoreSeller().getSellerId());
			order.setSellerName(
					trxOrder.getCoreSeller().getFirstName().concat(" ").concat(trxOrder.getCoreSeller().getLastName()));
			for (TrxOrderItemDetail orderItemDetail : trxOrder.getTrxOrderItemDetails()) {
				TrxOrderItemId pk = orderItemDetail.getPk();
				Item item = new Item();
				item.setDescription(pk.getCoreItem().getDescription());
				item.setId(pk.getCoreItem().getId().toString());
				item.setItemId(pk.getCoreItem().getItemId());
				item.setItemName(pk.getCoreItem().getItemName());
				item.setItemType(pk.getCoreItem().getCoreItemType().getName());
				item.setSellerName(pk.getCoreItem().getCoreSeller().getFirstName().concat(" ")
						.concat(pk.getCoreItem().getCoreSeller().getLastName()));
				item.setSellingPrice(pk.getCoreItem().getSellingPrice());
				item.setQuantity(orderItemDetail.getQuantity());
				order.getItems().add(item);

			}
			response.getOrder().add(order);

		}

		return response;
	}

	// custom service by silvi.zain validasi
	@PayloadRoot(localPart = "ValidasiOrderRequest", namespace = TARGET_NAMESPACE)
	public @ResponsePayload ValidasiOrderResponse getOrderResponse(@RequestPayload ValidasiOrderRequest request)
			throws RetailException, ParseException {
		ValidasiOrderResponse response = new ValidasiOrderResponse();
		int validOrder = trxOrderManager.checkValidOrder(request.getSellerId(), request.getOrderId());

		response.setValidOrder(validOrder);

		return response;
	}

	@PayloadRoot(localPart = "CheckAccountAmountRequest", namespace = TARGET_NAMESPACE)
	public @ResponsePayload CheckAccountAmountResponse checkValidAmount(
			@RequestPayload CheckAccountAmountRequest request) throws RetailException, ParseException {
		CheckAccountAmountResponse response = new CheckAccountAmountResponse();

		CoreUser coreUser = coreUserManager.get(new Long(request.getUserId()));
		if (coreUser == null) {
			throw new RetailException("01 - Customer not found.");
		}

		totalAmount = BigDecimal.ZERO;
		for (int i = 0; i < request.getItemsOrder().size(); i++) {

			CoreItem coreItem = coreItemManager.getByItemId(request.getItemsOrder().get(i).getItemId());

			BigDecimal quantityItem = BigDecimal.valueOf(request.getItemsOrder().get(i).getQuantity());
			totalAmount = totalAmount.add(coreItem.getSellingPrice().multiply(quantityItem));
		}

//		AccountDto dtoAcc = new AccountDto();
//		dtoAcc.setAccountNumber(coreUser.getSva());
//		dtoAcc = this.accountAdapter.getLimitPayroll(dtoAcc);

//		boolean isAllowPayroll = dtoAcc.isAllowedPayroll();
//		BigDecimal limitPayroll = dtoAcc.getLimitPayroll();

//		AccountDto dtoAcc2 = this.accountAdapter.balanceInquiry(coreUser.getSva());
//		BigDecimal amtBalance = dtoAcc2.getAmount();

//		if (totalAmount.compareTo(amtBalance) > 0 && isAllowPayroll == true
//				&& limitPayroll.compareTo(totalAmount) > 0) { // 4b
//			response.setResponseCode(101);
//			response.setResponseDesc(
//					"Saldo rekening anda tidak cukup, apakah anda ingin dikenakan pada gaji bulan depan?");
//		} else if (totalAmount.compareTo(amtBalance) > 0 && isAllowPayroll == true
//				&& limitPayroll.compareTo(totalAmount) < 0) { // 4c
//			response.setResponseCode(102);
//			response.setResponseDesc("Saldo rekening anda tidak cukup untuk melakukan pemesanan");
//		} else if (totalAmount.compareTo(amtBalance) > 0 && isAllowPayroll == false) { // 4d
//			response.setResponseCode(103);
//			response.setResponseDesc("Saldo rekening anda tidak cukup untuk melakukan pemesanan");
//		} else {
//			response.setResponseCode(0);
//			response.setResponseDesc("SUKSES");
//		}
		return response;
	}

	private TrxRecord createPaymentHistory(TrxOrder order, BigDecimal totalAmt) {
		TrxRecord record = new TrxRecord();
		record.setAccountNumber(order.getCoreUser().getSva());
		record.setAmount(totalAmt);
		record.setCoreSeller(order.getCoreSeller());
		record.setCoreUser(order.getCoreUser());
		record.setPairedAccount("KOPERASI");
		record.setTransactionDate(new Date());
		record.setTransactionId(order.getOrderId());
		record.setTransactionType("PAYMENT");
		record = trxRecordManager.save(record);
		return record;
	}

}
